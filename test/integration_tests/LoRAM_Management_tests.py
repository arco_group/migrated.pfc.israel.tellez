import sys, Ice

from unittest import TestCase
from doublex import *

sys.path.append('./src/integration_fakes/LoRAM_Rescue')
from detection_system import SentryCommunicatorI
from rc_system import RCManagerI, SpeakerI, GPSI

sys.path.append('./src/LoRAM_Management')
from incident_management import IncidentManagerI
from remote_access_control import RemoteControlI

sys.path.append('./src/utils')
from communication_elements import LocationC

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import SentryCommunicatorPrx, ActuatorType, SpeakerPrx, RCManagerPrx, LocationType, GPSPrx


class Incident_Management_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_notify_incident_solved(self):
        # Given
        email = "foo@example.com"
        idIncident = Stub()
        incident = Stub()

        servant = Mimic(Spy, SentryCommunicatorI)
        sentry = self.adapter_add(SentryCommunicatorPrx, servant)

        with Stub() as session:
            session.checkUser(email).returns(True)
            session.getIncident(email).returns(idIncident)
            session.getSentry(email).returns(sentry)

        with Stub() as genIncident:
            genIncident.findIncident(idIncident).returns(incident)

        sut = IncidentManagerI()

        sut.session = session
        sut.genIncident = genIncident

        # When
        sut.solveIncident(email)

        # Then
        assert_that(servant.solveIncident, called())


class Remote_Access_Control_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_activate_actuator(self):
        # Given
        request = Stub()
        request.typeReq = ActuatorType.TypeSpeaker
        request.volume = 1
        request.rate = 1

        servantSpeaker = Mimic(Spy, SpeakerI)
        speaker = self.adapter_add(SpeakerPrx, servantSpeaker)

        with Mimic(Spy, RCManagerI) as servantRCManager:
            servantRCManager.getSpeaker(ANY_ARG).returns(speaker)
        rcManager = self.adapter_add(RCManagerPrx, servantRCManager)

        with Mimic(Stub, SentryCommunicatorI) as servantSentry:
            servantSentry.getRCManager(ANY_ARG).returns(rcManager)
        sentry = self.adapter_add(SentryCommunicatorPrx, servantSentry)

        with Stub() as accessControl:
            accessControl.isSuitableActivate(ANY_ARG).returns(True)

        sut = RemoteControlI(sentry)

        sut.accessC = accessControl

        # When
        sut.activate(request)

        # Then
        assert_that(servantRCManager.getSpeaker, called())
        assert_that(servantSpeaker.turnOn, called())

    def test_deactivate_actuator(self):
        # Given
        deactivationRequest = Stub()
        deactivationRequest.typeReq = ActuatorType.TypeSpeaker

        activationRequest = Stub()
        activationRequest.typeReq = ActuatorType.TypeSpeaker
        activationRequest.volume = 1
        activationRequest.rate = 1

        servantSpeaker = Mimic(Spy, SpeakerI)
        speaker = self.adapter_add(SpeakerPrx, servantSpeaker)

        with Mimic(Spy, RCManagerI) as servantRCManager:
            servantRCManager.getSpeaker(ANY_ARG).returns(speaker)
        rcManager = self.adapter_add(RCManagerPrx, servantRCManager)

        with Mimic(Stub, SentryCommunicatorI) as servantSentry:
            servantSentry.getRCManager(ANY_ARG).returns(rcManager)
        sentry = self.adapter_add(SentryCommunicatorPrx, servantSentry)

        with Stub() as accessControl:
            accessControl.isSuitableActivate(ANY_ARG).returns(True)

        sut = RemoteControlI(sentry)

        sut.accessC = accessControl

        # When
        sut.activate(activationRequest)
        sut.deactivate(deactivationRequest)

        # Then
        assert_that(servantRCManager.getSpeaker, called())
        assert_that(servantSpeaker.turnOff, called())

    def test_get_victim_location(self):
        # Given
        request = Stub()
        request.locType = LocationType.GPSType
        request.timeBetweenUpdates = 15000
        request.distanceChangeForUpdates = 0
        location = LocationC(25, 42.2)

        with Mimic(Spy, GPSI) as servantGPS:
            servantGPS.getLocation(ANY_ARG).returns(location)
        gps = self.adapter_add(GPSPrx, servantGPS)

        with Mimic(Spy, RCManagerI) as servantRCManager:
            servantRCManager.getGPS(ANY_ARG).returns(gps)
        rcManager = self.adapter_add(RCManagerPrx, servantRCManager)

        with Mimic(Stub, SentryCommunicatorI) as servantSentry:
            servantSentry.getRCManager(ANY_ARG).returns(rcManager)
        sentry = self.adapter_add(SentryCommunicatorPrx, servantSentry)

        with Stub() as accessControl:
            accessControl.isSuitableGetLocation(ANY_ARG).returns(True)

        sut = RemoteControlI(sentry)

        sut.accessC = accessControl

        # When
        test1 = sut.getLocation(request)

        # Then
        assert_that(servantRCManager.getGPS, called())
        assert_that(servantGPS.getLocation, called())
        assert_that(test1, is_(location))
