import sys, Ice

from unittest import TestCase
from doublex import *

sys.path.append('./src/LoRAM_Management')
from user_management import UserManagerI, User
from incident_management import IncidentManagerI

sys.path.append('./src/utils')
from communication_elements import UserDataC, IssueC, TimeC, DateC

sys.path.append('./src/integration_fakes/LoRAM_Agent')
from LoRAM_Agent import *
from detection_system import *

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import UserManagerPrx, IncidentManagerPrx, SentryCommunicatorPrx, UserNotFound


class User_Management_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_login_accepted(self):
        # Given
        email = "foo@example.com"
        password = "1234"
        user = UserDataC(email, "name", "surname1", "surname2", 123456789, 987654321)

        with Mimic(Spy, UserManagerI) as servant:
            servant.login(email, password, ANY_ARG).returns(True)
            servant.getLoggedUserData(email, ANY_ARG).returns(user)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Agent(manager)

        # When
        sut.login(email, password)

        # Then
        assert_that(servant.login, called().with_args(email, password, ANY_ARG))
        assert_that(sut.connected, is_(True))
        assert_that(servant.getLoggedUserData, called().with_args(email, ANY_ARG))
        assert_that(sut.user, is_(user))

    def test_login_with_wrong_password(self):
        # Given
        email = "foo@example.com"
        password = "4321"

        with Mimic(Spy, UserManagerI) as servant:
            servant.login(email, password, ANY_ARG).returns(False)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Agent(manager)

        # When
        sut.login(email, password)

        # Then
        assert_that(servant.login, called().with_args(email, password, ANY_ARG))
        assert_that(sut.connected, is_(False))

    def test_login_with_nonexistent_user(self):
        # Given
        email = "bar@example.com"
        password = "1234"

        with Mimic(Spy, UserManagerI) as servant:
            servant.login(email, password, ANY_ARG).raises(UserNotFound)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Agent(manager)

        # When
        sut.login(email, password)

        # Then
        assert_that(servant.login, called().with_args(email, password, ANY_ARG))
        assert_that(sut.connected, is_(False))

    def test_register_user(self):
        # Given
        email = "foo@example.com"
        password = "1234"
        name = "name"
        surname1 = "surname1"
        surname2 = "surname2"
        phone = 123456789
        numAA = 987654321
        user = UserDataC(email, name, surname1, surname2, phone, numAA)

        servant = Mimic(Spy, UserManagerI)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Agent(manager)

        # When
        sut.register(email, password, name, surname1, surname2, phone, numAA)

        # Then
        assert_that(servant.register, called().with_args(user, password, ANY_ARG))

    def test_logout(self):
        # Given
        user = Stub()
        user.email = "foo@example.com"

        servant = Mimic(Spy, UserManagerI)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Agent(manager)
        sut.connected = True
        sut.user = user

        # When
        sut.logout()

        # Then
        assert_that(servant.logout, called().with_args(user.email, ANY_ARG))


class Detection_System_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_notify_issue(self):
        # Given
        email = "foo@example.com"
        user = Stub()
        user.email = email

        servantIncidentManager = Mimic(Spy, IncidentManagerI)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.getIncidentManager(email, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        time = TimeC(10, 20, 30)
        date = DateC(25, 5, 2013)

        with Stub() as datesChanger:
            datesChanger.obtainTimeToTimestamp(ANY_ARG).returns(time)
            datesChanger.obtainDateToTimestamp(ANY_ARG).returns(date)

        sut = LoRAM_Agent(manager)
        sut.connected = True
        sut.user = user
        sut.datesChanger = datesChanger

        sut.startRoute()
        servantSentry = Mimic(Stub, SentryCommunicatorI)
        sut.sentry.communicator.proxy = self.adapter_add(SentryCommunicatorPrx, servantSentry)

        # When
        timer = sut.alert()
        timer.expire()

        # Then
        sentryExpected = sut.sentry.communicator.proxy
        issueExpected = IssueC(time, date, IssueType.AlertButtonPressed)
        assert_that(servantIncidentManager.notifyIssue, \
                        called().with_args(email, sentryExpected, issueExpected, ANY_ARG))
