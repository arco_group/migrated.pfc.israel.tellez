import sys, Ice

from unittest import TestCase
from doublex import *

sys.path.append('./src/LoRAM_Management')
from user_management import UserManagerI
from incident_management import IncidentManagerI
from remote_access_control import RemoteControlI

sys.path.append('./src/utils')
from communication_elements import RescuerDataC, IncidentDataC, UserDataC, \
    SpeakerActivationRequestC, DeactivationRequestC, LocationC, LocationRequestC

sys.path.append('./src/integration_fakes/LoRAM_Rescue')
from LoRAM_Rescue import *

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import UserManagerPrx, IncidentManagerPrx, RemoteControlPrx, UserNotFound, ActuatorType

class User_Management_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_loginRescuer_accepted(self):
        # Given
        nif = "11223344a"
        password = "1234"
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, 10)

        with Mimic(Spy, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)

        # Then
        assert_that(servant.loginRescuer, called().with_args(nif, password, ANY_ARG))
        assert_that(sut.connected, is_(True))
        assert_that(servant.getLoggedRescuerData, called().with_args(nif, ANY_ARG))
        assert_that(sut.user, is_(rescuer))

    def test_loginRescuer_with_wrong_password(self):
        # Given
        nif = "11223344a"
        password = "1234"

        with Mimic(Spy, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(False)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)

        # Then
        assert_that(servant.loginRescuer, called().with_args(nif, password, ANY_ARG))
        assert_that(sut.connected, is_(False))

    def test_loginRescuer_with_nonexistent_rescuer(self):
        # Given
        nif = "44332211b"
        password = "1234"

        with Mimic(Spy, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).raises(UserNotFound)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)

        # Then
        assert_that(servant.loginRescuer, called().with_args(nif, password, ANY_ARG))
        assert_that(sut.connected, is_(False))

    def test_register_rescuer(self):
        # Given
        nif = "11223344a"
        password = "1234"
        name = "name"
        surname1 = "surname1"
        surname2 = "surname2"
        phone = 123456789
        team = 10
        rescuer = RescuerDataC(nif, name, surname1, surname2, phone, team)

        servant = Mimic(Spy, UserManagerI)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.registerRescuer(nif, password, name, surname1, surname2, phone, team)

        # Then
        assert_that(servant.registerRescuer, called().with_args(rescuer, password, ANY_ARG))

    def test_logout(self):
        # Given
        rescuer = Stub()
        rescuer.nif = "11223344a"

        servant = Mimic(Spy, UserManagerI)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)
        sut.connected = True
        sut.user = rescuer

        # When
        sut.logout()

        # Then
        assert_that(servant.logout, called().with_args(rescuer.nif, ANY_ARG))


class Query_System_Test(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_list_incidents(self):
        # Given
        nif = "11223344a"
        password = "1234"
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, 10)

        servantIncidentManager = Mimic(Spy, IncidentManagerI)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()


        # Then
        assert_that(servantIncidentManager.listIncidents, called().with_args(rescuer.nif, \
                                                                                 ANY_ARG))

    def test_allocate_incident(self):
        # Given
        nif = "11223344a"
        password = "1234"
        team = 10
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, team)
        idIncident = 234
        victimEmail = "foo@example.com"
        victim = UserDataC(victimEmail)
        incident = IncidentDataC(idIncident, victim.email)
        incidentList = [incident]

        with Mimic(Spy, IncidentManagerI) as servantIncidentManager:
            servantIncidentManager.listIncidents(nif, ANY_ARG).returns(incidentList)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getLoggedUserData(victim.email, ANY_ARG).returns(victim)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()
        sut.querySystem.allocate(idIncident)

        # Then
        assert_that(servantIncidentManager.allocateIncident, \
                        called().with_args(team, idIncident, ANY_ARG))


class Rescue_System(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)

        return cast.uncheckedCast(proxy)

    def test_solve_incident(self):
        # Given
        nif = "112233344a"
        password = "1234"
        team = 10
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, team)
        idIncident = 234
        victimEmail = "foo@example.com"
        victim = UserDataC(victimEmail)
        incident = IncidentDataC(idIncident, victimEmail)
        incidentList = [incident]

        with Mimic(Spy, IncidentManagerI) as servantIncidentManager:
            servantIncidentManager.listIncidents(nif, ANY_ARG).returns(incidentList)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getLoggedUserData(victim.email, ANY_ARG).returns(victim)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()
        sut.querySystem.allocate(idIncident)
        sut.querySystem.rescueSystem.solve()

        # Then
        assert_that(servantIncidentManager.solveIncident, called().with_args(victimEmail, ANY_ARG))

    def test_activate_actuator(self):
        # Given
        nif = "112233344a"
        password = "1234"
        team = 10
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, team)
        idIncident = 234
        victimEmail = "foo@example.com"
        victim = UserDataC(victimEmail)
        incident = IncidentDataC(idIncident, victimEmail)
        incidentList = [incident]

        servantRC = Mimic(Spy, RemoteControlI)
        rc = self.adapter_add(RemoteControlPrx, servantRC)

        with Mimic(Spy, IncidentManagerI) as servantIncidentManager:
            servantIncidentManager.listIncidents(nif, ANY_ARG).returns(incidentList)
            servantIncidentManager.getRemoteControl(victimEmail, ANY_ARG).returns(rc)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getLoggedUserData(victim.email, ANY_ARG).returns(victim)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()
        sut.querySystem.allocate(idIncident)
        sut.querySystem.rescueSystem.activateSpeaker()

        # Then
        assert_that(servantIncidentManager.getRemoteControl, \
                        called().with_args(victimEmail, ANY_ARG))
        requestExpected = SpeakerActivationRequestC()
        assert_that(servantRC.activate, called().with_args(requestExpected, ANY_ARG))

    def test_deactivate_actuator(self):
        # Given
        nif = "112233344a"
        password = "1234"
        team = 10
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, team)
        idIncident = 234
        victimEmail = "foo@example.com"
        victim = UserDataC(victimEmail)
        incident = IncidentDataC(idIncident, victimEmail)
        incidentList = [incident]

        servantRC = Mimic(Spy, RemoteControlI)
        rc = self.adapter_add(RemoteControlPrx, servantRC)

        with Mimic(Spy, IncidentManagerI) as servantIncidentManager:
            servantIncidentManager.listIncidents(nif, ANY_ARG).returns(incidentList)
            servantIncidentManager.getRemoteControl(victimEmail, ANY_ARG).returns(rc)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getLoggedUserData(victim.email, ANY_ARG).returns(victim)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()
        sut.querySystem.allocate(idIncident)
        sut.querySystem.rescueSystem.deactivateSpeaker()

        # Then
        assert_that(servantIncidentManager.getRemoteControl, \
                        called().with_args(victimEmail, ANY_ARG))
        requestExpected = DeactivationRequestC(None, ActuatorType.TypeSpeaker)
        assert_that(servantRC.deactivate, called().with_args(requestExpected, ANY_ARG))

    def test_locate_victim(self):
        # Given
        nif = "112233344a"
        password = "1234"
        team = 10
        rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, team)
        idIncident = 234
        victimEmail = "foo@example.com"
        victim = UserDataC(victimEmail)
        incident = IncidentDataC(idIncident, victimEmail)
        incidentList = [incident]
        victimLocation = LocationC(25, 42.2)

        with Mimic(Spy, RemoteControlI) as servantRC:
            servantRC.getLocation(ANY_ARG).returns(victimLocation)
        rc = self.adapter_add(RemoteControlPrx, servantRC)

        with Mimic(Spy, IncidentManagerI) as servantIncidentManager:
            servantIncidentManager.listIncidents(nif, ANY_ARG).returns(incidentList)
            servantIncidentManager.getRemoteControl(victimEmail, ANY_ARG).returns(rc)
        incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

        with Mimic(Stub, UserManagerI) as servant:
            servant.loginRescuer(nif, password, ANY_ARG).returns(True)
            servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
            servant.getLoggedUserData(victim.email, ANY_ARG).returns(victim)
            servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
        manager = self.adapter_add(UserManagerPrx, servant)

        sut = LoRAM_Rescue(manager)

        # When
        sut.loginRescuer(nif, password)
        sut.querySystem.showIncidents()
        sut.querySystem.allocate(idIncident)
        test1 = sut.querySystem.rescueSystem.locate()

        # Then
        assert_that(servantIncidentManager.getRemoteControl, \
                        called().with_args(victimEmail, ANY_ARG))
        requestExpected = LocationRequestC()
        assert_that(servantRC.getLocation, called().with_args(requestExpected, ANY_ARG))
        assert_that(test1, is_(victimLocation))
