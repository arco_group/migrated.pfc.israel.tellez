import sys

from unittest import TestCase
from doublex import *

sys.path.append('./src/LoRAM_Management')
from user_management import *


class User_Test(TestCase):
    def setUp(self):
        self.email = "foo@example.com"
        self.password = "pass"
        self.name = "name"
        self.surname1 = "surname1"
        self.surname2 = "surname2"
        self.phone = 123456789
        self.numAA = 987654321

    def test_insertUser(self):
        # Given
        sut = User('', '')
        user = User(self.email, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.numAA)

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.insertUser(user)

        # Then
        assert_that(dbBroker, verify())
        expected = "INSERT INTO Users(email, password, name, surname1, surname2, phone, numAA) " \
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %s, %s);" % \
            (self.email, self.password, self.name, self.surname1, self.surname2, self.phone, \
                 self.numAA)
        assert_that(cursor.execute, called().with_args(expected))

    def test_updateUser(self):
        # Given
        sut = User('', '')
        user = User(self.email, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.numAA)

        oldEmail = "bar@example.com"

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.updateUser(user, oldEmail)

        # Then
        assert_that(dbBroker, verify())
        expected = "UPDATE Users SET email=\'%s\', password=\'%s\', name=\'%s\', surname1=\'%s\', " \
            "surname2=\'%s\', phone=%s, numAA=%s WHERE email=\'%s\'" % \
            (self.email, self.password, self.name, self.surname1, self.surname2, self.phone, \
                 self.numAA, oldEmail)
        assert_that(cursor.execute, called().with_args(expected))

    def test_deleteUser(self):
        # Given
        sut = User('', '')
        user = User(self.email, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.numAA)

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.deleteUser(user)

        # Then
        assert_that(dbBroker, verify())
        expected =  "DELETE FROM Users WHERE email=\'%s\' AND password=\'%s\'" % \
            (self.email, self.password)
        assert_that(cursor.execute, called().with_args(expected))

    def test_existUser_when_is_right(self):
        # Given
        sut = User('', '')

        with Spy() as cursor:
            cursor.execute(ANY_ARG).returns(1)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.existUser(self.email)

        # Then
        assert_that(dbBroker, verify())
        expected = "SELECT * FROM Users WHERE email=\'%s\'" % (self.email)
        assert_that(cursor.execute, called().with_args(expected))
        assert_that(test1, is_(True))

    def test_existUser_when_is_wrong(self):
        # Given
        sut = User('', '')

        with Spy() as cursor:
            cursor.execute(ANY_ARG).returns(0)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.existUser(self.email)

        # Then
        assert_that(dbBroker, verify())
        expected = "SELECT * FROM Users WHERE email=\'%s\'" % (self.email)
        assert_that(cursor.execute, called().with_args(expected))
        assert_that(test1, is_(False))

    def test_findUser_when_exist(self):
        # Given
        sut = User('', '')
        result = [self.email, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.numAA]

        with Spy() as cursor:
            cursor.fetchone().returns(result)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.findUser(self.email, self.password)

        # Then
        assert_that(dbBroker, verify())
        expected1 =  "SELECT email, password, name, surname1, surname2, phone, numAA FROM Users " \
            "WHERE email=\'%s\' AND password=\'%s\'" % \
            (self.email, self.password)
        assert_that(cursor.execute, called().with_args(expected1))

        expected2 = User(self.email, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.numAA)
        assert_that(test1, is_(expected2))

    def test_findUser_when_not_exist(self):
        # Given
        sut = User('', '')

        with Spy() as cursor:
            cursor.fetchone().returns(None)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # Then
        with self.assertRaises(UserOrPasswordIncorrect):
            sut.findUser(self.email, self.password)

        assert_that(dbBroker, verify())
        expected =  "SELECT email, password, name, surname1, surname2, phone, numAA FROM Users " \
            "WHERE email=\'%s\' AND password=\'%s\'" % \
            (self.email, self.password)
        assert_that(cursor.execute, called().with_args(expected))


class Rescuer_Test(TestCase):
    def setUp(self):
        self.nif = "11223344a"
        self.password = "pass"
        self.name = "name"
        self.surname1 = "surname1"
        self.surname2 = "surname2"
        self.phone = 123456789
        self.team = 12

    def test_insertRescuer(self):
        # Given
        sut = Rescuer('', '')
        rescuer = Rescuer(self.nif, self.password, self.name, self.surname1, self.surname2, \
                                      self.phone, self.team)

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.insertRescuer(rescuer)

        # Then
        assert_that(dbBroker, verify())
        expected = "INSERT INTO Rescue_staff(nif, password, name, surname1, surname2, phone, team) "\
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %s, %s);" % \
            (self.nif, self.password, self.name, self.surname1, self.surname2, self.phone, self.team)
        assert_that(cursor.execute, called().with_args(expected))

    def test_updateRescuer(self):
        # Given
        sut = Rescuer('', '')
        rescuer = Rescuer(self.nif, self.password, self.name, self.surname1, self.surname2, \
                                      self.phone, self.team)

        oldNif = "44332211b"

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.updateRescuer(rescuer, oldNif)

        # Then
        assert_that(dbBroker, verify())
        expected = "UPDATE Rescue_staff SET nif=\'%s\', password=\'%s\', name=\'%s\', " \
            "surname1=\'%s\', surname2=\'%s\', phone=%s, team=%s WHERE nif=\'%s\'" % \
            (self.nif, self.password, self.name, self.surname1, self.surname2, self.phone, \
                 self.team, oldNif)
        assert_that(cursor.execute, called().with_args(expected))

    def test_deleteRescuer(self):
        # Given
        sut = Rescuer('', '')
        rescuer = Rescuer(self.nif, self.password, self.name, self.surname1, self.surname2, \
                                      self.phone, self.team)

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.deleteRescuer(rescuer)

        # Then
        assert_that(dbBroker, verify())
        expected = "DELETE FROM Rescue_staff WHERE nif=\'%s\' AND password=\'%s\'" % \
            (self.nif, self.password)
        assert_that(cursor.execute, called().with_args(expected))

    def test_existRescuer_when_is_right(self):
        # Given
        sut = Rescuer('', '')

        with Spy() as cursor:
            cursor.execute(ANY_ARG).returns(1)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.existRescuer(self.nif)

        # Then
        assert_that(dbBroker, verify())
        expected = "SELECT * FROM Rescue_staff WHERE nif=\'%s\'" % (self.nif)
        assert_that(cursor.execute, called().with_args(expected))
        assert_that(test1, is_(True))

    def test_existRescuer_when_is_wrong(self):
        # Given
        sut = Rescuer('', '')

        with Spy() as cursor:
            cursor.execute(ANY_ARG).returns(0)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.existRescuer(self.nif)

        # Then
        assert_that(dbBroker, verify())
        expected = "SELECT * FROM Rescue_staff WHERE nif=\'%s\'" % (self.nif)
        assert_that(cursor.execute, called().with_args(expected))
        assert_that(test1, is_(False))

    def test_findRescuer_when_exist(self):
        # Given
        sut = Rescuer('', '')
        result = [self.nif, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.team]

        with Spy() as cursor:
            cursor.fetchone().returns(result)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.findRescuer(self.nif, self.password)

        # Then
        assert_that(dbBroker, verify())
        expected1 =  "SELECT nif, password, name, surname1, surname2, phone, team FROM " \
            "Rescue_staff WHERE nif=\'%s\' AND password=\'%s\'" % \
            (self.nif, self.password)
        assert_that(cursor.execute, called().with_args(expected1))

        expected2 = Rescuer(self.nif, self.password, self.name, self.surname1, self.surname2, \
                       self.phone, self.team)
        assert_that(test1, is_(expected2))

    def test_findRescuer_when_not_exist(self):
        # Given
        sut = Rescuer('', '')

        with Spy() as cursor:
            cursor.fetchone().returns(None)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # Then
        with self.assertRaises(UserOrPasswordIncorrect):
            sut.findRescuer(self.nif, self.password)

        assert_that(dbBroker, verify())
        expected =  "SELECT nif, password, name, surname1, surname2, phone, team FROM " \
            "Rescue_staff WHERE nif=\'%s\' AND password=\'%s\'" % \
            (self.nif, self.password)
        assert_that(cursor.execute, called().with_args(expected))


class User_Manager_Test(TestCase):
    def test_login_accepted(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        password = "aaaa"
        user = Stub()
        session = Spy()
        with Spy() as genUser:
            genUser.findUser(email, password).returns(user)

        sut.genUser = genUser
        sut.session = session

        # When
        success = sut.login(email, password)

        # Then
        assert_that(genUser.findUser, called().with_args(email, password))
        assert_that(session.putUser, called().with_args(user))
        assert_that(success, is_(True))

    def test_login_with_nonexistent_user(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        password = "aaaa"
        with Stub() as genUser:
            genUser.findUser(email, password).raises(UserOrPasswordIncorrect)
            genUser.existUser(email).returns(False)

        sut.genUser = genUser

        # Then
        with self.assertRaises(UserNotFound):
            sut.login(email, password)

    def test_login_with_wrong_password(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        password = "uuuu"
        with Stub() as genUser:
            genUser.findUser(email, password).raises(UserOrPasswordIncorrect)
            genUser.existUser(email).returns(True)

        sut.genUser = genUser

        # When
        success = sut.login(email, password)

        # Then
        assert_that(success, is_(False))

    def test_loginRescuer_accepted(self):
        # Given
        sut = UserManagerI()

        nif = "11223344a"
        password = "aaaa"
        rescuer = Stub()
        session = Spy()
        with Spy() as genRescuer:
            genRescuer.findRescuer(nif, password).returns(rescuer)

        sut.genRescuer = genRescuer
        sut.session = session

        # When
        success = sut.loginRescuer(nif, password)

        # Then
        assert_that(genRescuer.findRescuer, called().with_args(nif, password))
        assert_that(session.putRescuer, called().with_args(rescuer))
        assert_that(success, is_(True))

    def test_loginRescuer_with_nonexistent_rescuer(self):
        # Given
        sut = UserManagerI()

        nif = "11223344a"
        password = "aaaa"
        with Stub() as genRescuer:
            genRescuer.findRescuer(nif, password).raises(UserOrPasswordIncorrect)
            genRescuer.existRescuer(nif).returns(False)

        sut.genRescuer = genRescuer

        # Then
        with self.assertRaises(UserNotFound):
            sut.loginRescuer(nif, password)

    def test_loginRescuer_with_wrong_password(self):
        # Given
        sut = UserManagerI()

        nif = "11223344a"
        password = "aaaa"
        with Stub() as genRescuer:
            genRescuer.findRescuer(nif, password).raises(UserOrPasswordIncorrect)
            genRescuer.existRescuer(nif).returns(True)

        sut.genRescuer = genRescuer

        # When
        success = sut.loginRescuer(nif, password)

        # Then
        assert_that(success, is_(False))

    def test_logout(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        with Spy() as session:
            session.checkUser(email).returns(True)

        sut.session = session

        # When
        sut.logout(email)

        # Then
        assert_that(session.removeUser, called().with_args(email))

    def test_register_with_existing_user(self):
        # Given
        sut = UserManagerI()

        userData = Stub()
        userData.email = "aa@ee.com"
        password = "uuuu"

        with Stub() as genUser:
            genUser.existUser(userData.email).returns(True)

        sut.genUser = genUser

        # Then
        with self.assertRaises(UserAlreadyExists):
            sut.register(userData, password)

    def test_register_with_nonexistent_user(self):
        # Given
        sut = UserManagerI()

        userData = Stub()
        userData.email = "aa@ee.com"
        userData.name = "name"
        userData.surname1 = "surname1"
        userData.surname2 = "surname2"
        userData.phone = 1111
        userData.numAA = 2222
        password = "pass"
        with Spy() as genUser:
            genUser.existUser(userData.email).returns(False)

        sut.genUser = genUser

        # When
        sut.register(userData, password)

        # Then
        expected = User(userData.email, password, userData.name, userData.surname1, \
                            userData.surname2, userData.phone, userData.numAA)
        assert_that(genUser.insertUser, called().with_args(expected))

    def test_registerRescuer_with_existing_user(self):
        # Given
        sut = UserManagerI()

        userData = Stub()
        userData.nif = "11223344a"
        password = "uuuu"

        with Stub() as genRescuer:
            genRescuer.existRescuer(userData.nif).returns(True)

        sut.genRescuer = genRescuer

        # Then
        with self.assertRaises(UserAlreadyExists):
            sut.registerRescuer(userData, password)

    def test_registerRescuer_with_nonexistent_user(self):
        # Given
        sut = UserManagerI()

        userData = Stub()
        userData.nif = "11223344a"
        userData.name = "name"
        userData.surname1 = "surname1"
        userData.surname2 = "surname2"
        userData.phone = 1111
        userData.team = 10
        password = "pass"

        with Spy() as genRescuer:
            genRescuer.existRescuer(userData.nif).returns(False)

        sut.genRescuer = genRescuer

        # When
        sut.registerRescuer(userData, password)

        # Then
        expected = Rescuer(userData.nif, password, userData.name, userData.surname1, \
                               userData.surname2, userData.phone, userData.team)
        assert_that(genRescuer.insertRescuer, called().with_args(expected))

    def test_getLoggedUserData_with_logged_user(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        userData = Stub()
        with Spy() as user:
            user.getData().returns(userData)

        with Stub() as session:
            session.checkUser(email).returns(True)
            session.getUser(email).returns(user)

        sut.session = session

        # When
        test1 = sut.getLoggedUserData(email)

        # Then
        assert_that(user.getData, called())
        assert_that(test1, is_(userData))

    def test_getLoggedUserData_with_non_logged_user(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        with Stub() as session:
            session.checkUser(email).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(UserNotLogged):
            sut.getLoggedUserData(email)

    def test_getLoggedRescuerData_with_logged_rescuer(self):
        # Given
        sut = UserManagerI()

        nif = "11223344a"
        userData = Stub()
        with Spy() as rescuer:
            rescuer.getData().returns(userData)

        with Stub() as session:
            session.checkRescuer(nif).returns(True)
            session.getRescuer(nif).returns(rescuer)

        sut.session = session

        # When
        test1 = sut.getLoggedRescuerData(nif)

        # Then
        assert_that(rescuer.getData, called())
        assert_that(test1, is_(userData))

    def test_getLoggedRescuerData_with_non_logged_rescuer(self):
        # Given
        sut = UserManagerI()

        nif = "11223344a"
        with Stub() as session:
            session.checkRescuer(nif).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(UserNotLogged):
            sut.getLoggedRescuerData(nif)

    def test_getIncidentManager_with_logged_user(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        with Stub() as session:
            session.checkUser(email).returns(True)

        proxyIM = Stub()
        with Spy() as incidentManager:
            incidentManager.getProxy().returns(proxyIM)

        sut.session = session
        sut.incidentManager = incidentManager

        # When
        test1 = sut.getIncidentManager(email)

        # Then
        assert_that(incidentManager.getProxy, called())
        assert_that(test1, is_(proxyIM))

    def test_getIncidentManager_with_non_logged_user(self):
        # Given
        sut = UserManagerI()

        email = "aa@ee.com"
        with Stub() as session:
            session.checkUser(email).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(AccessDenied):
            sut.getIncidentManager(email)
