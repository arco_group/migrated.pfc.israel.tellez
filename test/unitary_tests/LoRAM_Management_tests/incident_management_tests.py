import sys

from unittest import TestCase
from doublex import *

sys.path.append('./src/LoRAM_Management')
from incident_management import *

sys.path.append('./src/utils')
from communication_elements import *


class Incident_Test(TestCase):
    def setUp(self):
        self.victim = "foo@example.com"
        self.date = "2000-2-1 12:20:30"
        self.typeIncident = "DetectedFall"
        self.state = "NoAllocate"
        self.rescueTeam = 230

    def test_insertIncident(self):
        # Given
        sut = Incident('', '', '')
        incident = Incident(self.victim, self.date, self.typeIncident, self.state, self.rescueTeam)

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.insertIncident(incident)

        # Then
        assert_that(dbBroker, verify())
        expected =  "INSERT INTO Incidents(victim, date, state, type, rescue_team) " \
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', %s);" % \
            (self.victim, self.date, self.state, self.typeIncident, \
                 self.rescueTeam)
        assert_that(cursor.execute, called().with_args(expected))

    def test_findIncidentId(self):
        # Given
        sut = Incident('', '', '')

        idIncident = 15
        result = [idIncident]
        with Spy() as cursor:
            cursor.fetchone().returns(result)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.findIncidentId(self.victim, self.date)

        # Then
        assert_that(dbBroker, verify())
        expected = "SELECT id FROM Incidents WHERE victim=\'%s\' AND date=\'%s\'" % \
            (self.victim, self.date)
        assert_that(cursor.execute, called().with_args(expected))
        assert_that(test1, is_(idIncident))

    def test_updateIncident(self):
        # Given
        sut = Incident('', '', '')

        incident = Incident(self.victim, self.date, self.typeIncident, self.state, self.rescueTeam)
        idIncident = 15

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.updateIncident(incident, idIncident)

        # Then
        assert_that(dbBroker, verify())
        expected = "UPDATE Incidents SET victim=\'%s\', date=\'%s\', state=\'%s\', type=\'%s\', " \
            "rescue_team=%s WHERE id=%s" % \
            (self.victim, self.date, self.state, self.typeIncident, \
                 self.rescueTeam, idIncident)
        assert_that(cursor.execute, called().with_args(expected))

    def test_deleteIncident(self):
        # Given
        sut = Incident('', '', '')

        idIncident = 15

        cursor = Spy()

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        sut.deleteIncident(idIncident)

        # Then
        assert_that(dbBroker, verify())
        expected = "DELETE FROM Incidents WHERE id=%s" % (idIncident)
        assert_that(cursor.execute, called().with_args(expected))

    def test_findIncident_when_exist(self):
        # Given
        sut = Incident('', '', '')

        idIncident = 15
        result = [self.victim, self.date, self.typeIncident, self.state, self.rescueTeam]

        with Spy() as cursor:
            cursor.fetchone().returns(result)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # When
        test1 = sut.findIncident(idIncident)

        # Then
        assert_that(dbBroker, verify())
        expected1 = "SELECT victim, date, type, state, rescue_team FROM Incidents " \
            "WHERE id=%s" % (idIncident)
        assert_that(cursor.execute, called().with_args(expected1))

        expected2 = Incident(self.victim, self.date, self.typeIncident, self.state, \
                       self.rescueTeam)
        assert_that(test1, is_(expected2))

    def test_findIncident_when_not_exist(self):
        # Given
        sut = Incident('', '', '')

        idIncident = 15

        with Spy() as cursor:
            cursor.fetchone().returns(None)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        sut.dbBroker = dbBroker

        # Then
        with self.assertRaises(IncidentNotFound):
            sut.findIncident(idIncident)

        assert_that(dbBroker, verify())
        expected = "SELECT victim, date, type, state, rescue_team FROM Incidents " \
            "WHERE id=%s" % (idIncident)
        assert_that(cursor.execute, called().with_args(expected))

    def test_findUnresolvedIncidents(self):
        # Given
        sut = Incident('', '', '')

        resultInc1 = [1, self.victim, self.date, self.state, self.typeIncident, self.rescueTeam]
        result = [resultInc1]
        date = DateC(1, 2, 2000)
        time = TimeC(12, 20, 30)

        with Spy() as cursor:
            cursor.fetchall().returns(result)

        with Stub() as con:
            con.cursor().returns(cursor)

        with Mock() as dbBroker:
            dbBroker.getConnection().returns(con)
            dbBroker.returnConnection(con)

        with Stub() as datesChanger:
            datesChanger.obtainDateToTimestamp(self.date).returns(date)
            datesChanger.obtainTimeToTimestamp(self.date).returns(time)

        sut.dbBroker = dbBroker
        sut.datesChanger = datesChanger

        # When
        test1 = sut.findUnresolvedIncidents()

        # Then
        assert_that(dbBroker, verify())
        expected1 = "SELECT id, victim, date, state, type, rescue_team FROM Incidents " \
            "WHERE state <> \'%s\'" % (StateType.Resolved)
        assert_that(cursor.execute, called().with_args(expected1))

        expected2 = [IncidentData(1, self.victim, date, time, StateType.NoAllocate, \
                                      IssueType.DetectedFall, self.rescueTeam)]
        assert_that(test1, is_(expected2))


class Incident_Manager_Test(TestCase):
    def test_notifyIssue_with_logged_user(self):
        # Given
        sut = IncidentManagerI()

        email = "foo@example.com"
        timestamp = "2000-2-1 12:20:30"
        sentry = Stub()
        issue = Stub()
        issue.typeIss = "AlertButtonPressed"
        idIncident = 23

        with Spy() as session:
            session.checkUser(email).returns(True)

        with Stub() as datesChanger:
            datesChanger.createTimestamp(ANY_ARG).returns(timestamp)

        with Spy() as genIncident:
            genIncident.findIncidentId(ANY_ARG).returns(idIncident)

        sut.session = session
        sut.datesChanger = datesChanger
        sut.genIncident = genIncident

        # When
        sut.notifyIssue(email, sentry, issue)

        # Then
        assert_that(session.addSentry, called().with_args(email, sentry))
        expected = Incident(email, timestamp, issue.typeIss)
        assert_that(genIncident.insertIncident, called().with_args(expected))
        assert_that(session.addIncident, called().with_args(email, idIncident))

    def test_notifyIssue_with_non_logged_user(self):
        # Given
        sut = IncidentManagerI()

        email = "foo@example.com"
        sentry = Stub()
        issue = Stub()

        with Stub() as session:
            session.checkUser(email).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(AccessDenied):
            sut.notifyIssue(email,sentry, issue)

    def test_listIncidents_of_logged_rescuer(self):
        # Given
        sut = IncidentManagerI()

        nif = "11223344a"
        incidentsList = Stub()

        with Stub() as session:
            session.checkRescuer(nif).returns(True)

        with Spy() as genIncident:
            genIncident.findUnresolvedIncidents().returns(incidentsList)

        sut.session = session
        sut.genIncident = genIncident

        # When
        test1 = sut.listIncidents(nif)

        # Then
        assert_that(genIncident.findUnresolvedIncidents, called())
        assert_that(test1, is_(incidentsList))

    def test_listIncidents_of_non_logged_rescuer(self):
        # Given
        sut = IncidentManagerI()

        nif = "11223344a"

        with Stub() as session:
            session.checkRescuer(nif).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(AccessDenied):
            sut.listIncidents(nif)

    def test_getRemoteControl(self):
        # Given
        sut = IncidentManagerI()

        email = "foo@example.com"

        proxy = Stub()

        with Spy() as rc:
            rc.getProxy().returns(proxy)

        with Stub() as session:
            session.checkUser(email).returns(True)
            session.checkRC(email).returns(True)
            session.getRC(email).returns(rc)

        sut.session = session

        # When
        test1 = sut.getRemoteControl(email)

        # Then
        assert_that(rc.getProxy, called())
        assert_that(test1, is_(proxy))

    def test_allocateIncident(self):
        # Given
        sut = IncidentManagerI()

        idIncident = 12
        team = 9
        incident = Stub()

        with Spy() as genIncident:
            genIncident.findIncident(idIncident).returns(incident)

        sut.genIncident = genIncident

        # When
        sut.allocateIncident(team, idIncident)

        # Then
        assert_that(incident.rescueTeam, is_(team))
        assert_that(incident.state, is_(StateType.Allocated))
        assert_that(genIncident.updateIncident, called().with_args(incident, idIncident))

    def test_solveIncident_of_logged_user(self):
        # Given
        sut = IncidentManagerI()

        email = "foo@example.com"
        idIncident = 23
        incident = Stub()
        sentry = Spy()

        with Spy() as session:
            session.checkUser(email).returns(True)
            session.getIncident(email).returns(idIncident)
            session.getSentry(email).returns(sentry)

        with Spy() as genIncident:
            genIncident.findIncident(idIncident).returns(incident)

        sut.session = session
        sut.genIncident = genIncident

        # When
        sut.solveIncident(email)

        # Then
        assert_that(incident.state, is_(StateType.Resolved))
        assert_that(genIncident.updateIncident, called().with_args(incident, idIncident))
        assert_that(session.deleteIncident, called().with_args(email))
        assert_that(sentry.solveIncident, called())


    def test_solveIncident_of_non_logged_user(self):
        # Given
        sut = IncidentManagerI()

        email = "foo@example.com"

        with Stub() as session:
            session.checkUser(email).returns(False)

        sut.session = session

        # Then
        with self.assertRaises(AccessDenied):
            sut.solveIncident(email)
