import sys

from unittest import TestCase
from doublex import *

sys.path.append('./src/LoRAM_Management')
from remote_access_control import *


class AccessControl_Test(TestCase):
    def setUp(self):
        self.levelHigh = 75
        self.levelMediumHigh = 51
        self.levelMedium = 50
        self.levelMediumLow = 49
        self.levelLow = 15
        self.noBattery = 0

        self.levelDifferenceLimit = 5
        self.levelDifferenceOverLimit = 6
        self.levelDifferenceUnderLimit = 4

        with Stub() as self.battery1:
            self.battery1.getLevel().returns(self.levelHigh)

        with Stub() as self.battery2:
            self.battery2.getLevel().returns(self.levelMediumHigh)

        with Stub() as self.battery3:
            self.battery3.getLevel().returns(self.levelMedium)

        with Stub() as self.battery4:
            self.battery4.getLevel().returns(self.levelMediumLow)

        with Stub() as self.battery5:
            self.battery5.getLevel().returns(self.levelLow)

        with Stub() as self.battery6:
            self.battery6.getLevel().returns(self.noBattery)

    def test_isSuitableActivate_with_two_actuators_on(self):
        # Given

        sut1 = AccessControl(self.battery1)

        sut20 = AccessControl(self.battery2)
        sut20.lastBatteryLevel = (self.battery2.getLevel() + self.levelDifferenceLimit)
        sut21 = AccessControl(self.battery2)
        sut21.lastBatteryLevel = (self.battery2.getLevel() + self.levelDifferenceOverLimit)
        sut22 = AccessControl(self.battery2)
        sut22.lastBatteryLevel = (self.battery2.getLevel() + self.levelDifferenceUnderLimit)

        sut30 = AccessControl(self.battery3)
        sut30.lastBatteryLevel = (self.battery3.getLevel() + self.levelDifferenceLimit)
        sut31 = AccessControl(self.battery3)
        sut31.lastBatteryLevel = (self.battery3.getLevel() + self.levelDifferenceOverLimit)
        sut32 = AccessControl(self.battery3)
        sut32.lastBatteryLevel = (self.battery3.getLevel() + self.levelDifferenceUnderLimit)

        sut4 = AccessControl(self.battery4)

        sut5 = AccessControl(self.battery5)

        sut6 = AccessControl(self.battery6)

        actuatorsOn = [True, True, False]

        # When
        test1 = sut1.isSuitableActivate(actuatorsOn)

        test20 = sut20.isSuitableActivate(actuatorsOn)
        test21 = sut21.isSuitableActivate(actuatorsOn)
        test22 = sut22.isSuitableActivate(actuatorsOn)

        test30 = sut30.isSuitableActivate(actuatorsOn)
        test31 = sut31.isSuitableActivate(actuatorsOn)
        test32 = sut32.isSuitableActivate(actuatorsOn)

        test4 = sut4.isSuitableActivate(actuatorsOn)
        test5 = sut5.isSuitableActivate(actuatorsOn)
        test6 = sut6.isSuitableActivate(actuatorsOn)

        # Then
        assert_that(test1, is_(True))
        assert_that(test20, is_(False))
        assert_that(test21, is_(False))
        assert_that(test22, is_(True))
        assert_that(test30, is_(False))
        assert_that(test31, is_(False))
        assert_that(test32, is_(False))
        assert_that(test4, is_(False))
        assert_that(test5, is_(False))
        assert_that(test6, is_(False))

    def test_isSuitableActivate_with_one_actuator_on(self):
        # Given

        sut1 = AccessControl(self.battery1)
        sut2 = AccessControl(self.battery2)
        sut3 = AccessControl(self.battery3)

        sut40 = AccessControl(self.battery4)
        sut40.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceLimit)
        sut41 = AccessControl(self.battery4)
        sut41.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceOverLimit)
        sut42 = AccessControl(self.battery4)
        sut42.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceUnderLimit)

        sut50 = AccessControl(self.battery5)
        sut50.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceLimit)
        sut51 = AccessControl(self.battery5)
        sut51.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceOverLimit)
        sut52 = AccessControl(self.battery5)
        sut52.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceUnderLimit)

        sut6 = AccessControl(self.battery6)

        actuatorsOn = [True, False, False]

        # When
        test1 = sut1.isSuitableActivate(actuatorsOn)
        test2 = sut2.isSuitableActivate(actuatorsOn)
        test3 = sut3.isSuitableActivate(actuatorsOn)

        test40 = sut40.isSuitableActivate(actuatorsOn)
        test41 = sut41.isSuitableActivate(actuatorsOn)
        test42 = sut42.isSuitableActivate(actuatorsOn)

        test50 = sut50.isSuitableActivate(actuatorsOn)
        test51 = sut51.isSuitableActivate(actuatorsOn)
        test52 = sut52.isSuitableActivate(actuatorsOn)

        test6 = sut6.isSuitableActivate(actuatorsOn)

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(True))
        assert_that(test3, is_(True))
        assert_that(test40, is_(False))
        assert_that(test41, is_(False))
        assert_that(test42, is_(True))
        assert_that(test50, is_(False))
        assert_that(test51, is_(False))
        assert_that(test52, is_(False))
        assert_that(test6, is_(False))

    def test_isSuitableActivate_without_actuators_on(self):
        # Given

        sut1 = AccessControl(self.battery1)
        sut2 = AccessControl(self.battery2)
        sut3 = AccessControl(self.battery3)
        sut4 = AccessControl(self.battery4)
        sut5 = AccessControl(self.battery5)
        sut6 = AccessControl(self.battery6)

        actuatorsOn = [False, False, False]

        # When
        test1 = sut1.isSuitableActivate(actuatorsOn)
        test2 = sut2.isSuitableActivate(actuatorsOn)
        test3 = sut3.isSuitableActivate(actuatorsOn)
        test4 = sut4.isSuitableActivate(actuatorsOn)
        test5 = sut5.isSuitableActivate(actuatorsOn)
        test6 = sut6.isSuitableActivate(actuatorsOn)

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(True))
        assert_that(test3, is_(True))
        assert_that(test4, is_(True))
        assert_that(test5, is_(True))
        assert_that(test6, is_(False))

    def test_isSuitableGetLocation_with_two_actuators_on(self):
        # Given

        sut1 = AccessControl(self.battery1)
        sut2 = AccessControl(self.battery2)
        sut3 = AccessControl(self.battery3)

        sut40 = AccessControl(self.battery4)
        sut40.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceLimit)
        sut41 = AccessControl(self.battery4)
        sut41.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceOverLimit)
        sut42 = AccessControl(self.battery4)
        sut42.lastBatteryLevel = (self.battery4.getLevel() + self.levelDifferenceUnderLimit)

        sut50 = AccessControl(self.battery5)
        sut50.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceLimit)
        sut51 = AccessControl(self.battery5)
        sut51.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceOverLimit)
        sut52 = AccessControl(self.battery5)
        sut52.lastBatteryLevel = (self.battery5.getLevel() + self.levelDifferenceUnderLimit)

        sut6 = AccessControl(self.battery6)

        actuatorsOn = [True, True, False]

        # When
        test1 = sut1.isSuitableGetLocation(actuatorsOn)
        test2 = sut2.isSuitableGetLocation(actuatorsOn)
        test3 = sut3.isSuitableGetLocation(actuatorsOn)

        test40 = sut40.isSuitableGetLocation(actuatorsOn)
        test41 = sut41.isSuitableGetLocation(actuatorsOn)
        test42 = sut42.isSuitableGetLocation(actuatorsOn)

        test50 = sut50.isSuitableGetLocation(actuatorsOn)
        test51 = sut51.isSuitableGetLocation(actuatorsOn)
        test52 = sut52.isSuitableGetLocation(actuatorsOn)

        test6 = sut6.isSuitableGetLocation(actuatorsOn)

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(True))
        assert_that(test3, is_(True))
        assert_that(test40, is_(False))
        assert_that(test41, is_(False))
        assert_that(test42, is_(True))
        assert_that(test50, is_(False))
        assert_that(test51, is_(False))
        assert_that(test52, is_(False))
        assert_that(test6, is_(False))

    def test_isSuitableGetLocation_with_one_actuators_on(self):
        # Given

        sut1 = AccessControl(self.battery1)
        sut2 = AccessControl(self.battery2)
        sut3 = AccessControl(self.battery3)
        sut4 = AccessControl(self.battery4)
        sut5 = AccessControl(self.battery5)
        sut6 = AccessControl(self.battery6)

        actuatorsOn = [True, False, False]

        # When
        test1 = sut1.isSuitableGetLocation(actuatorsOn)
        test2 = sut2.isSuitableGetLocation(actuatorsOn)
        test3 = sut3.isSuitableGetLocation(actuatorsOn)
        test4 = sut4.isSuitableGetLocation(actuatorsOn)
        test5 = sut5.isSuitableGetLocation(actuatorsOn)
        test6 = sut6.isSuitableGetLocation(actuatorsOn)

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(True))
        assert_that(test3, is_(True))
        assert_that(test4, is_(True))
        assert_that(test5, is_(True))
        assert_that(test6, is_(False))

class Remote_Control_Test(TestCase):
    def test_activate_when_is_suitable(self):
        # Given
        request = Stub()
        request.typeReq = ActuatorType.TypeSpeaker
        request.volume = 0.4
        request.rate = 1.2

        battery = Stub()

        speaker = Spy()

        with Stub() as rcManager:
            rcManager.getBattery().returns(battery)
            rcManager.getSpeaker().returns(speaker)

        with Stub() as sentry:
            sentry.getRCManager().returns(rcManager)

        sut = RemoteControlI(sentry)

        with Stub() as accessC:
            accessC.isSuitableActivate(ANY_ARG).returns(True)

        sut.accessC = accessC

        # When
        sut.activate(request)

        # Then
        assert_that(speaker.configure, called().with_args(request.volume, request.rate))
        assert_that(speaker.turnOn, called())
        assert_that(sut.speaker['active'], is_(True))

    def test_activate_when_is_not_suitable(self):
        # Given
        request = Stub()
        battery = Stub()

        with Stub() as rcManager:
            rcManager.getBattery().returns(battery)

        with Stub() as sentry:
            sentry.getRCManager().returns(rcManager)

        sut = RemoteControlI(sentry)

        with Stub() as accessC:
            accessC.isSuitableActivate(ANY_ARG).returns(False)

        sut.accessC = accessC

        # Then
        with self.assertRaises(NotSuitable):
            sut.activate(request)

    def test_deactivate(self):
        # Given
        request = Stub()
        request.typeReq = ActuatorType.TypeLamp
        battery = Stub()

        with Stub() as rcManager:
            rcManager.getBattery().returns(battery)

        with Stub() as sentry:
            sentry.getRCManager().returns(rcManager)

        sut = RemoteControlI(sentry)

        lamp = Spy()

        sut.lamp['proxy'] = lamp
        sut.lamp['active'] = True

        # When
        sut.deactivate(request)

        # Then
        assert_that(lamp.turnOff, called())
        assert_that(sut.lamp['active'], is_(False))

    def test_get_location_when_is_suitable(self):
        # Given
        request = Stub()
        request.locType = LocationType.GPSType
        request.timeBetweenUpdates = 1000
        request.distanceChangeForUpdates = 5
        location = Stub()

        battery = Stub()
        with Spy() as gps:
            gps.getLocation().returns(location)

        with Stub() as rcManager:
            rcManager.getBattery().returns(battery)
            rcManager.getGPS().returns(gps)

        with Stub() as sentry:
            sentry.getRCManager().returns(rcManager)

        sut = RemoteControlI(sentry)

        with Stub() as accessC:
            accessC.isSuitableGetLocation(ANY_ARG).returns(True)

        sut.accessC = accessC

        # When
        test1 = sut.getLocation(request)

        # Then
        assert_that(gps.configure, called().with_args(request.timeBetweenUpdates, \
                                                          request.distanceChangeForUpdates))
        assert_that(gps.getLocation, called())
        assert_that(test1, is_(location))

    def test_get_location_when_is_not_suitable(self):
        # Given
        request = Stub()
        battery = Stub()

        with Stub() as rcManager:
            rcManager.getBattery().returns(battery)

        with Stub() as sentry:
            sentry.getRCManager().returns(rcManager)

        sut = RemoteControlI(sentry)

        with Stub() as accessC:
            accessC.isSuitableGetLocation(ANY_ARG).returns(False)

        sut.accessC = accessC

        # Then
        with self.assertRaises(NotSuitable):
            sut.getLocation(request)
