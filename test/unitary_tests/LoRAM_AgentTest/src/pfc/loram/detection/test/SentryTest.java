package pfc.loram.detection.test;

import static org.mockito.Mockito.*;
import pfc.loram.LoRAM;
import pfc.loram.detection.Sentry;
import pfc.loram.detection.SentryCommunicatorI;
import pfc.loram.detection.sensors.AccelerationSensor;
import Ice.ObjectAdapter;
import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import LoRAM.Issue;
import LoRAM.IssueType;
import LoRAM.UserData;
import android.test.ServiceTestCase;

public class SentryTest extends ServiceTestCase<Sentry> {
	
	private Sentry sut;
	private LoRAM mApp;
	
	private SentryCommunicatorI _communicator;
	private AccelerationSensor _accelerationSensor;
	
	public SentryTest(Class<Sentry> serviceClass) {
		super(serviceClass);
	}
	
	public SentryTest() {
		super(Sentry.class);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		mApp = mock(LoRAM.class);
		IncidentManagerPrx manager = mock(IncidentManagerPrx.class);
		ObjectAdapter adapter = mock(ObjectAdapter.class);
		_communicator = mock(SentryCommunicatorI.class);
		_accelerationSensor = mock(AccelerationSensor.class);
		UserData user = new UserData("foo@example.com", "name", "surname1", "surname2", 123456789, 987654321);
		
		when(mApp.getIncidentManagerProxy()).thenReturn(manager);
		when(mApp.get_adapter()).thenReturn(adapter);
		when(mApp.get_user()).thenReturn(user);
		
		setApplication(mApp);
	}
	
	public void testConfirmIssue() {
		// Given
		startService(null);
		sut = getService();
		
		sut.set_state("onRoute");
		IssueType issueType = IssueType.AlertButtonPressed;

		// When
		sut.confirmIssue(issueType);
		
		// Then
		assertEquals("confirming", sut.get_state());
		
	}
	
	public void testCancelConfirmation() {
		// Given
		startService(null);
		sut = getService();
		
		sut.set_state("confirming");
		
		// When
		sut.cancelConfirmation();
		
		// Then
		assertEquals("onRoute", sut.get_state());
	}
	
	public void testNotifyIssue() throws AccessDenied {
		// Given
		startService(null);
		sut = getService();
		Issue issue = new Issue();
		
		sut.set_communicator(_communicator);
		sut.set_state("confirming");
		
		// When
		sut.notifyIssue(issue);
		
		// Then
		verify(_communicator).notifyIssue(issue);
		assertEquals("rescue", sut.get_state());
	}
	
	public void testFinishRoute() {
		// Given
		startService(null);
		sut = getService();
		
		sut.set_accelerationSensor(_accelerationSensor);
		// When
		sut.finishRoute();
		
		// Then
		verify(_accelerationSensor).onDestroy();
		assertEquals(false, sut.is_rcBound());
	}
	
}
