package pfc.loram.detection.test;

import static org.mockito.Mockito.*;

import pfc.loram.detection.Sentry;
import pfc.loram.detection.SentryCommunicatorI;
import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import LoRAM.Issue;
import LoRAM.NotExistRCModule;
import LoRAM.RCManagerPrx;
import LoRAM.SentryCommunicatorPrx;
import LoRAM.UserData;
import android.test.AndroidTestCase;

public class SentryCommunicatorITest extends AndroidTestCase {

	private SentryCommunicatorPrx _proxy;
	private IncidentManagerPrx _manager;
	private Sentry _sentry;
	private UserData _user;
	
	private SentryCommunicatorI sut;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		_manager = mock(IncidentManagerPrx.class);
		_proxy = mock(SentryCommunicatorPrx.class);
		_sentry = mock(Sentry.class);
		_user = new UserData("foo@example.com", "name", "surname1", "surname2", 
				123456789, 987654321);
	}
	
	public void testNotifyIssue() throws AccessDenied {
		// Given
		sut = new SentryCommunicatorI(_manager, _sentry, 
				_user);
		
		Issue issue = new Issue();
		
		sut.set_proxy(_proxy);
		
		// When
		sut.notifyIssue(issue);
		
		// Then
		verify(_manager).notifyIssue(_user.email, _proxy, issue);
	}
	
	public void testGetRCManager() throws NotExistRCModule {
		// Given
		sut = new SentryCommunicatorI(_manager, _sentry, _user);
		
		RCManagerPrx proxy = mock(RCManagerPrx.class);
		when(_sentry.getRCProxy()).thenReturn(proxy);
		
		// When
		RCManagerPrx rcManager = sut.getRCManager();
		
		// Then
		assertEquals(proxy, rcManager);
	}
	
	public void testSolveIncident() {
		// Given
		sut = new SentryCommunicatorI(_manager, _sentry, _user);
		
		// When
		sut.solveIncident();
		
		// Then
		verify(_sentry).solveIncident();
	}
	
}
