package pfc.loram.actuators.test;

import static org.mockito.Mockito.*;
import pfc.loram.rc.actuators.BatteryI;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.test.AndroidTestCase;

public class BatteryITest extends AndroidTestCase{

	private BatteryI sut;
	private Context context;
	private Intent batteryStatus;
	
	public BatteryITest(String name) {
		setName(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		context = mock(Context.class);
		batteryStatus = mock(Intent.class);
		
		when(context.registerReceiver((BroadcastReceiver) anyObject(), (IntentFilter) anyObject())).thenReturn(batteryStatus);
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testGetLevel() {
		// Given
		sut = new BatteryI(context);
		int level = 50;
		int scale = 500;
		float expected = (level / (float)scale) * 100;
		
		when(batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(level);
		when(batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(scale);
		
		// When
		float actual = sut.getLevel();
		
		// Then
		verify(batteryStatus).getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		verify(batteryStatus).getIntExtra(BatteryManager.EXTRA_SCALE, -1);
		assertEquals(expected, actual);
	}
	
}
