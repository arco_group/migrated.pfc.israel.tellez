package pfc.loram.actuators.test;

import pfc.loram.R;
import pfc.loram.rc.actuators.SpeakerI;
import android.test.AndroidTestCase;

public class SpeakerITest extends AndroidTestCase {

	private SpeakerI mSpeakerI;
	
	public SpeakerITest(String name) {
		setName(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		
		mSpeakerI = new SpeakerI(mContext, R.raw.alarm);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public final void testSpeakerIContext() {
		assertNotNull(mSpeakerI);
	}
	
	public final void testTurnOn() {
		mSpeakerI.turnOn();
		final boolean expected = true;
		final boolean actual = mSpeakerI.is_on();
		
		assertEquals(expected, actual);
	}
	
	public final void testTurnOff() {
		mSpeakerI.turnOff();
		final boolean expected = false;
		final boolean actual = mSpeakerI.is_on();
		
		assertEquals(expected, actual);
	}
	
	public final void testConfigure() {
		final float expectedVolume = 3.0f;
		final float expectedRate = 1.5f;
		mSpeakerI.configure(expectedVolume, expectedRate);
		final float actualVolume = mSpeakerI.get_volume();
		final float actualRate = mSpeakerI.get_rate();
		
		assertEquals(expectedVolume, actualVolume, 1e-6);
		assertEquals(expectedRate, actualRate, 1e-6);
	}
	
	public final void testChangeSound() {
		final float notExpected = mSpeakerI.get_soundID();
		mSpeakerI.changeSound(R.raw.alarm);
		final float actual = mSpeakerI.get_soundID();
		
		assertNotSame(notExpected, actual);
	}
}
