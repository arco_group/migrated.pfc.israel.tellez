package pfc.loram.actuators.test;

import pfc.loram.rc.actuators.VibratorI;
import android.test.AndroidTestCase;

public class VibratorITest extends AndroidTestCase{

	private VibratorI mVibratorI;
	
	public VibratorITest(String name) {
		setName(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		this.mVibratorI = new VibratorI(mContext);
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public final void testVibratorIContext() {
		assertNotNull(mVibratorI);
	}
	
	public final void testTurnOn() {
		mVibratorI.turnOn();
		final boolean expected = true;
		final boolean actual = mVibratorI.is_on();
		
		assertEquals(expected, actual);
	}
	
	public final void testTurnOff() {
		mVibratorI.turnOff();
		final boolean expected = false;
		final boolean actual = mVibratorI.is_on();
		
		assertEquals(expected, actual);
	}
}
