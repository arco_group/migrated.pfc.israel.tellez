package pfc.loram.actuators.test;

import pfc.loram.rc.actuators.LampI;
import android.test.AndroidTestCase;

public class LampITest extends AndroidTestCase {
	
	private LampI mLampI;
	
	public LampITest(String name) {
		setName(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		this.mLampI = new LampI();
	}
	
	protected void tearDown() throws Exception{
		super.tearDown();
	}
	
	public void testLampIContext() {
		assertNotNull(mLampI);
	}
	
	public final void testTurnOn() {
		mLampI.turnOn();
		final boolean expected = true;
		final boolean actual = mLampI.is_on();
		
		assertEquals(expected, actual);
	}
	
	public final void testTurnOff() {
		mLampI.turnOff();
		final boolean expected = false;
		final boolean actual = mLampI.is_on();
		
		assertEquals(expected, actual);
	}

}
