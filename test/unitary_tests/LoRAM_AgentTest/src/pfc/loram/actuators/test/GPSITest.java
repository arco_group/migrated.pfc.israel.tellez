package pfc.loram.actuators.test;

import static org.mockito.Mockito.*;

import pfc.loram.rc.actuators.GPSI;
import LoRAM.ActuatorOff;
import LoRAM.Location;
import android.content.Context;
import android.location.LocationManager;
import android.test.AndroidTestCase;

public class GPSITest extends AndroidTestCase {

	private GPSI sut;
	private Context context;
	private LocationManager locationManager;
	
	public GPSITest(String name) {
		setName(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		context = mock(Context.class);
		locationManager = mock(LocationManager.class);
		
		when(context.getSystemService(anyString())).thenReturn(locationManager);
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testIsEnabled() {
		// Given
		sut = new GPSI(context);
		boolean expected = true;
		
		when(locationManager.isProviderEnabled(anyString())).thenReturn(expected);
		
		// When
		boolean actual = sut.isEnable();
		
		// Then
		verify(locationManager).isProviderEnabled(anyString());
		assertEquals(expected, actual);
	}
	
	public void testGetLocationWhenIs_On() {
		// Given
		sut = new GPSI(context);
		android.location.Location location = mock(android.location.Location.class);
		double expectedLatitude = 37.422006;
		double expectedLongitude = -122.084095;
		
		when(locationManager.isProviderEnabled(anyString())).thenReturn(true);
		when(location.getLatitude()).thenReturn(expectedLatitude);
		when(location.getLongitude()).thenReturn(expectedLongitude);
		sut.onLocationChanged(location);
		
		// When
		Location actual = null;
		try {
			actual = sut.getLocation();
		} catch (ActuatorOff e) {
			fail();
		}
		
		// Then
		assertEquals(expectedLatitude, actual.latitude);
		assertEquals(expectedLongitude, actual.longitude);
	}
	
	public final void testGetLocationWhenIs_Off() {
		// Given
		sut = new GPSI(context);
		
		when(locationManager.isProviderEnabled(anyString())).thenReturn(false);
		
		// When
		try {
			sut.getLocation();
			fail();
		} catch (ActuatorOff e) {
			assertTrue(true);
		}
	}

}
