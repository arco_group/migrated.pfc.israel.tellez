package pfc.loram.util.test;

import static org.mockito.Mockito.*;

import java.util.TimerTask;

import pfc.loram.util.Timer;
import pfc.loram.util.TimerListener;
import android.test.AndroidTestCase;

public class TimerTest extends AndroidTestCase {
	
	private long _delay;
	private TimerListener _listener;
	private java.util.Timer _internalTimer;
	
	private Timer sut; 
	
	public TimerTest(String name) {
		setName(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		_delay = 10;
		_listener = mock(TimerListener.class);
		_internalTimer = mock(java.util.Timer.class);
	}
	
	public void testStart() {
		// Given
		
		sut = new Timer(_delay, _listener);
		
		sut.set_timer(_internalTimer);
		
		// When
		sut.start();
		
		// Then
		verify(_internalTimer).schedule(any(TimerTask.class), anyLong(), anyLong());
		verify(_listener).onTimerStart();
		assertEquals(true, sut.is_started());
		assertEquals(0, sut.get_progress());
		
	}
	
	public void testExpire() {
		// Given
		sut = new Timer(_delay, _listener);
		
		sut.set_timer(_internalTimer);
		
		// When
		sut.start();
		sut.expire();
		
		// Then
		verify(_internalTimer).cancel();
		verify(_listener).onExpiry();
		assertEquals(true, sut.is_expired());
		assertEquals(false, sut.is_started());
	}
	
	public void testCancel() {
		// Given
		sut = new Timer(_delay, _listener);
		
		sut.set_timer(_internalTimer);
		
		// When
		sut.start();
		sut.cancel();
		
		// Then
		verify(_internalTimer).cancel();
		verify(_listener).onCancelled();
		assertEquals(true, sut.is_canceled());
		assertEquals(false, sut.is_started());
	}

}
