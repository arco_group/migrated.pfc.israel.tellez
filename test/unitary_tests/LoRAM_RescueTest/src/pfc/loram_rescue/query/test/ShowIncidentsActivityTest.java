package pfc.loram_rescue.query.test;

import static org.mockito.Mockito.*;

import pfc.loram_rescue.LoRAM_Rescue;
import pfc.loram_rescue.query.ShowIncidentsActivity;
import LoRAM.AccessDenied;
import LoRAM.Date;
import LoRAM.IncidentData;
import LoRAM.IncidentManagerPrx;
import LoRAM.IssueType;
import LoRAM.RescuerData;
import LoRAM.StateType;
import LoRAM.Time;
import android.content.Intent;
import android.test.ActivityUnitTestCase;

public class ShowIncidentsActivityTest extends ActivityUnitTestCase<ShowIncidentsActivity> {

	private ShowIncidentsActivity sut;
	private LoRAM_Rescue mApp;
	private IncidentManagerPrx manager;
	
	public ShowIncidentsActivityTest(Class<ShowIncidentsActivity> activityClass) {
		super(activityClass);
	}

	public ShowIncidentsActivityTest() {
		super(ShowIncidentsActivity.class);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		mApp = mock(LoRAM_Rescue.class);
		manager = mock(IncidentManagerPrx.class);
		RescuerData user = new RescuerData("11223344a", "name", "surname1", "surname2", 123456789, 11);
		
		when(mApp.getIncidentManagerProxy()).thenReturn(manager);
		when(mApp.get_user()).thenReturn(user);
		
		setApplication(mApp);
		
	}
	
	public void testListIncidentTask() throws AccessDenied {
		// Given
		startActivity(new Intent(getInstrumentation().getTargetContext(), ShowIncidentsActivity.class), null, null);
		sut = getActivity();
		ShowIncidentsActivity.ListIncidentTask task = sut.new ListIncidentTask();
		
		IncidentData[] expected = new IncidentData[3];
		expected[0] = new IncidentData(1, "victim1", new Date(), new Time(), StateType.Allocated, 
				IssueType.AlertButtonPressed, 11);
		expected[1] = new IncidentData(2, "victim2", new Date(), new Time(), StateType.NoAllocate, 
				IssueType.DetectedCollision, 22);
		expected[2] = new IncidentData(3, "victim3", new Date(), new Time(), StateType.Resolved, 
				IssueType.DetectedFall, 33);
		
		when(manager.listIncidents(anyString())).thenReturn(expected);

		// When
		task.execute(mApp);
		sut.onResume();
		
		// Then
		verify(manager).listIncidents(anyString());
		
	}
}
