import sys

from unittest import TestCase
from doublex import *

sys.path.append('./src')
from utils.timer import *
from utils.datesChanger import *
from utils.session import *
from utils.dbBroker import *
from utils.communication_elements import *


class Timer_Test(TestCase):
    def test_expire(self):
        # Given
        listener = Spy()
        delay = 10
        sut = Timer(delay, listener.onExpiry, Stub())

        # When
        sut.start()
        sut.expire()

        # Then
        assert_that(listener.onExpiry, called())

    def test_cancel(self):
        # Given
        listener = Spy()
        delay = 10
        sut = Timer(delay, listener.onExpiry, listener.onCancelled)

        # When
        sut.start()
        sut.cancel()

        # Then
        assert_that(listener.onCancelled, called())
        assert_that(listener.onExpiry, never(called()))


class DatesChanger_Test(TestCase):
    def test_create(self):
        # Given
        sut = DatesChanger()

        date = DateC(1, 2, 2000)
        time = TimeC(1,2,3)

        # When
        timestamp = sut.createTimestamp(date, time)

        # Then
        assert_that(timestamp, is_("2000-2-1 1:2:3"))

    def test_obtainData(self):
        # Given
        sut = DatesChanger()

        timestamp = "2000-2-1 12:20:30"

        # When
        date = sut.obtainDateToTimestamp(timestamp)

        # Then
        expected = DateC(1, 2, 2000)
        assert_that(date, is_(expected))

    def test_obtainTime(self):
        # Given
        sut = DatesChanger()

        timestamp = "2000-2-1 12:20:30"

        # When
        time = sut.obtainTimeToTimestamp(timestamp)

        # Then
        expected = TimeC(12, 20, 30)
        assert_that(time, is_(expected))


class DBBroker_Test(TestCase):
    def test_getConnection(self):
        # Given
        sut = DBBroker

        con = Stub()
        sut.available = [con]

        # When
        test1 = sut.getConnection()

        # Then
        assert_that(test1, is_(con))
        assert_that(con in sut.available, is_(False))
        assert_that(len(sut.available), is_(0))
        assert_that(sut.busy[-1], is_(con))
        assert_that(len(sut.busy), is_(1))

    def test_returnConnection(self):
        # Given
        sut = DBBroker

        con = Spy()
        sut.busy = [con]

        # When
        sut.returnConnection(con)

        # Then
        assert_that(con.commit, called())
        assert_that(con in sut.busy, is_(False))
        assert_that(sut.available[-1], is_(con))

    def test_clean(self):
        # Given
        sut = DBBroker

        con1 = Spy()
        con2 = Spy()

        sut.available = [con1, con2]

        # When
        sut.clean()

        # Then
        assert_that(con1.close, called())
        assert_that(con2.close, called())
        assert_that(len(sut.available), is_(0))


class Session_Test(TestCase):
    def test_putUser(self):
        # Given
        sut = Session

        user = Stub()
        user.email = "aa@ee.com"

        # When
        sut.putUser(user)

        # Then
        assert_that(user.email in sut.users, is_(True))
        assert_that(sut.users[user.email]['user'], is_(user))

    def test_putRescuer(self):
        # Given
        sut = Session

        rescuer = Stub()
        rescuer.nif = "11223344a"

        # When
        sut.putRescuer(rescuer)

        # Then
        assert_that(rescuer.nif in sut.rescueStaff, is_(True))
        assert_that(sut.rescueStaff[rescuer.nif]['rescuer'], is_(rescuer))

    def test_removeUser(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        sut.users[email] = Stub()

        # When
        sut.removeUser(email)

        # Then
        assert_that(email in sut.users, is_(False))

    def test_removeRescuer(self):
        # Given
        sut = Session

        nif = "11223344a"
        sut.rescueStaff[nif] = Stub()

        # When
        sut.removeRescuer(nif)

        # Then
        assert_that(nif in sut.rescueStaff, is_(False))

    def test_checkUser(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        sut.users[email] = Stub()

        # When
        test1 = sut.checkUser(email)
        test2 = sut.checkUser("oo@ii.com")

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(False))

    def test_checkRescuer(self):
        # Given
        sut = Session

        nif = "11223344a"
        sut.rescueStaff[nif] = Stub()

        # When
        test1 = sut.checkRescuer(nif)
        test2 = sut.checkRescuer("44332211b")

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(False))

    def test_checkRC(self):
        # Given
        sut = Session

        email = "foo@example.com"
        sessionData = {'rc': Stub()}
        sut.users[email] = sessionData

        emailFail = "bar@example.com"
        sut.users[emailFail] = {}

        # When
        test1 = sut.checkRC(email)
        test2 = sut.checkRC(emailFail)

        # Then
        assert_that(test1, is_(True))
        assert_that(test2, is_(False))

    def test_addSentry(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        sut.users[email] = {}
        sentry = Stub()

        # When
        sut.addSentry(email, sentry)

        # Then
        assert_that(sut.users[email]['sentry'], is_(sentry))

    def test_addIncident(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        sut.users[email] = {}
        idIncident = 290

        # When
        sut.addIncident(email, idIncident)

        # Then
        assert_that(sut.users[email]['incident'], is_(idIncident))

    def test_addRC(self):
        # Given
        sut = Session

        email = "foo@example.com"
        sut.users[email] = {}
        rc = Stub()

        # When
        sut.addRC(email, rc)

        # Then
        assert_that(sut.users[email]['rc'], is_(rc))

    def test_getUser(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        user = Stub()
        sut.users[email] = {'user': user}

        # When
        test1 = sut.getUser(email)

        # Then
        assert_that(test1, is_(user))

    def test_getRescuer(self):
        # Given
        sut = Session

        nif = "11223344a"
        rescuer = Stub()
        sut.rescueStaff[nif] = {'rescuer': rescuer}

        # When
        test1 = sut.getRescuer(nif)

        # Then
        assert_that(test1, is_(rescuer))

    def test_getSentry(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        sentry = Stub()
        sut.users[email] = {'sentry': sentry}

        # When
        test1 = sut.getSentry(email)

        # Then
        assert_that(test1, is_(sentry))

    def test_getIncident(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        idIncident = 290
        sut.users[email] = {'incident': idIncident}

        # When
        test1 = sut.getIncident(email)

        # Then
        assert_that(test1, is_(idIncident))

    def test_getRC(self):
        # Given
        sut = Session

        email = "foo@example.com"
        rc = Stub()
        sut.users[email] = {'rc': rc}

        # When
        test1 = sut.getRC(email)

        # Then
        assert_that(test1, is_(rc))

    def test_deleteIncident(self):
        # Given
        sut = Session

        email = "aa@ee.com"
        idIncident = 290
        sut.users[email] = {'incident': idIncident}

        # When
        sut.deleteIncident(email)

        # Then
        assert_that('incident' in sut.users[email], is_(False))
