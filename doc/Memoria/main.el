(TeX-add-style-hook "main"
 (lambda ()
    (LaTeX-add-bibliographies)
    (TeX-run-style-hooks
     "eurosym"
     "mathtools"
     "custom"
     ""
     "latex2e"
     "arco-pfc10"
     "arco-pfc"
     "oneside"
     "metadata"
     "resumen"
     "abstract"
     "acro"
     "thanks"
     "intro"
     "antecedentes"
     "objetivos"
     "metodo"
     "arquitectura"
     "resultados"
     "conclusiones"
     "manualusuario"
     "manualusuariorescate"
     "helloworld_iceandroid")))

