\chapter{Método de trabajo}
\label{chap:metodo}

\drop{E}{}n este capítulo se explicará al lector la metodología de desarrollo empleada
para la realización de este proyecto. Se mostrarán también las herramientas, tanto
hardware como software, utilizadas en la realización del mismo.


\section{Metodología de trabajo}
% Hablar sobre TDD y Extreme Programming(XP)

% Contar por que he escogido esa metodología
% Explicar la metodología, mostrar esquema de trabajo de la metodología


La realización de este proyecto suponía hacer frente a una serie de dificultades durante
su desarrollo, ya que los requisitos del sistema no estaban del todo claros al inicio,
podían ser cambiantes y a pesar de tener una idea clara de lo que se pretendía hacer, se
suponía imposible prever todos los requisitos inicialmente. Todo ello dificultaba realizar
un cálculo a priori del alcance del proyecto, por lo que se optó por hacer uso de una
metodología ágil.

La metodología de desarrollo finalmente escogida para la realización de
\ac{LoRAM} ha sido \acx{XP}, ya que hace especial énfasis en la adaptabilidad a los
cambios durante la fase de análisis y desarrollo.

\subsection{Extreme Programming}

\ac{XP}~\cite{KentBeck} es una metodología ágil de desarrollo de software creada por
Kent Beck. Esta metodología tiene como objetivo entregar al cliente el software que
necesita en el momento que lo necesita, respondiendo a los requerimientos cambiantes de
los clientes incluso en las fases finales del ciclo de vida del desarrollo. Otro objetivo
que presenta \ac{XP} es potenciar el trabajo en equipo, de modo que tanto los clientes
como los jefes de proyecto y desarrolladores, estén involucrados en el desarrollo.

La metodología \ac{XP} define cuatro variables para los proyectos de software: costo,
tiempo, calidad y ámbito. De estas cuatro variables solo tres de ellas podrán ser
establecidas por actores externos al grupo de desarrollo, mientras que la cuarta deberá
ser establecida por el equipo de desarrollo, en función del valor de las anteriores.

\ac{XP} se basa en una serie de valores que deben ser maximizados en todo proyecto para
alcanzar el éxito~\cite{XP_Values}.

\begin{itemize}
\item \textbf{Simplicidad:} Se debe realizar de la forma más sencilla posible
  únicamente lo que se nos pide. Se irán dando pequeños pasos hacia los objetivos
  marcados, corrigiendo los fallos que vayan apareciendo sobre la marcha.

\item \textbf{Comunicación:} La comunicación entre todas las partes es importante durante
  todo el proceso para evitar errores en el desarrollo.

\item \textbf{Retroalimentación:} Se realizan iteraciones cortas en las que se presenta un
  software funcional al cliente. De este modo se minimizan los
  problemas derivados de errores en la comprensión de los requisitos.

\item \textbf{Honestidad:} Ser honestos sobre las estimaciones y
  afrontar los problemas que se presenten sin miedo a desechar trabajo
  realizado.
\end{itemize}

Con el fin de maximizar estos valores, \ac{XP} establece una serie de reglas y
prácticas que deben ser realizadas. Estas reglas pueden ser agrupadas en cinco
tipos~\cite{XP_Rules}: \emph{Planificación, gestión, diseño, codificación y pruebas}. En
la figura~\ref{fig:process_model_XP} podemos ver un modelo visual de los procesos que
intervienen en \ac{XP}.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{/Process_model_for_XP.png}
    \caption{Modelo de proceso de \acs{XP}}
    \label{fig:process_model_XP}
  \end{center}
\end{figure}

A continuación se muestran en detalle las reglas y prácticas indicadas por \ac{XP} según
la división anteriormente citada.

\begin{description}
\item [Planificación.] Se comienza recopilando <<historias de usuario>> para describir
  la funcionalidad del software. A continuación el usuario asigna una prioridad a cada
  <<historia de usuario>> y el equipo de desarrollo evalúa el tiempo que será necesario
  para la realización de cada una. El proyecto se divide en iteraciones y se planifica el
  comienzo de cada una de las iteraciones.

\item [Gestión.] Se asigna al equipo un espacio de trabajo y se establece un ritmo
  sostenido. Se realiza una reunión entre los miembros al inicio de cada día para medir
  la velocidad en la que avanza el proyecto, y poder compartir problemas y soluciones que
  vallan surgiendo.

\item [Diseño.] Se hace especial hincapié en la simplicidad y en la utilización de
  metáforas con el fin de facilitar la comprensión rápida del código, por ello se
  utilizan nombres claros y descriptivos que no requieran de documentación extra. Para
  reducir riesgos se crean soluciones <<spike>> y se utiliza la
  recodificación (\emph{refactoring}) siempre que sea posible.

\item [Codificación.] La disponibilidad del cliente es requerida durante todo el
  proyecto, por lo que se necesita que pase a formar parte del grupo. Se hace uso de
  estándares y se realiza una programación dirigida por las pruebas
  (ver sección~\ref{sec:TDD}). También se establece una programación por pares, es decir, dos
  personas trabajan juntas en el mismo ordenador con el fin de evitar errores. Además,
  estos grupos tendrán libertad para cambiar el código que sea necesario, ya que se
  establece una propiedad colectiva del código. Las integraciones se realizan a menudo,
  aunque solo una pareja puede realizar integraciones a la vez.

\item [Pruebas.] Las pruebas son uno de los conceptos más importantes en \ac{XP}.
  Para cada módulo deben existir sus correspondientes pruebas unitarias, que deberán
  ser pasadas en su totalidad antes de la publicación de dicho módulo. Para la corrección
  de errores se deberán escribir pruebas que permitan verificar que el error ha sido
  resuelto. Finalmente se deberán realizar pruebas de aceptación en base a las historias
  de usuario, y serán los usuarios los encargados de verificar que los resultados de
  dichas pruebas son los esperados.
\end{description}


\subsection{\acl{TDD}}
\label{sec:TDD}

La metodología \ac{XP} incluye como técnica de diseño e implementación
\acx{TDD}~\cite{TDD}. Uno de los objetivos principales que se persigue al diseñar software
con esta técnica, es la de implementar solo las funciones exactas que el cliente precisa y
ninguna más, de este modo se evita desarrollar funcionalidades que nunca serán
usadas. Otro objetivo que se presenta, es la minimización del número de defectos que
llegan en la fase de producción al software. Además se propone una producción de software
modular que este preparada para el cambio y sea altamente reutilizable.

La utilización correcta de \ac{TDD} como técnica de diseño conlleva la aplicación del
algoritmo \ac{TDD}, que consiste en la realización de los tres siguientes pasos:

\begin{itemize}
\item Escribir el test o especificación del requisito.

\item Implementar el código adecuado según ese test.

\item Refactorizar para hacer mejoras y eliminar duplicidad.
\end{itemize}

El primero de este pasos consiste en expresar en forma de código el requisito. De este
modo podemos entender inicialmente un test como un ejemplo o especificación. Para escribir
el test debemos pensar primero en un comportamiento del \acx{SUT} bien definido y en como
comprobaríamos que, efectivamente, hace lo que le pedimos.

Una vez tenemos el ejemplo escrito, debemos escribir únicamente el código necesario para
que el test sea pasado con éxito. Durante esta tarea nos surgirán dudas sobre distintos
comportamiento del \ac{SUT} ante distintas entradas, pero debemos contener el impulso
inicial de codificarlas sobre la marcha, y limitarnos a anotar esas dudas para futuras
especificaciones.

La refactorización consiste en modificar el diseño de modo que el comportamiento
permanezca intacto. Para ello, revisamos el código, incluido el de los test, en busca de
líneas duplicadas y prestando atención en que se cumplan los principios de diseño
establecidos. Con todo esto aplicamos la refactorización oportuna.


\section{Herramientas}

En este apartado se mostrarán las distintas herramientas, tanto hardware como software, que
han intervenido en la elaboración de \ac{LoRAM}.

\subsection{Lenguajes}

Para la elaboración de este proyecto ha sido necesaria la utilización de varios lenguajes
de programación. A continuación se muestra la lista completa de todos ellos:

\begin{definitionlist}
\item[\textbf{Java}] Lenguaje de programación orientado a objetos, desarrollado por Sun
  Microsystems (actualmente propiedad de Oracle
  Corporation\footnote{\url{http://www.oracle.com}}). Este lenguaje se utiliza para el
  desarrollo de los módulos de la aplicación que se ejecutan bajo Android debido a la alta
  integración que existe entre este \ac{SO} y Java.

\item[\textbf{Python}] Lenguaje de programación interpretado, desarrollado por Guido van
  Rossum~\cite{Python}. Este lenguaje se usa para el código del módulo de gestión y para la
  implementación de los test de sistema.

\item[\textbf{Slice}] Lenguaje similar a \emph{C} creado por ZeroC para definir las
  interfaces de los objetos y poder separar estas de la implementación de dichos objetos.
\end{definitionlist}

\subsection{Hardware}
\label{sec:hardware}

Para el desarrollo de \ac{LoRAM} se ha empleado un equipo de sobremesa, usado
principalmente para la realización del código de la aplicación y los test. Este
equipo cuenta con las siguientes características:

\begin{itemize}

\item Intel\textsuperscript{\textregistered} Core\textsuperscript{TM}2 Quad CPU
  Q6600(2.40GHz x 4).

\item 6GB RAM.

\item 1TB Disco Duro.
\end{itemize}

También ha sido necesaria la utilización de un smartphone para realizar algunas pruebas
sobre la plataforma de destino de algún módulo de la aplicación. Las características del
smartphone usado son las siguientes:

\begin{itemize}
\item HTC Desire.

\item Qualcomm Snapdragon QSD8250 1GHz.

\item 576MB RAM.

\item 512MB ROM, microSD 4GB.

\item \ac{GPS}, acelerómetro, Wi-Fi(802.11b/g), Bluetooth 2.1.

\item Versión de Android 2.2.2 Froyo.
\end{itemize}

\subsection{Software}

En el desarrollo de este proyecto han intervenido diferentes herramientas y bibliotecas
software. A continuación se detallarán todas ellas, explicando brevemente para que han
sido utilizadas.


\subsubsection{Sistema operativo:}

\begin{definitionlist}
\item[Debian] Se ha empleado Debian 7.0 (wheezy) 64-bit con núcleo Linux
  3.0.0-1-amd64, instalado en el equipo de sobremesa indicado en la sección
  ~\ref{sec:hardware}, como \ac{SO} principal para la realización de este proyecto.

\item[Android] El \ac{SO} empleado en el smartphone ha sido Android 2.2.2
  (Froyo) por ser la ultima versión actualizada por HTC para este dispositivo.
\end{definitionlist}


\subsubsection{Software de desarrollo:}

\begin{definitionlist}
\item[GNU Emacs] Editor de texto extensible perteneciente al proyecto
  GNU~\cite{emacs}. Este editor se ha usado como \ac{IDE} para la programación en Python
  de los test y del módulo de gestión. También ha sido usado para redactar la
  documentación del proyecto. La versión usada ha sido GNU Emacs 23.4.1.

\item[Eclipse] \ac{IDE} multiplataforma empleado para el desarrollo de los
  módulos de la aplicación que se ejecutan en Android~\cite{Eclipse}. La versión utilizada
  ha sido 3.7.2 (Indigo), a la que se le han añadido los plugins \emph{ADT Plugin} y
  \emph{Slice2Java} para la integración con Android y \ac{Ice} respectivamente.

\item[ZeroC Ice] Middleware de comunicación orientado a objetos creado por
  ZeroC Inc~\cite{ice}. Se ha utilizado \ac{Ice} para realizar la comunicación entre los
  distintos módulos de la aplicación. La versión usada ha sido la 3.4.

\item[Android SDK] Kit de desarrollo de software para
  Android~\cite{AndroidSDK}. Nos ha permitido desarrollar los módulos de la aplicación
  para Android, así como depurarlos y probarlos en un emulador. Se ha empleado la versión
  21 de este \ac{SDK}.

\item[JUNIT] Framework integrado en Eclipse para la realización de test en
  Java~\cite{JUnit}. Se ha empleado para realizar los test unitarios de los módulos
  implementados para Android. La versión empleada ha sido JUNIT 4.

\item[Nose] Herramienta para facilitar la realización de pruebas en
  Python~\cite{Nose}. La versión usada ha sido la 1.1.2.

\item[python-doublex] Framework de dobles de prueba para python creado por David
  Villa~\cite{Doublex}. Se ha utilizado para la realización de los test
  unitarios e integración que verifican el comportamiento del sistema.

\item[Mockito] Framework de dobles de prueba para Java~\cite{Mockito}. Se ha
  utilizado, junto con Junit, para la realización de las pruebas unitarias, de los
  componentes del sistema implementados para Android. La versión empleada ha sido la 1.9.
\end{definitionlist}


\subsubsection{Documentación y gráficos:}

\begin{definitionlist}
\item[\LaTeX] Lenguaje de marcado para la composición de documentos de
  texto~\cite{Latex}. Se ha utilizado para realizar esta memoria.

\item[BibTeX] Herramienta para dar formato a listas de referencias en documentos
  escritos en \LaTeX. Usado para crear las referencias de este documento.

\item[Dia] Programa para dibujar diagramas estructurados. Se ha utilizado en su
  versión 0.97.2 para crear los diagramas que aparecen en algunas figuras de este
  documento.

\item[Gimp] Programa de manipulación de imágenes de GNU. Usado en su versión 2.8
  para crear los gráficos que aparecen en algunas de las figuras de este proyecto.
\end{definitionlist}


\subsubsection{Control de versiones:}

\begin{definitionlist}
\item[Mercurial] Sistema de control de versiones distribuido~\cite{Hg}. Este
  sistema ha sido usado junto a un repositorio que se encuentra en el servidor de
  alojamiento \emph{Bitbucket}\footnote{\url{https://bitbucket.org/}}.
\end{definitionlist}


% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "castellano8"
%   mode: latex
%   TeX-master: "main"
% End:
