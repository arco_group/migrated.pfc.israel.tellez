(TeX-add-style-hook "resultados"
 (lambda ()
    (LaTeX-add-labels
     "chap:resultados"
     "fig:graficoFrenada_30kmh"
     "fig:graficoFrenada_50kmh"
     "fig:log_notificacionIncidente"
     "fig:pantalla_listarUno"
     "fig:pantalla_datosIncidente_NoAsignado"
     "fig:prueba_localizacion"
     "fig:log_activacionControles"
     "tab:costes-HW"
     "tab:sloc-lenguajes"
     "tab:sloc-componentes"
     "tab:costes-desarrollo")
    (TeX-run-style-hooks
     "tables/costes-HW"
     "tables/sloc_lenguajes"
     "tables/sloc_componentes"
     "tables/costes-desarrollo")))

