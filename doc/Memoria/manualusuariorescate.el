(TeX-add-style-hook "manualusuariorescate"
 (lambda ()
    (LaTeX-add-labels
     "chap:manual_usuario_rescate"
     "fig:pantalla_errorNIF"
     "fig:pantalla_listarIncidentes"
     "fig:datosIncidente_asignado"
     "fig:error_localizarNoAsignado"
     "fig:pantalla_controles"
     "fig:pantalla_controlesActivos")))

