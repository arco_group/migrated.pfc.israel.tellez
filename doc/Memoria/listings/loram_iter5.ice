module LoRAM {

  enum ActuatorType { TypeSpeaker, TypeLamp, TypeVibrator, TypeLocation };

  class ActivationRequest {
    Time timeReq;
    Time duration;
    ActuatorType typeReq;
  };

  class DeactivationRequest {
    Time timeReq;
    ActuatorType typeReq;
  };

  interface Speaker {
    void turnOn();
    void turnOff();
  };

  interface Lamp {
    void turnOn();
    void turnOff();
  };

  interface Vibrator {
    void turnOn();
    void turnOff();
  };

  interface RCManager {
    Speaker* getSpeaker() throws NotAvailableActuator;
    Lamp* getLamp() throws NotAvailableActuator;
    Vibrator* getVibrator() throws NotAvailableActuator;
    GPS* getGPS() throws NotAvailableActuator;
  };

  interface RemoteControl {
    void activate(ActivationRequest request) throws NotAvailableActuator, NotSuitable;
    void deactivate(DeactivationRequest request);
    Location getLocation(LocationRequest request) throws NotAvailableActuator,
      ActuatorOff;
  };

};
