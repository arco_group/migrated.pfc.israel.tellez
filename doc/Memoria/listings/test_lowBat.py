def test_activation_with_low_battery(self):
    # Given
    self.levelLow = 15

    with Stub() as self.battery:
        self.battery.getLevel().returns(self.levelLow)

    sut = AccessControl(self.battery)

    vibratorOn = True
    lampOn = False
    speakerOn = False
    actuatorsOn = [speakerOn, lampOn, vibratorOn]

    # When
    test = sut.isSuitableActivate(actuatorsOn)

    # Then
    assert_that(test, is_(False))
