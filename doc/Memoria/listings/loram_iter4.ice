module LoRAM {

  exception GenericError {
    string reason;
  };

  exception ActuatorOff extends GenericError {};
  exception NotAvailableActuator extends GenericError {};
  exception NotExistRCModule extends GenericError {};

  enum LocationType { GPSType, NETWORKType };

  class Location {
    double latitude;
    double longitude;
  };

  class LocationRequest {
    LocationType locType;
    long timeBetweenUpdates;
    long distanceChangeForUpdates;
  };

  interface GPS {
    bool isEnable();
    Location getLocation() throws ActuatorOff;
    void configure(long timeBetweenUpdates, long distanceChangeForUpdates);
  };

  interface RCManager {
    GPS* getGPS() throws NotAvailableActuator;
  };

  interface SentryCommunicator {
    RCManager* getRCManager() throws NotExistRCModule;
    void solveIncident();
  };

  interface RemoteControl {
    Location getLocation(LocationRequest request) throws NotAvailableActuator,
      ActuatorOff;
  };

  interface IncidentManager {
    void notifyIssue(string email, SentryCommunicator* sentry, Issue iss);
    IncidentsList listIncidents(string nif);
    UserData getUserData(string email);
    RemoteControl* getRemoteControl(string email) ImposibleConnectWithUser;

  };

};
