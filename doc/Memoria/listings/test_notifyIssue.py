def test_notify_issue(self):
    # Given
    email = "foo@example.com"
    user = Stub()
    user.email = email

    servantIncidentManager = Mimic(Spy, IncidentManagerI)
    incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

    with Mimic(Stub, UserManagerI) as servant:
        servant.getIncidentManager(email, ANY_ARG).returns(incidentManager)
    manager = self.adapter_add(UserManagerPrx, servant)

    time = TimeC(10, 20, 30)
    date = DateC(25, 5, 2013)

    with Stub() as datesChanger:
        datesChanger.obtainTimeToTimestamp(ANY_ARG).returns(time)
        datesChanger.obtainDateToTimestamp(ANY_ARG).returns(date)

    sut = LoRAM_Agent(manager)
    sut.connected = True
    sut.user = user
    sut.datesChanger = datesChanger

    sut.startRoute()
    servantSentry = Mimic(Stub, SentryCommunicatorI)
    sut.sentry.communicator.proxy = self.adapter_add(SentryCommunicatorPrx, servantSentry)

    # When
    timer = sut.alert()
    timer.expire()

    # Then
    sentryExpected = sut.sentry.communicator.proxy
    issueExpected = IssueC(time, date, IssueType.AlertButtonPressed)
    assert_that(servantIncidentManager.notifyIssue, \
                        called().with_args(email, sentryExpected, issueExpected, ANY_ARG))
