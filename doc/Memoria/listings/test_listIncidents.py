def test_list_incidents(self):
    # Given
    nif = "11223344a"
    password = "1234"
    rescuer = RescuerDataC(nif, "name", "surname1", "surname2", 123456789, 10)

    servantIncidentManager = Mimic(Spy, IncidentManagerI)
    incidentManager = self.adapter_add(IncidentManagerPrx, servantIncidentManager)

    with Mimic(Stub, UserManagerI) as servant:
        servant.loginRescuer(nif, password, ANY_ARG).returns(True)
        servant.getLoggedRescuerData(nif, ANY_ARG).returns(rescuer)
        servant.getIncidentManager(nif, ANY_ARG).returns(incidentManager)
    manager = self.adapter_add(UserManagerPrx, servant)

    sut = LoRAM_Rescue(manager)

    # When
    sut.loginRescuer(nif, password)
    sut.querySystem.showIncidents()


    # Then
    assert_that(servantIncidentManager.listIncidents, called().with_args(rescuer.nif, \
                                                                                 ANY_ARG))
