package israel.androidserver;

import Example.HelloPrx;
import Example.HelloPrxHelper;
import Example._HelloDisp;
import Ice.Communicator;
import Ice.Current;
import Ice.ObjectAdapter;

public class HelloI extends _HelloDisp {

   private HelloPrx _proxy;
   
   private String _message;
   
   public HelloI() {
      this._proxy = null;
   }
   
   public HelloPrx add_to (Communicator broker, ObjectAdapter adapter) {
      _proxy = HelloPrxHelper.uncheckedCast(adapter.add(this, broker.stringToIdentity("hello")));
      
      return _proxy;
   }
   
   @Override
   public void puts(String message, Current __current) {
      _message = message;
   }

   public String get_message() {
      return _message;
   }
   

}
