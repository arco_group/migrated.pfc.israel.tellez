def test_get_victim_location(self):
    # Given
    request = Stub()
    request.locType = LocationType.GPSType
    request.timeBetweenUpdates = 15000
    request.distanceChangeForUpdates = 0
    location = LocationC(25, 42.2)

    with Mimic(Spy, GPSI) as servantGPS:
        servantGPS.getLocation(ANY_ARG).returns(location)
    gps = self.adapter_add(GPSPrx, servantGPS)

    with Mimic(Spy, RCManagerI) as servantRCManager:
        servantRCManager.getGPS(ANY_ARG).returns(gps)
    rcManager = self.adapter_add(RCManagerPrx, servantRCManager)

    with Mimic(Stub, SentryCommunicatorI) as servantSentry:
        servantSentry.getRCManager(ANY_ARG).returns(rcManager)
    sentry = self.adapter_add(SentryCommunicatorPrx, servantSentry)

    with Stub() as accessControl:
        accessControl.isSuitableGetLocation(ANY_ARG).returns(True)

    sut = RemoteControlI(sentry)

    sut.accessC = accessControl

    # When
    test1 = sut.getLocation(request)

    # Then
    assert_that(servantRCManager.getGPS, called())
    assert_that(servantGPS.getLocation, called())
    assert_that(test1, is_(location))
