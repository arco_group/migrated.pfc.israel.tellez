module LoRAM {

  enum Level { High, Medium, Low };

  class SpeakerActivationRequest extends ActivationRequest {
    float volume;
    float rate;
  };

  class LampActivationRequest extends ActivationRequest {
    Level brightness;
  };

  class VibratorActivationRequest extends ActivationRequest {
    Level frequency;
  };

  interface Speaker {
    void turnOn();
    void turnOff();
    void configure(float volume, float rate);
  };

  interface Lamp {
    void turnOn();
    void turnOff();
    void configure(Level brightness);
  };

  interface Vibrator {
    void turnOn();
    void turnOff();
    void configure(Level frequency);
  };
};
