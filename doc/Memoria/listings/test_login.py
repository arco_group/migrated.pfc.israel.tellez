def test_login_accepted(self):
    # Given
    email = "foo@example.com"
    password = "1234"
    user = UserDataC(email, "name", "surname1", "surname2", 123456789, 987654321)

    with Mimic(Spy, UserManagerI) as servant:
        servant.login(email, password, ANY_ARG).returns(True)
        servant.getLoggedUserData(email, ANY_ARG).returns(user)
    manager = self.adapter_add(UserManagerPrx, servant)

    sut = LoRAM_Agent(manager)

    # When
    sut.login(email, password)

    # Then
    assert_that(servant.login, called().with_args(email, password, ANY_ARG))
    assert_that(sut.connected, is_(True))
    assert_that(servant.getLoggedUserData, called().with_args(email, ANY_ARG))
    assert_that(sut.user, is_(user))
