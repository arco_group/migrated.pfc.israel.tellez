package israel.androidclient;

import Example.HelloPrx;
import Example.HelloPrxHelper;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

   private Ice.Communicator _broker;
   private HelloPrx _hello;

   private EditText _txvMessage;
   private Button _btnSend;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      //Activity controllers
      _txvMessage = (EditText)findViewById(R.id.txvMessage);
      _btnSend = (Button)findViewById(R.id.btnSend);

      //Communication setting
      if (VERSION.SDK_INT == 8) {// android.os.Build.VERSION_CODES.FROYO (8)
         //
         // Workaround for a bug in Android 2.2 (Froyo).
         //
         // See http://code.google.com/p/android/issues/detail?id=9431
         //

         java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
         java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
      }

      initializeCommunicator();

      _hello = this.createProxy();

      //Controllers setting
      _btnSend.setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            String message = _txvMessage.getText().toString();
            _hello.puts(message);
         }
      });
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.activity_main, menu);
      return true;
   }

   @Override
   public void onDestroy() {
      super.onDestroy();

      if(_broker != null) {
         try {
            _broker.destroy();
         }
         catch(Ice.LocalException e) {
            e.printStackTrace();
         }
      }
   }

   private void initializeCommunicator() {
      try {
         _broker = Ice.Util.initialize();
      } catch(Ice.LocalException e) {
         e.printStackTrace();
      } catch (Exception e) {
         System.err.println(e.getMessage());
      }
   }

   private HelloPrx createProxy() {
      //replace IP with the server's IP address
      String s =  "hello -t:tcp -h 192.168.1.10 -p 10000";
      Ice.ObjectPrx base = _broker.stringToProxy(s);

      return HelloPrxHelper.checkedCast(base);
   }
}
