module LoRAM {

  exception UserNotFound extends GenericError {};
  exception UserNotLogged extends GenericError {};
  exception AccessDenied extends GenericError {};
  exception UserAlreadyExists extends GenericError {};

  class RescuerData {
    string nif;
    string name;
    string surname1;
    string surname2;
    int phone;
    int team;
  };

  interface UserManager {
    bool login(string email, string password) throws UserNotFound;
    bool loginRescuer(string nif, string password) throws UserNotFound;
    void logout(string identification);
    void register(UserData user, string password) throws UserAlreadyExists;
    void registerRescuer(RescuerData user, string password) throws UserAlreadyExists;
    UserData getLoggedUserData(string email) throws UserNotLogged;
    RescuerData getLoggedRescuerData(string nif) throws UserNotLogged;
    IncidentManager* getIncidentManager(string identification) throws AccessDenied;
  };



};
