module LoRAM {

  enum IssueType { DetectedFall, DetectedCollision, Motion, AlertButtonPressed, UnknownIssue };

  class Date {
    int day;
    int month;
    int year;
  };

  class Time {
    int hour;
    int minute;
    int second;
  };

  class Issue {
    Time timeIss;
    Date dateIss;
    IssueType typeIss;
  };

  interface SentryCommunicator {
    void solveIncident();
  };

  interface IncidentManager {
    void notifyIssue(string user, SentryCommunicator* sentry, Issue iss);
  };

};
