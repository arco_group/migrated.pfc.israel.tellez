package israel.androidserver;

import Example.HelloPrx;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

   private Ice.Communicator _broker;
   private Ice.ObjectAdapter _adapter;
   
   private HelloI _hello;
   
   private TextView _txvMessage;
   private Button _btnCheck;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      
      //Activity controllers
      _txvMessage = (TextView)findViewById(R.id.txvMessage);
      _btnCheck = (Button)findViewById(R.id.btnCheck);
      
      //Communication setting
      if (VERSION.SDK_INT == 8) {// android.os.Build.VERSION_CODES.FROYO (8)
         //
         // Workaround for a bug in Android 2.2 (Froyo).
         //
         // See http://code.google.com/p/android/issues/detail?id=9431
         //
                                 
         java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
         java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
      }
      
      initializeCommunicator();
      
      _hello = new HelloI();
      _hello.add_to(_broker, _adapter);
      
      //Controllers setting
      _btnCheck.setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            _txvMessage.setText(_hello.get_message());
         }
      });
      
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.activity_main, menu);
      return true;
   }
   
   @Override
   public void onDestroy() {
      super.onDestroy();
      
      if(_broker != null) {
         try {
            _broker.destroy();
         }
         catch(Ice.LocalException e) {
            e.printStackTrace();
         }
      }
   }
   
   private void initializeCommunicator() {
      try {
         _broker = Ice.Util.initialize();
         _adapter = _broker.createObjectAdapterWithEndpoints("adapter", "tcp -p 10000");
         
         _adapter.activate();
      } catch(Ice.LocalException e) {
         e.printStackTrace();
      } catch (Exception e) {
         System.err.println(e.getMessage());
      }
   }
}
