module LoRAM {

  class UserData {
    string email;
    string name;
    string surname1;
    string surname2;
    int phone;
    int numAA;
  };

  interface IncidentManager {
    void notifyIssue(UserData user, SentryCommunicator* sentry, Issue iss);
    IncidentsList listIncidents(string rescuer);
    UserData getUserData(string email);
  };

};
