module LoRAM {

  enum StateType { NoAllocate, Allocated, Resolved, UnknownState };

  class IncidentData {
    int idIncident;
    string victim;
    Date dateIncident;
    Time timeIncident;
    StateType state;
    IssueType typeIncident;
    int rescueTeam;
  };

  sequence<IncidentData> IncidentsList;

  interface IncidentManager {
    void notifyIssue(string user, SentryCommunicator* sentry, Issue iss);
    IncidentsList listIncidents(string rescuer);
  };

};
