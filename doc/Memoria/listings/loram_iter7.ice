module LoRAM {

  interface Battery {
    float getLevel();
  };

  interface RCManager {
    Speaker* getSpeaker() throws NotAvailableActuator;
    Lamp* getLamp() throws NotAvailableActuator;
    Vibrator* getVibrator() throws NotAvailableActuator;
    GPS* getGPS() throws NotAvailableActuator;
    Battery* getBattery();
  };

};
