(TeX-add-style-hook "antecedentes"
 (lambda ()
    (LaTeX-add-labels
     "chap:antecedentes"
     "sec:arq_objetos_distribuidos"
     "fig:distribuidos"
     "fig:icecs"
     "fig:ejecucionandroid"
     "sec:activities"
     "sec:services"
     "tab:android-sensors"
     "fig:funcionamiento_eCall"
     "fig:Smartphone_ACN"
     "sec:wreckWatch"
     "fig:arqWreckWatch"
     "fig:experiment_Acceleration")
    (TeX-run-style-hooks
     "tables/android-sensors")))

