(TeX-add-style-hook "manualusuario"
 (lambda ()
    (LaTeX-add-labels
     "chap:manual_usuario"
     "fig:pantalla_login"
     "fig:pantalla_registro"
     "fig:pantalla_iniciarRuta"
     "fig:pantalla_enRuta"
     "fig:pantalla_confirmar"
     "fig:pantalla_asistencia"
     "fig:dialogo_cerrarSesion")))

