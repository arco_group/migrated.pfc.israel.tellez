\chapter{Antecedentes}
\label{chap:antecedentes}

\drop{E}{}ste capítulo tiene como finalidad explicar al lector los
distintos componentes y tecnologías que han tenido lugar en el desarrollo
de este proyecto fin de carrera. Debido a la escasa existencia de
sistemas similares a \ac{LoRAM}, basaremos el estudio del estado del arte en
mostrar los diferentes aspectos que han tenido lugar para su desarrollo.


\section{Aplicaciones distribuidas}

Una aplicación distribuida es aquella en la que existen diferentes
componentes, los cuales pueden ser ejecutados en distintas plataformas
conectadas a una misma red. La comunicación mas habitual es la que
presenta la arquitectura de dos niveles \ac{C/S}, aunque podemos
encontrar arquitecturas con una división de más niveles.

Con el término distribución nos referimos a software construido por
varias partes, donde a cada una se le asigna un conjunto específico
de responsabilidades dentro del sistema. Para poder realizar una
separación física del sistema, se debe tener clara la separación
lógica del mismo. Así podemos distinguir entre separación lógica
<<capas>> y física <<niveles>>.

La arquitectura de una aplicación distribuida, según su funcionalidad,
presenta tres grupos de elementos:

\begin{itemize}
\item Capa de servidor: se encarga de recibir peticiones de acceso a
  servicios o a datos y de suministrar esta información al solicitante.

\item Capa de negocio: gestiona los procesos internos y el acceso a
  los datos.

\item Capa de presentación: constituye los mecanismos necesarios para
  hacer posible la interación con el usuario.
\end{itemize}

A la hora de realizar una aplicación distribuida es importante tener
en cuenta varios aspectos como la topología de la red, concurrencia o
seguridad, ya que será necesario controlar los tiempos de respuesta y
proporcionar mecanismos que impidan accesos no autorizados.


\section{Arquitectura de objetos distribuidos}
\label{sec:arq_objetos_distribuidos}

Una arquitectura de objetos distribuidos~\cite{sommerville} está
compuesta por objetos que proveen servicios a través de una interfaz
que ellos mismos facilitan. A diferencia de las arquitecturas \ac{C/S}
no existe una distinción lógica entre el proveedor y el receptor del
servicio (ver figura~\ref{fig:distribuidos}).

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{/Arquitectura_objetos_distribuidos.png}
    \caption{Arquitectura de objetos distribuidos}
    \label{fig:distribuidos}
  \end{center}
\end{figure}

Un middleware conocido como \emph{intermediario de peticiones de
  objetos} será el encargado de proporcionar los servicios necesarios
para realizar la comunicación entre objetos. Además dicho middleware
permitirá a los objetos ser añadidos o eliminados del sistema.

Existen varias alternativas hoy en día que se ajustan a esta
arquitectura. Así tenemos middleware orientados a objetos como
\emph{\ac{CORBA}} o \emph{\ac{RMI}} por citar algunos. A continuación
pasamos a estudiar \emph{ZeroC Ice} por ser el escogido para la
realización de este proyecto.


\subsection{ZeroC Ice}

El middleware \acx{Ice}~\cite{CDHconIce} desarrollado por
ZeroC, Inc.\footnote{\url{http://www.zeroc.com}}, nos permite realizar una
comunicación de objetos distribuidos. Este middleware está diseñado
para soportar varios lenguajes de programación como C, Java o Python
entre otros. Además esta disponible para múltiples plataformas entre
las que se encuentran GNU/Linux, Windows o Android. Todo esto lo
convierte en una opción muy interesante a la hora de crear un sistema
distribuido de objetos, dada su amplia capacidad de adaptación.

En \ac{Ice} podemos destacar las siguientes ventajas:

\begin{itemize}
\item Al utilizar la misma implementación para todos los lenguajes no
  se añade complejidad al desarrollo de un proyecto multi-lenguaje.

\item Los detalles de configuración de las comunicaciones son
  completamente ortogonales al desarrollo del software.

\item Presenta una interfaz sencilla, en la que el significado de las
  operaciones se deduce fácilmente.
\end{itemize}

Para realizar una comunicación entre dos partes de una aplicación una
debe asumir el rol de cliente y la otra el de servidor\footnote{Estos
  roles pueden ser intercambiados entre las distintas partes durante
  la comunicación}. La parte que asuma el papel de cliente solicitará
servicios mediante la invocación a métodos de objetos remotos, y la
parte con el rol de servidor proporcionara servicios como respuesta a
las solicitudes de los clientes. En la figura~\ref{fig:icecs} se
muestra el aspecto de esta estructura en \ac{Ice}.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{/Ice_C_S_Structure.png}
    \caption{Estructura C/S en Ice}
    \label{fig:icecs}
  \end{center}
\end{figure}

Para comprender el funcionamiento de \ac{Ice} se necesitan conocer
una serie de conceptos que detallaremos en las siguientes líneas.


\subsubsection{Objeto}

En \ac{Ice} un objeto es una entidad conceptual que tiene un tipo,
una identidad e información de direccionamiento. De forma específica
podemos caracterizarlo por los siguientes puntos:

\begin{itemize}
\item Es una entidad en el espacio de direcciones remoto o local capaz
  de responder a las peticiones de los clientes.

\item Un único objeto \ac{Ice} puede ser instanciado en un único
  servidor o de manera redundante, en múltiples servidores.

\item Cada objeto tiene una o mas interfaces. Una interfaz es una
  colección de operaciones soportadas por un objeto.

\item Una operación tiene cero o mas parámetros y un valor de
  retorno. Parámetros y valores de retorno tienen un tipo específico,
  y los parámetros además tienen una determinada dirección.

\item Un objeto \ac{Ice} tiene una \emph{interfaz principal}
  diferenciada del resto. Además, puede proporcionar cero o más
  interfaces alternativas excluyentes, conocidas como \emph{facets}.

\item Cada objeto tiene una identidad de objeto única que lo distingue
  del resto de objetos.
\end{itemize}


\subsubsection{Proxy}

Cuando hablamos de un proxy en \ac{Ice} nos referimos a un
representante local de un objeto (generalmente remoto) en el espacio
de direcciones del cliente. \ac{Ice} soporta varios tipos de proxies
para cubrir diferentes situaciones:

\begin{itemize}
\item \textbf{Proxies textuales}, la información asociada se
  representa como una cadena.

\item \textbf{Proxies directos}, encapsulan una identidad de objeto
  junto con la dirección asociada a su servidor.

\item \textbf{Proxies indirectos}, pueden proporcionar sólo la
  identidad de un objeto, o bien pueden especificar una identidad
  junto con un identificador de adaptador de objetos.

\item \textbf{Proxies fijos}, están asociados a una conexión en
  particular. El proxy contiene un manejador de conexión.
\end{itemize}


\subsubsection{Sirviente}

Un sirviente es el componente de la parte del servidor encargado de
proporcionar el comportamiento asociado a la invocación de
operaciones. Se trata de una instancia de una clase registrada en el
núcleo de ejecución del servidor como sirviente para uno o varios
objetos. Las operaciones de la interfaz del objeto \ac{Ice} se
corresponden con los métodos de esta clase, que son los
encargados de proporcionar el comportamiento de dichas operaciones.

Un único objeto distribuido puede tener múltiples sirvientes, pero
también un sirviente puede encarnar uno o varios objetos. La
diferencia radica en que si un sirviente encarna a un único objeto, la
identidad de este objeto estará implícita en el sirviente, pero si el
sirviente encarna varios objetos, el sirviente mantendrá la identidad
del objeto con cada solicitud y así podrá decir que objeto encarnar.


\subsubsection{Slice}

El lenguaje \acx{Slice} constituye el mecanismo de abstracción
fundamental para la separación de las interfaces de objetos de sus
implementaciones. \ac{Slice} establece un contrato entre el cliente y
el servidor en el que se describen los tipos de objetos e interfaces
utilizados por una aplicación. Esta descripción es independiente del
lenguaje de implementación, por lo que no importa si el cliente está
escrito en el mismo lenguaje que el servidor. En el
listado de código~\ref{code:ejslice} se muestra un ejemplo de interfaz
en \ac{Slice}.

\begin{listing}[
  float=ht,
  language = C,
  caption = {Ejemplo de interfaz en \ac{Slice}},
  label = code:ejslice]
module Example {
  interface Hello {
    void puts(string message);
  };
};
\end{listing}

Una descripción más amplia de este lenguaje la podemos encontrar
en el manual de \ac{Ice}~\cite{icemanual}.


\subsubsection{Language Mappings}

Se conoce por \emph{language mappings} a las reglas que rigen la forma
en que cada construcción \ac{Slice} se traduce a un lenguaje de
programación específico. Estas reglas son simples y regulares,
haciendo innecesaria la lectura del código generado para utilizar la
\ac{API} que se genera.


\section{El \acl{SO} Android}

\emph{Android}\footnote{\url{http://www.android.com}} es un \ac{SO} basado
en Linux propiedad de la empresa Google y apoyado por la \emph{Open
  Handset  Alliance}. Fue diseñado para trabajar sobre diferentes
dispositivos (Tablets, móviles, reproductores
multimedia,~\...) y actualmente se encuentra en la versión 4.2 <<Jelly
Bean>>.

En Android las aplicaciones se programan en Java, y son empaquetadas
en paquetes <<.apk>>. Cada aplicación se ejecuta en un proceso
independiente al que se le asocia una máquina virtual conocida como
\emph{Dalvik}~\cite{AndroidAPIGuides}. En la
figura~\ref{fig:ejecucionandroid} podemos ver un esquema sobre como se
ejecutan las aplicaciones en Android.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{/Ejecucion_Android.png}
    \caption{Estructura de ejecución de aplicaciones en Android}
    \label{fig:ejecucionandroid}
  \end{center}
\end{figure}

Este tipo de ejecución presenta importantes ventajas, como por
ejemplo, si una aplicación se bloquea, el resto del sistema no se verá
afectado y su máquina virtual podrá ser reiniciada. El sistema podrá
gestionar la carga de aplicaciones en tiempo real, decidiendo cuales
permanecerán en memoria y cuales serán eliminadas. Además cada
aplicación será la encargada de gestionar sus propios datos decidiendo
que información ofrecer en cada momento.


\subsection{Componentes de una aplicación}

Los componentes nos ayudan a definir el comportamiento global de una
aplicación en Android. Cada uno de ellos constituye una entidad propia
y desempeña un papel específico.

En Android cada uno de los componentes definidos por una aplicación
podrá ser utilizado por otras aplicaciones. Esto es posible gracias a
que no se define un punto único de entrada a la aplicación, a
diferencia de otros sistemas donde debe existir un <<\emph{main}>>.

Los componentes de una aplicación Android se pueden clasificar en
cuatro tipos con diferentes propósitos y ciclos de vida.


\subsubsection{Activities}
\label{sec:activities}

Una \emph{Activity} representa una pantalla con una interfaz de
usuario. Las aplicaciones pueden estar compuestas de varias
activities, pero sólo una de ellas será la cargada al iniciar la
aplicación, considerada \emph{activity} <<principal>>.

En algunas ocasiones, una \emph{activity} puede estar compuesta por
\emph{fragments}. Un \emph{fragment} representa una porción de la
interfaz de usuario en una \emph{activity}, de modo que se pueden
combinar múltiples \emph{fragment} para formar una
\emph{activity}. Para hacer uso de este componente se necesita la
\ac{API} level 11 de Android.

%Con el fin de realizar diferentes acciones, una \emph{activity}
%puede iniciar otra \emph{activity}. Cuando esto sucede la
%\emph{activity} anterior se detiene y pasa a una pila conocida como
%<<back stack>>. Esta pila funciona como una \acx{LIFO} de modo que
%cuando el usuario presiona el botón <<atrás>> se destruye la
%\emph{activiy} actual y se carga la anterior.


\subsubsection{Services}
\label{sec:services}

Un servicio es un componente sin interfaz de usuario que se ejecuta
en segundo plano <<background>>. Son similares a los servicios
presentes en otros sistemas, como por ejemplo los demonios en
Linux. Otros componentes como las \emph{Activities} pueden iniciar
estos servicios o unirse a alguno en ejecución.


\subsubsection{Content providers}

Gestionan un conjunto de datos de aplicación compartidos. Estos datos
pueden estar almacenados en una base de datos SQLite, en la web, o en
cualquier otro lugar de almacenamiento persistente al que pueda
acceder la aplicación. A través del \emph{proveedor de contenidos}
(content provider) otras aplicaciones podrán consultar o modificar los
datos dependiendo de los permisos que se le otorguen. Android nos
ofrece algunos \emph{proveedores de contenidos}, como por ejemplo el
que gestiona la información de contacto del usuario o la agenda,
aunque también podemos crear nuestro propio \emph{content provider} y
ofrecerlo al sistema.


\subsubsection{Broadcast receivers}

Un \emph{broadcast receiver} es un componente sin interfaz de usuario
que reacciona a los anuncios \emph{broadcast} del sistema. Estos
anuncios nos informan de que la batería está baja, que se ha capturado
una imagen, etc. Las aplicaciones de usuario también pueden iniciar
anuncios broadcast para informar de algún evento a otras
aplicaciones.

En la práctica las aplicaciones pueden tener varios \emph{broadcast
  receiver}. Cada uno de ellos deberá implementarse como una subclase
de \emph{BroadcastReceiver}, y cada mensaje de broadcast le llegará
como un objeto \emph{Intent}.


\subsection{Intents}

Un \emph{Intent} es un mensaje asíncrono que nos permite activar
otros componentes, ya sean de la misma aplicación que lo solicita o de
otra. Los componentes que nos permite activar son los de los tipos
\emph{Activities, Services o Broadcast receivers}.

Para las \emph{Activities y Services}, un \emph{intent} define la
acción a realizar y puede especificar una \ac{URI} de datos
necesarios.

Para los \emph{Broadcast receivers} el \emph{intent} simplemente define
el tipo de broadcast que quiere recibir.


%\section{Desarrollo de Apps en Android}
% Que es un app
% Hablar sobre los distintos estados de ejecución de las aplicaciones
% (OnStop, OnPause ...). Poner esquemas de ciclo de vida


\section{Ice for Android}

\emph{Ice for Android}~\cite{IceforAndroid} presenta una solución para
crear aplicaciones distribuidas orientadas a objetos para plataformas
móviles que usan el \ac{SO} Android. Soporta las mismas \ac{API}s que
\emph{Ice for Java} lo que facilita la reutilización y la experiencia.

Para mejorar la productividad ZeroC ha creado \emph{Slice2Java}, un
plug-in de Eclipse\footnote{\url{http://www.eclipse.org}} que
automatiza la traducción de los ficheros \emph{Slice} y gestiona el
código resultante generado. El uso de este \ac{IDE} se debe a la
estrecha integración que tiene con las herramientas de desarrollo
ofrecidas por Android. Para descargar e instalar este plug-in podemos
consultar la siguiente dirección \url{http://www.zeroc.com/eclipse.html}.

En el Anexo~\ref{chap:iceandroid} podemos encontrar un ejemplo
sencillo sobre el uso de \ac{Ice} en Android.


\section{Sensores en smartphone con Android}

Un \emph{smartphone} o teléfono inteligente, es un teléfono móvil que
incorpora un \ac{SO} y una tecnología más compleja que dota al teléfono
de más funciones de conectividad y capacidades de cómputo más avanzadas.

Los smartphone incorporan una serie de sensores, cada vez
más amplia, que nos permiten realizar aplicaciones capaces de detectar
determinadas circunstancias, y realizar una serie de acciones. El
\ac{SO} Android da soporte a tres grandes categorías de
sensores~\cite{AndroidSensors}:

\begin{itemize}
\item \textbf{Sensores de movimiento:} miden fuerzas de rotación y
  aceleración sobre los tres ejes.

\item \textbf{Sensores ambientales:} miden parámetros ambientales como
  la temperatura del aire, la presión, la iluminación y la
  humedad.

\item \textbf{Sensores de posición:} miden la posición física del
  dispositivo.
\end{itemize}

En el cuadro~\ref{tab:android-sensors} podemos ver la lista completa de sensores
compatibles con Android.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/android-sensors.tex}
  }
  \caption[Sensores compatibles con Android]
  {Sensores compatibles con Android
    (\textsc{Android Developers}~\cite{AndroidSensors})}
  \label{tab:android-sensors}
\end{table}

En Android el framework que nos permite acceder a los sensores forma
parte del paquete \emph{android.hardware}. Este framework nos permite
determinar el tiempo de ejecución de los sensores en el dispositivo,
así como determinar las capacidades de cada sensor, como su alcance
máximo, requisitos de energía y resolución.

Para usar un sensor en Android primero es preciso identificarlo, por
lo que crearemos una instancia de \emph{SensorManager} y después
obtendremos la lista de sensores del dispositivo mediante una llamada
a \emph{getSensorList()}. A este método podremos indicarle si deseamos
obtener la lista completa de sensores <<TYPE\_ALL>> o solo los de un
tipo específico, usando la constante adecuada. También podemos obtener
un sensor de un tipo específico llamando al método
\emph{getDefaultSensor()}, lo que nos devolverá un sensor
predeterminado para el tipo indicado o \emph{null} si no existe un
sensor para ese tipo. Finalmente para obtener los datos del sensor
debemos registrar un \emph{SensorEventListener} para el sensor
específico. En el listado~\ref{code:ejSensor} se muestra
como crear una clase que obtenga un sensor acelerómetro y se registre
para obtener cambios en él.

\begin{listing}[
  float=ht,
  language = Java,
  caption = {Ejemplo de obtención de sensor acelerómetro en Android},
  label = code:ejSensor]
public class AccelerometerSensor implements SensorEventListener {

  private SensorManager sM;
  private Sensor sA;

  public AccelerometerSensor(Context ctx) {
    sM = (SensorManager)ctx.getSystemService(Context.SENSOR_SERVICE):
    sA = sM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    sM.registerListener(this, sA, SensorManager.SENSOR_DELAY_NORMAL);
  }

  @Override
  public final void onAccuracyChanged(Sensor sensor, int accuracy) {
    // Hacer algo si se produce un cambio de precision en el sensor
  }

  @Override
  public final void onSensorChanged(SensorEvent event) {
    // Hacer algo si el sensor recoge un nuevo valor
  }
}
\end{listing}

Como se puede observar en el listado~\ref{code:ejSensor} la interfaz
\emph{SensorEventListener} requiere la implementación de los métodos
\emph{onAccuracyChanged()} y \emph{onSensorChanged()}. El primero nos
indica un cambio en la precisión del sensor, y el segundo un nuevo
valor recogido por el sensor.


\section{Sistemas de localización}

Los sistemas de localización~\cite{ORSI} son aquellos que nos permiten determinar
la posición geográfica de un objeto móvil o inmóvil, mediante una
serie de tecnologías de posicionamiento.

Para que la localización sea posible es preciso contar con un sistema
de referencia que nos permita determinar una posición sobre un
plano. De forma general, el sistema más extendido es el \emph{sistema de
  coordenadas geográficas}, que se basa en dos coordenadas
(\emph{latitud y longitud}) para determinar la posición de un punto
sobre la superficie terrestre.

Según las tecnologías empleadas para realizar la localización podemos
diferenciar entre dos tipos de sistemas de localización, basándonos en
el alcance de su ámbito de actuación:

\begin{itemize}
\item \textbf{\emph{outdoor} (exteriores)}: Son usados para
  localización en grandes áreas geográficas. En cuanto a las
  tecnologías empleadas encontramos sistemas basados en satélite como
  el \ac{GPS} (Estados Unidos), GALILEO (Unión Europea), o basados en
  redes celulares de largo alcance como \acx{GSM}.
\item \textbf{\emph{indoor} (Interiores)}: Están destinados a
  localización en pequeñas áreas geográficas, o para interiores de
  edificios. Los \acx{ILS} se utilizan principalmente para implantar
  sistemas \ac{RTLS} mediante el uso de tecnologías inalámbricas de
  corto alcance como Wi-Fi, Bluetooth, \ac{RFID} o Zigbee.
\end{itemize}


\subsection{Localización en Android}

La localización en Android~\cite{AndroidLocation} se realiza mediante el paquete
\emph{android.location}\footnote{\url{http://developer.android.com/reference/android/location/package-summary.html}},
que proporciona las \ac{API}s necesarias para determinar la ubicación
geográfica del usuario. Cuando se desarrolla una aplicación para
determinar la ubicación en Android se pueden emplear dos mecanismos:

\begin{itemize}
\item \acx{GPS}\footnote{\url{http://www.gps.gov/}}: es el más
  preciso, pero conlleva un enorme gasto de batería y solo funciona en
  entornos \emph{outdoor}.

\item Network Location Provider: hace uso de antenas celulares y
  señales Wi-Fi. Proporciona información tanto \emph{indoor} como
  \emph{outdoor}, pero en áreas interurbanas es poco preciso.
\end{itemize}

Para obtener la ubicación de un usuario en Android primero debemos
solicitar al sistema una instancia del servicio
\emph{LocationManager}. Posteriormente, debemos indicar a este servicio
que deseamos recibir actualizaciones de ubicación, para lo que
deberemos hacer una llamada a \emph{requestLocationUpdates()}
pasándole un objeto \emph{LocationListener}. Este
\emph{LocationListener} deberá ser implementado definiendo una serie
de métodos que el \emph{LocationManager} llama cuando cambia la
ubicación del usuario. En el listado~\ref{code:ejlocation} se muestra
un ejemplo de petición de localización mediante \ac{GPS} con un
intervalo de 500ms entre actualizaciones de localización. En este
ejemplo se recoge la latitud y longitud, pero no se hace nada con
ellas.

\begin{listing}[
  float=ht,
  language = Java,
  caption = {Ejemplo de petición de localización en Android con \ac{GPS}},
  label = code:ejlocation]
Context ctx = getApplicationContext();
LocationManager lM;
LocationListener lL;

lM =(LocationManager)ctx.getSystemService(Context.LOCATION_SERVICE);

lL = new LocationListener() {
  public void onLocationChanged(Location location){
    double lat = location.getLatitude();
    double lon = location.getLongitude();
  }

  public void onProviderDisabled(String provider) {}

  public void onProviderEnabled(String provider) {}

  public void onStatusChanged(String provider, int status, Bundle
      extras) {}

};

lM.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, lL);
\end{listing}

Para que sea posible la utilización de la localización \emph{GPS} es
necesario añadir el permiso adecuado en el fichero
\emph{AndroidManifest.xml} (ver listado~\ref{code:permisosGPS}).

\begin{listing}[
  float=ht,
  language = XML,
  caption = {Permisos necesarios para uso de \ac{GPS} en Android},
  label = code:permisosGPS]
<manifest ...>
  <uses-permission
      android:name="android.permission.ACCESS_FINE_LOCATION"/>
</manifest>
\end{listing}


\section{Sistemas \acs{RC}}

Los sistemas \acx{RC} son aquellos que nos permiten controlar a
distancia uno o varios dispositivos que pueden estar dotados de una
serie de sensores y actuadores. En estos sistemas intervienen dos
partes: una que envía órdenes y otra que las recibe y actúa en
consecuencia. Por lo que tenemos una comunicación entre ambas
emisor/receptor.

La parte que envía las órdenes contendrá un panel de control para
permitir activar componentes actuadores o modificar su
comportamiento. También puede incluir alertas o indicadores que nos
permitan conocer el estado de algún sensor.

Por otro lado la parte que recibe las ordenes estará compuesta por una
serie de sensores y actuadores a los que tendrá acceso. Cuando reciba
una petición se pondrá en contacto con estos para realizar la acción
necesaria.

Existe una amplia gama de sistemas destinados al control remoto de
dispositivos. Estos pueden ir desde algo tan cotidiano como el
mando a distancia de una televisión o un coche teledirigido, hasta
sistemas más complejos que nos permitan controlar un \ac{PC} o un
smartphone a distancia.


\subsection{Cerberus}

En cuanto a sistemas \ac{RC} orientados a smartphone podemos destacar
\emph{Cerberus}\footnote{\url{https://www.cerberusapp.com/}}.
Cerberus~\cite{Cerberus} es un \emph{app} disponible para Android cuya
funcionalidad esta orientada a la recuperación de terminales Android
perdidos o robados. Permite el control remoto del dispositivo de dos
formas diferentes:

\begin{itemize}
\item Mediante Internet accediendo desde la página web de Cerberus.

\item Mediante mensajes de texto \ac{SMS}.
\end{itemize}

La aplicación cuenta con una función conocida como <<SIM Checker>> que
nos informa del nuevo número que tendría el teléfono en el caso de que
la tarjeta \ac{SIM} sea cambiada, de modo que la manipulación del
dispositivo mediante mensajes \ac{SMS} siga siendo posible.

El control remoto de Cerberus permite realizar una gran variedad de
acciones en el dispositivo, entre las que podemos destacar algunas
como la localización del dispositivo, activación de una alarma,
grabación de audio con el micrófono o la obtención de las últimas
llamadas realizadas o recibidas.


\section{Detección automática de accidentes}

El tiempo que transcurre desde que se produce un accidente hasta el
momento en que la víctima es localizada y atendida, es fundamental
para que la intervención de los servicios de socorro tenga éxito. Por
ello, es importante reducir este tiempo el máximo posible con el fin
de minimizar las consecuencias del accidente en los heridos.

Una de las formas de reducir este tiempo de respuesta es mediante el
uso de los sistemas \acx{ACN}. Estos sistemas son capaces de detectar
cuando se produce un accidente y de alertar al personal de emergencia
de dicho accidente. Uno de los sistemas \ac{ACN} más famosos es
\emph{OnStar}. En~\cite{JulieLahausse} se realiza un estudio sobre como los
\ac{ACN} pueden ayudar a reducir la mortalidad en accidentes de
tráfico.

OnStar~\cite{OnStar} es un sistema \ac{ACN} perteneciente a la empresa
americana General Motors LLC. Este sistema proporciona servicios como
respuesta automática ante choques, navegación y asistencia en
carretera. Su sistema de respuesta automática ante choques consta
de una serie de sensores colocados en el vehículo que permiten
detectar un choque y alertar a un centro de asesoramiento. A
continuación si el usuario solicita ayuda o no contesta se le enviará
un equipo de emergencias a su localización calculada mediante
\ac{GPS}. Además, el vehículo puede transmitir información importante
sobre la colisión, como la dirección y el número de impactos, o el
estado de vuelco del vehículo.

Aunque OnStar es uno de los sistemas más destacados, existen otros
sistemas similares, que incluyen \ac{ACN}. Podemos nombrar algunos
como Mercedes-Benz Tele Aid, Ford's VEMS/RESCU system y BMW Assist.

Recientemente, la Comisión
Europea\footnote{\url{http://ec.europa.eu/index_es.htm}} ha propuesto
hacer obligatoria la incorporación del sistema \emph{eCall}, a partir
del 2015, en todos los vehículos de fabricación nueva. El comunicado de
prensa puede verse en~\cite{CEeCall}. El sistema eCall supone una alternativa
en Europa al sistema OnStar mostrado anteriormente. Básicamente su
funcionamiento es el mismo, salvo que eCall viene programado para
realizar un llamada al número de emergencia europeo 112 en caso de
accidente. Esta llamada también puede ser realizada manualmente con un
botón que incorpora y que podría ser pulsado por un testigo del
accidente~\cite{eCall}. En la imagen de la
figura~\ref{fig:funcionamiento_eCall} podemos ver un esquema de su
funcionamiento.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/funcionamiento_eCall.jpg}
    \caption{Esquema de funcionamiento de eCall (European Commision~\cite{CEimg_eCall})}
    \label{fig:funcionamiento_eCall}
  \end{center}
\end{figure}


\subsection{Detección automática de accidentes basada en smartphone}

Los sistemas \ac{ACN} como OnStar presentan el inconveniente de no
estar disponibles en todos los coches, además de ser caros de equipar
en vehículos antiguos. Sin embargo, los smartphone gracias a la amplia
variedad de sensores que incorporan (acelerómetro, \ac{GPS}, etc.), nos
brindan la oportunidad de crear sistemas capaces de detectar un
accidente e informar de forma rápida a los servicios
sanitarios. Además permiten mantener una conexión persistente a
Internet lo que les permite transmitir datos de forma continua para
informar de cualquier problema que se detecte.

La figura~\ref{fig:Smartphone_ACN} nos muestra el funcionamiento de
este tipo de sistemas. Como se observa en la figura, se distinguen
tres acciones básicas realizadas por el sistema:

\begin{enumerate}
\item Mediante los sensores del smartphone se detecta un posible
  accidente de trafico.

\item Mediante una conexión de datos 3G el teléfono transmite la
  información relativa al accidente a un servidor.

\item El servidor procesa la información del accidente y realiza
  las acciones que considere oportunas.
\end{enumerate}

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/Smartphone-Based_Accident_Detection_System.png}
    \caption{Estructura de un sistemas de detección de accidentes
      basado en Smartphone (WreckWatch~\cite{JulesWhite})}
    \label{fig:Smartphone_ACN}
  \end{center}
\end{figure}

Los sistemas de detección automática de accidentes basados en
smartphone presentan importantes ventajas con respecto a los basados
en vehículo, como son el acceso a la tecnología o el precio de ésta,
sin embargo presentan la desventaja de los \emph{falsos positivos}. El
problema de los falsos positivos rara vez aparece en sistemas basados
en vehículo, ya que estos cuentan con sensores capaces de detectar los
daños físicos en el vehículo, o la activación de mecanismos de
emergencia en el coche, como por ejemplo el airbag. Por otro lado
podemos contrarrestar el efecto de los falsos positivos haciendo uso
de información adicional, como señales acústicas antes y después del
accidente o las fuerzas de aceleración registradas.

A continuación se muestran las principales ventajas que podemos
encontrar en los sistemas basados en smartphone con respecto a los
sistemas de abordo instalados en los vehículos~\cite{JulesWhite}:

\begin{itemize}
\item Pueden medir de una forma más precisa las fuerzas experimentadas
  por las víctimas, ya que el dispositivo suele estar situado en su
  bolsillo.

\item El coste de un smartphone es relativamente bajo, y su uso esta
  muy extendido. Se evita tener que instalar hardware adicional en el
  vehículo.

\item Reducción de la complejidad del software de mantenimiento.

\item Posibilidad de ampliar capacidades de procesamiento y
  almacenamiento a través de servicios basados en cloud.
\end{itemize}

Por otro lado, los sistemas basados en smartphone presentan varias
desventajas a tener en cuenta:

\begin{itemize}
\item Consumen una cantidad significativa de batería.

\item La fuerza de impacto captada por el smartphone se ve reducida
  por los sistemas de seguridad del vehículo.

\item Una avería en el teléfono ocasionada por un impacto puede
  impedir la notificación del accidente.

\item El software de los sensores está controlado por el \ac{SO} del
  smartphone. Esto quiere decir que una actualización en dicho
  software podría dejar obsoleta la detección de accidentes.

\item Las pruebas de calidad se hacen difíciles debido a los falsos
  positivos.
\end{itemize}


\subsection{WreckWatch}
\label{sec:wreckWatch}

\emph{WreckWatch}~\cite{JulesWhite} es un sistema de detección automática de
accidentes de tráfico basado en smartphone. El sistema graba todos
los datos posibles referentes a los sucesos que podrían originar un
accidente. En concreto realiza un registro de la trayectoria,
velocidad y fuerzas de aceleración sobre un vehículo que está siendo
conducido y durante un accidente. El sistema también es capaz de
alertar a los servicios de emergencia de un accidente, añadiendo
imágenes y vídeos realizados por testigos que se encuentren en el
lugar del suceso.

\subsubsection{Arquitectura}

El sistema presenta una arquitectura \ac{C/S}, donde el cliente ha
sido desarrollado para su ejecución en entornos Android. La
información se transmite del cliente al servidor mediante operaciones
estándar \emph{HTTP POST} (ver figura \ref{fig:arqWreckWatch}).

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/WreckWatch_Arquitecture.png}
    \caption{Diagrama de arquitectura de WreckWatch~\cite{JulesWhite}}
    \label{fig:arqWreckWatch}
  \end{center}
\end{figure}

El \textbf{cliente} se compone de una serie de
\emph{Activities} (ver sección~\ref{sec:activities}) para la
cartografía, pruebas y carga de imágenes. También presenta
\emph{servicios} en segundo plano para la detección de accidentes
mediante la consulta constante de los sensores del sistema, como el
acelerómetro o el \ac{GPS}. La aplicación permite modificar la
frecuencia de rastreo de los sensores, con el fin de reducir los
consumos de batería, y la recopilación de datos del teléfono, para
obtener información de interés, como los contactos que se deben avisar
en caso de emergencia.

El \textbf{servidor} esta desarrollado utilizando Java/MySQL con Jetty
y Spring Framework. Proporciona un servicio de recogida de datos, y un
mecanismo para la comunicación con los servicios de emergencia y
familiares de la víctima. Permite a los sistemas clientes el envío de
características del accidente y presenta varias interfaces, como un
mapa de Google y servicios web XML/JSON, para acceder a esta
información.


\subsubsection{Modelo formal de detección de accidente}

En la creación de un modelo formal de detección de accidentes basado
en smartphone, con una precisión aceptable, es importante hacer frente
a dos problemas importantes:

\begin{itemize}
\item Detectar el accidente sin la medición directa de datos de
  impacto de sensores de a bordo instalados en el vehículo.

\item Examinar la posibilidad de falsos positivos.
\end{itemize}

Para hacer frente a estos problemas el sistema WreckWatch realiza un
muestreo continuo de varios sensores, atendiendo a unos umbrales de
filtrado que pretenden predecir cuando sucede un accidente. El modelo
empleado por el sistema para explorar el estado del vehículo es el
siguiente:

\begin{center}
  \begin{equation}
    \gamma = < \phi,T_\phi,\rho, T_\rho, \beta, \epsilon, S_\phi,
    S_\rho, S_\beta, M_\phi, M_\rho, M_\beta, M_\epsilon >
  \end{equation}
\end{center}

Donde:

\begin{itemize}
\item $\phi$: variable de aceleración que indica la
  aceleración máxima registrada por el teléfono en cualquier dirección.

\item ${S_\phi}$: lapso de tiempo desde que un evento de
  aceleración establece un valor para $\phi$ hasta que la variable es
  reiniciada.

\item $\rho$: variable binaria de evento de sonido que indica si un
  evento de sonido mayor que $M_\rho$dBs se ha producido. Mediante un
  estudio se ha establecido que 140dBs es un buen valor para $M_\rho$.

\item ${S_\rho}$: lapso de tiempo que se mantendrá a nivel 1 la
  variable $\rho$ después de registrar un evento de sonido superior a
  $M_\rho$dBs.

\item $\beta$: variable de velocidad umbral que toma valor uno si el
  vehículo ha estado viajando a más de $M\beta$mph.

\item $S_\beta$: lapso de tiempo que se mantendrá el valor 1 para la
  variable $\beta$ después de que el vehículo deje de viajar por
  encima de $M_\beta$mph.

\item $\epsilon$: distancia recorrida desde la última vez que la
  variable $\beta$ cambiará de 1 a 0.

\item $M_\phi$: aceleración mínima en Gs para que un solo evento de
  aceleración active la detección de accidentes.

\item $M_\rho$: decibelios mínimos requeridos para que un evento
  acústico active la variable $\rho$.

\item $M_\beta$: velocidad mínima, en millas por hora, que el
  dispositivo debe detectar para activar el sistema de detección de
  accidentes.
\item $M_\epsilon$: distancia máxima en metros que el dispositivo
  puede desplazarse a una velocidad más baja que $M_\beta$, antes de
  que el sistema de detección de accidentes sea desactivado.
\end{itemize}

La función de detección de accidente evalúa a 1 si un accidente es
detectado o 0 en caso contrario.

\begin{center}
  \begin{equation}
    Ev: \gamma \rightarrow \{0,1\}
  \end{equation}
\end{center}

La detección de accidente se establece de la siguiente forma:

\begin{center}
  \begin{equation}
    Ev(\gamma) = \left\{
      \begin{array}{l l}
        1 & \quad \text{si} \left(\frac{\phi}{M_\phi} + \alpha\rho
          \geq M_{Tr} \right) \land (\beta ==1) \\
        1 & \quad \text{si} (\epsilon < M_\epsilon) \land
        \left(\frac{\phi}{M_\phi} + \alpha\rho \geq M_{Tr} \right) \\
        0 & \quad \text{en otros casos}
      \end{array} \right.
  \end{equation}
\end{center}

Donde:

\begin{itemize}
\item $\alpha$: es un factor ajustable que se aplica al evento de
  sonido para indicar su importancia en la detección del accidente.

\item $M_{Tr}$: es el umbral para la detección del accidente.
\end{itemize}

Como se abstrae de la fórmula, el sistema reconocerá un accidente
mediante dos posibles escenarios: en el primero, el sistema
registra un evento de aceleración y sonido con una escala alta
mientras el vehículo se esta moviendo con una velocidad superior a
$M_\beta$, y en el segundo se produce una aceleración y un evento de
sonido habiendo recorrido una distancia inferior a $M_\epsilon$ desde
la última vez que se activó $M_\beta$, con este escenario se intenta
detectar si el vehículo es golpeado mientras esta detenido.


\subsubsection{Evaluación de falsos positivos}

WreckWatch basa su sistema de detección de accidentes en mediciones de
aceleración y sonidos, por lo que se enfrenta a falsos positivos
derivados de ambas mediciones. En esta sección mostraremos los casos
que se consideraron como causantes de falsos positivos en la creación
de este sistema y los análisis realizados para determinar su
importancia.

En cuanto a los casos que provocan falsos positivos relacionados con
los valores del acelerómetro, los casos considerados son: la caída
accidental del smartphone dentro del vehículo, o la realización de una
frenada de emergencia que no de lugar a accidente. Los resultados
obtenidos de los ensayos realizados por WreckWatch se muestran
en la figura~\ref{fig:experiment_Acceleration}. Como se observa, en
ninguno de los dos casos se sobrepasa el umbral establecido de 4G por
lo que se considera poco probable que estas situaciones den lugar a un
falso positivo.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{/Acceleration_During_Fall_and_Sudden_Stops.png}
    \caption{Aceleración registrada durante una caída y una frenada
      brusca~\cite{JulesWhite}}
    \label{fig:experiment_Acceleration}
  \end{center}
\end{figure}

Por otro lado, se consideraron seis casos que podrían causar
falsos positivos relacionados con la medición de sonido,
entre los que se tenían en cuenta la radio, el ruido de los ocupantes
del vehículo o el de la carretera. Este estudio demostró que el
smartphone no era capaz de detectar niveles de sonido superiores a
145 dB por lo que la medición de los niveles de sonido podría dar lugar
a la aparición de falsos positivos. Sin embargo, esta medida nos es
útil para mejorar la percepción de accidentes, ya que si se registra
una aceleración que se encuentre ligeramente por debajo del umbral,
pero que presentan niveles altos en la percepción de sonido, podríamos
tratar el caso también como un accidente.

% Local Variables:
%   coding: utf-8
%   mode: latex
%   mode: flyspell
%   ispell-local-dictionary: "castellano8"
% End:
