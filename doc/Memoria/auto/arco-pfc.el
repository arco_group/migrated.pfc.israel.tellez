(TeX-add-style-hook "arco-pfc"
 (lambda ()
    (TeX-add-symbols
     '("quoteauthor" 1)
     '("drop" 2)
     '("dedication" 1)
     '("license" 1)
     '("publishdate" 2)
     '("logo" 1)
     '("homepage" 1)
     '("email" 1)
     '("phone" 1)
     '("country" 1)
     '("city" 1)
     '("address" 1)
     '("advisor" 2)
     '("chapterformat" 1)
     '("sigla" 1)
     '("Acro" 2)
     '("acx" 1)
     '("myrowcolors" 1)
     '("tabfoot" 1)
     '("tabhead" 1)
     "tabheadformat"
     "tabfootformat"
     "tabcolorrow"
     "lstfont"
     "bibfont"
     "frontchapterformat"
     "mainchapterformat"
     "backchapterformat"
     "putlogo"
     "pretitle"
     "frontpage"
     "copyrightpage"
     "jury"
     "appendixtitle"
     "attributionArcoPFC"
     "UrlSpecialsOld"
     "UrlSpecials"
     "urlOld"
     "frontmatterorig"
     "mainmatterorig"
     "backmatterorig")
    (TeX-run-style-hooks
     "lettrine"
     "tipa"
     "pifont"
     "relsize"
     "fancyhdr"
     "titlesec"
     "rigidchapters"
     "clearempty"
     "tocbibind"
     "hyperref"
     "bookmarks"
     "acronym"
     "printonlyused"
     "listings"
     "colortbl"
     "booktabs"
     "xcolor"
     "table"
     "epsfig"
     "graphicx"
     "setspace"
     "geometry"
     "enumitem"
     "xifthen"
     "atbeginend"
     "hyphenat"
     "htt"
     "babel"
     "spanish"
     "inputenc"
     "utf8"
     "fontenc"
     "T1"
     "bk10"
     "book")))

