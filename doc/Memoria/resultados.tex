\chapter{Resultados}
\label{chap:resultados}

\drop{E}{}n este capítulo se muestran los resultados obtenidos de un análisis sobre
posibles errores debidos a falsos positivos, y se estudia la aplicación en funcionamiento
sobre un escenario ficticio, donde se observan los comportamientos ante esta situación de
prueba. También se realiza una estimación del coste que ha supuesto la realización del
proyecto, y se determina el tiempo empleado en llevarlo a cabo.

\section{Resultados}

En esta sección se realiza un análisis sobre el proceso de detección, y se analizan los
resultados obtenidos en un ensayo realizado para intentar determinar las causas que pueden
provocar falsos positivos. Posteriormente se muestra los pasos realizados en una prueba
real de la aplicación, y se indican los comportamientos obtenidos por ésta.


\subsubsection{Análisis previo}

Una de las principales dificultades que existía a la hora de realizar la aplicación, era
la de ser capaz de detectar un accidente lidiando con el problema de los falsos
positivos. Para la detección, se optó por hacer uso del acelerómetro integrado en el
teléfono, pero entonces apareció la dificultad de determinar un umbral adecuado que nos
permitiera saber en qué situación estamos ante un accidente de tráfico.

Ya que no se contaba con los medios apropiados para recrear impactos y poder realizar un
cálculo del umbral adecuado, se decidió estudiar tecnologías similares para comprobar en
que valor se solía establecer dicho umbral. Así, se comprobó que en otros sistemas en los
que es preciso detectar un accidente, como es el caso del airbag, este valor se
establecía en 3G. Sin embargo todos los sistemas que se encontraron estaban orientados a
vehículos automóviles, por lo que no se sabía como afectarían los falsos positivos en una
motocicleta, que cuenta con otras condiciones, como un alto grado de vibración.

Posteriormente se realizó un estudio sobre los casos que eran potencialmente propensos a
ser considerados como falsos positivos. Dado que la aplicación solo se activa cuando se
indica el inicio de una ruta, estos casos debían ser estudiados en el momento en que nos
encontramos sobre la motocicleta. La causa principal que se consideró era la de realizar
una parada de emergencia.

Para la realización de la prueba se usó un ciclomotor tipo scooter y un smartphone HTC
Desire. La captura de los datos se ha realizado con la app de Android \emph{Accelerometer
  Monitor}\footnote{\url{https://play.google.com/store/apps/details?id=com.lul.accelerometer}}. Los
resultados obtenidos al realizar una parada de emergencia a $30 km/h$ y $50 Km/h$ se
muestran en los gráficos de las figuras \ref{fig:graficoFrenada_30kmh} y
\ref{fig:graficoFrenada_50kmh} respectivamente.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/grafico30kmh.eps}
    \caption{Gráfico de los valores máximos de aceleración en valor absoluto para una
      frenada de emergencia a 30km/h. El eje vertical expresa la fuerza de aceleración
      experimentada en $m/s^2$ y el eje horizontal expresa el momento de tiempo en décimas
      de segundo
    ($ds$)}
    \label{fig:graficoFrenada_30kmh}
  \end{center}
\end{figure}

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/grafico50kmh.eps}
    \caption{Gráfico de los valores máximos de aceleración en valor absoluto para una
      frenada de emergencia a 50km/h. El eje vertical expresa la fuerza de aceleración
      experimentada en $m/s^2$ y el eje horizontal expresa el momento de tiempo en décimas
      de segundo
    ($ds$)}
    \label{fig:graficoFrenada_50kmh}
  \end{center}
\end{figure}

Estos gráficos representan los valores absolutos máximos de aceleración en $m/s^2$
captados en las frenadas de emergencia de los dos casos representados. Para la creación de
estos gráficos se ha usado la herramienta
Octave\footnote{\url{http://www.gnu.org/software/octave/}}.

Como se puede observar en los gráficos, en ninguno de los casos se alcanza ni de lejos el
valor umbral de 3G, por lo que podemos decir que las frenadas de emergencia no serán causa
de falsos positivos.

\subsubsection{Detección y envío del accidente}

Dada la imposibilidad de probar la aplicación en su entorno real, por motivos obvios, se
ha optado por representar un escenario alternativo que nos pueda ayudar a probar la
aplicación, sin la necesidad de que se produzca un accidente real. Para la realización de
esta prueba ha sido necesario adaptar el valor umbral de detección de accidente fijándolo
en poco mas de 1G ($15 m/s^2$), para de este modo evitar la detección inmediata, a la vez
que se tiene un valor lo suficientemente pequeño como para que pueda ser creado mediante
un movimiento de mano.

La prueba ha consistido en realizar un movimiento brusco con la mano, mientras se sostiene
un smartphone HTC Desire con la aplicación de usuario activa, y una vez que se ha indicado
el inicio de una ruta. Al realizar esta prueba, el teléfono ha experimentado una
aceleración de $17.65 m/s^2$ que es superior al valor fijado como limite, por lo que ha
considerado que estamos ante un accidente y ha procedido a mostrar la pantalla de
confirmación (ver figura~\ref{fig:pantalla_confirmar}).

Transcurrido el tiempo necesario para la confirmación, la aplicación ha
notificado del accidente a la aplicación de gestión, la cual ha registrado el accidente y
lo ha almacenado en la base de datos. En el \emph{log} de la
figura~\ref{fig:log_notificacionIncidente} podemos ver como el gestor ha recibido un
incidente del usuario <<prueba@example.com>>.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/log_envio_issue.png}
    \caption{Pantalla de log del gestor tras la recepción de un incidente}
    \label{fig:log_notificacionIncidente}
  \end{center}
\end{figure}

\subsubsection{Búsqueda y asignación del accidente}

Una vez realizado el paso anterior la aplicación del usuario ha activado un servicio que
nos permitirá comunicarnos con el smartphone de forma remota para solicitarle diversas
acciones.

Mediante el uso de otro teléfono, esta vez un LG L5, con la aplicación de rescate
instalada, hemos iniciado sesión con un usuario de rescate valido y se nos ha mostrado la
ventana con el listado de accidentes (ver figura~\ref{fig:pantalla_listarUno}), en
este caso solo uno. Tras seleccionar este accidente se nos muestra una ventana con tres
pestañas. En la pestaña activa encontramos toda la información relacionada con el
accidente que se acaba de detectar, así como los datos de la víctima (ver
figura~\ref{fig:pantalla_datosIncidente_NoAsignado}). Si intentamos pulsar
en la pestaña <<localización>> o en <<controles>> nos encontramos con el mensaje que nos
indica que previamente debemos tener asignado dicho accidente (ver
figura~\ref{fig:error_localizarNoAsignado}).

\begin{figure}[!h]
  \begin{center}
    \fcolorbox{black}{gray}{\includegraphics[width=0.3\textwidth]{/pantallazo_LG_listarUno.png}}
    \caption{Pantalla de listado de incidentes}
    \label{fig:pantalla_listarUno}
  \end{center}
\end{figure}

\begin{figure}[!h]
  \begin{center}
    \fcolorbox{black}{gray}{\includegraphics[width=0.3\textwidth]{/pantallazo_datosIncidente_NoAsignado.png}}
    \caption{Pantalla de datos de incidente}
    \label{fig:pantalla_datosIncidente_NoAsignado}
  \end{center}
\end{figure}

La asignación del accidente seleccionado la realizamos en la pestaña <<Datos incidente>>,
en la cual aparece en la parte inferior un botón que nos permite asignarnos el
accidente (ver figura~\ref{fig:datosIncidente_asignado}).

\subsubsection{Localización de la víctima}

Para finalizar la prueba, una vez realizada la asignación indicada en el paso anterior,
procedemos a pulsar en la pestaña <<localización>>. En esta ocasión se nos muestra un mapa
con un punto dibujado sobre él, que representa la localización de la víctima
(ver figura~\ref{fig:prueba_localizacion}). Esto nos permitiría ir a la zona indicada para tratar
de localizarla. No obstante, una vez allí podríamos tener dificultades para encontrarla,
por lo que realizaremos la activación del altavoz y la linterna.

\begin{figure}[!h]
  \begin{center}
    \fcolorbox{black}{gray}{\includegraphics[width=0.3\textwidth]{/pantallazo_LG_Localizacion.png}}
    \caption{Captura de pantalla de LoRAM\_Rescue mostrando ubicación de la víctima}
    \label{fig:prueba_localizacion}
  \end{center}
\end{figure}

La activación de los componentes que nos facilitaran la localización in situ, la hemos
realizado desde la pestaña <<controles>>. Una vez que indicamos la activación de estos
componentes, la pantalla de la aplicación de rescate ejecutada por el LG nos muestra que
estos componentes se encuentran activos (ver figura~\ref{fig:pantalla_controlesActivos}),
mientras que en la aplicación de usuario ejecutada por el HTC comienza a sonar una alarma
y se enciende la luz de la cámara. En la figura~\ref{fig:log_activacionControles} podemos
ver el \emph{log} mostrado por el gestor durante este proceso.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{/log_controlesActivos.png}
    \caption{Pantalla de log del gestor de la activación de los controles}
    \label{fig:log_activacionControles}
  \end{center}
\end{figure}

\section{Costes y recursos}

En esta sección se mostrarán los costes estimados, tanto materiales como de recursos
humanos, en la realización de este proyecto. Además se realizará una estimación del tiempo
empleado en llevarlo a cabo.

Aunque la idea del proyecto surgió mucho antes, la realización del mismo comenzó en
septiembre de 2012, y se han contabilizado 40 semanas empleadas para su desarrollo, con
una dedicación media de 8 horas al día durante 5 días a la semana. Entre el tiempo
dedicado al proyecto, debemos incluir también, el tiempo invertido al inicio del
desarrollo en estudiar las diversas tecnologías y herramientas que han intervenido, como
Android o \ac{Ice}.

A continuación se realiza un cálculo aproximado del coste que ha supuesto los recursos
hardware utilizados en la realización de este proyecto. En el cuadro~\ref{tab:costes-HW}
puede verse el desglose de estos componentes con el coste estimado. En esta tabla
solamente se ha tenido en cuenta el precio de cada componente, sin indicar otros gastos,
como consumo eléctrico o desgaste de material.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/costes-HW.tex}
  }
  \caption[Coste económico del hardware empleado]
  {Coste económico del hardware empleado}
  \label{tab:costes-HW}
\end{table}

Por otra parte los recursos y herramientas software utilizadas han sido software libre,
por lo que no han supuesto ningún incremento en los costes derivado del uso de licencias
privativas.

El número de líneas de código totales empleadas por lenguaje de programación puede verse
en el cuadro~\ref{tab:sloc-lenguajes}, este cuadro además incluye las líneas de código
destinadas a las pruebas. La división por componentes del sistema, de estas líneas de
código, puede verse en el cuadro~\ref{tab:sloc-componentes}.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/sloc_lenguajes.tex}
  }
  \caption[Líneas de código por lenguaje de programación]
  {Líneas de código por lenguaje de programación}
  \label{tab:sloc-lenguajes}
\end{table}

\begin{table}[hp]
  \centering
  {\small
  \input{tables/sloc_componentes.tex}
  }
  \caption[Líneas de código separadas por componentes del sistema]
  {Líneas de código separadas por componentes del sistema}
  \label{tab:sloc-componentes}
\end{table}

La realización de la estimación del coste que ha supuesto el proyecto, en cuanto a
recursos humanos, puede verse en el cuadro~\ref{tab:costes-desarrollo}. Para crear este
cuadro se ha usado la herramienta \emph{SLOCCount} que nos ha permitido calcular el tamaño
del proyecto en \acx{SLOC} y realizar una estimación del esfuerzo según el modelo
\emph{COCOMO}. En esta estimación no se ha tenido en cuenta el esfuerzo realizado en la
creación de esta memoria.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/costes-desarrollo.tex}
  }
  \caption[Coste económico estimado en recursos humanos]
  {Coste económico estimado en recursos humanos}
  \label{tab:costes-desarrollo}
\end{table}

\subsection{Repositorio}

El proyecto completo, incluido el código y la documentación, se encuentra almacenado en
\emph{bitbucket.org}\footnote{\url{https://bitbucket.org/}}. La descarga del proyecto
puede realizarse en un terminal con la siguiente orden:

\begin{listing}[
  language = sh]
$ hg clone https://bitbucket.org/arco_group/pfc.loram
\end{listing} %$

El repositorio se encuentra organizado de la siguiente forma:

\begin{itemize}
\item db: Contiene el código \emph{sql} de creación de la base de datos empleada por el
  sistema.
\item doc: Contiene los ficheros \emph{.tex} y \emph{.bib} que forman esta memoria y el
  anteproyecto de este trabajo.
\item src: Contiene todo el código del sistema dividido por componentes.
\item test: Contiene todos los test creados para la realización de este proyecto,
  divididos en unitarios y de integración.
\end{itemize}

% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "castellano8"
%   mode: latex
%   TeX-master: "main"
% End:
