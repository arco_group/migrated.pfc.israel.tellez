% -*- coding: utf-8 -*-

\chapter{Introducción}
\label{chap:intro}

\drop{E}{}n los accidentes de motocicleta es habitual encontrar
problemas para localizar a los motoristas accidentados, ya que estos
suelen salir despedidos del vehículo, dificultando las labores de
búsqueda y asistencia de las víctimas. En la mayoría de estos
accidentes una rápida intervención puede ser vital y puede incluso
permitir salvar una vida.

Los denominados teléfonos inteligentes o \emph{smartphone} constituyen
una plataforma con múltiples sensores de diversa índole que nos
permiten realizar tareas como localización o incluso detectar
accidentes. La creciente evolución de estas tecnologías, y el
abaratamiento de sus costes, ha hecho posible la aparición de nuevas
aplicaciones cada vez más sofisticadas, orientadas tanto a la
localización de personas como a la detección de riesgos.

La realización de este proyecto pretende hacer uso de estas
tecnologías para proporcionar una solución económica a los problemas
expuestos. En este capítulo se introducirán los principales conceptos
que han intervenido en su realización.

\section{Sistemas de seguridad de vehículos}

Desde la aparición de los primeros vehículos a motor su capacidades
han ido en aumento, siendo éstos cada vez más potentes y veloces. Este
hecho ha supuesto la necesidad de crear mecanismos que garanticen la
seguridad y prevengan de posibles accidentes.

Las empresas dedicadas a la fabricación de automóviles han ido
incorporando en los vehículos cada vez más y mejores tecnologías con
el fin de hacer la conducción más segura y reducir el número
de accidentes producidos, o en el caso de que estos ocurran, minimizar
en la medida de lo posible el número de víctimas mortales.

Los sistemas de seguridad de vehículos, se dividen según dos puntos de
vista diferentes que abarcan la prevención y la limitación de daños
en caso de producirse un accidente, tanto para el vehículo, como para
sus ocupantes~\cite{parera2000sistemas}. Estos puntos de vista son los
siguientes:

\begin{itemize}
\item Seguridad activa: es la seguridad que nos proporcionan todos los
  elementos incorporados en el vehículo, incrementando su seguridad
  durante los desplazamientos, con el fin de disminuir el riesgo de
  que se produzca un accidente. Los sistemas de seguridad activa
  pueden estar orientados a mejorar la seguridad en diferentes partes
  del vehículo, como en el motor, la transmisión o los frenos. Entre
  los mecanismos de seguridad activa encontramos los frenos de disco,
  sistemas de control de estabilidad (ESP), sistema de control del par
  motor (ASR) o el sistema de control de frenada (ABS).

\item Seguridad pasiva: es la seguridad que nos proporcionan todos los
  elementos, que incorporados o no al vehículo, reducen los daños de
  los ocupantes, en caso de que se produzca el accidente. Entre los
  mecanismos de seguridad pasiva destacamos el cinturón de seguridad,
  el airbag o el casco.
\end{itemize}

En los últimos años están apareciendo nuevos sistemas de seguridad
basados en detección del entorno. Dichos sistemas están aún poco
extendidos y en la mayoría de los casos se presentan como
equipamientos opcionales o son solo prototipos. Entre estos nuevos
sistemas podemos encontrar sistemas anticolisión como el \emph{City
  Safety} de Volvo~\cite{citySafety}, sistemas de alerta de cambio
involuntario de carril como \emph{LDWS} de Citroen~\cite{ldws} o
sistemas de reconocimiento de objetos como
ContiGuard~\cite{contiGuard} o EyeSight~\cite{eyeSight}, por destacar
algunos.

\section{Mecanismos de detección y asistencia en accidentes de
  tráfico}

Recientemente han surgido un nuevo tipo de sistemas de seguridad para
vehículos que podríamos encuadrar en el marco de la seguridad
pasiva. Este tipo de sistemas de seguridad detectan mediante sensores
cuando se produce un accidente, y notifican este suceso a un centro de
emergencias.

El interés por este tipo de sistemas ha ido en aumento, llegando hasta
el punto de ser obligatoria su incorporación en los vehículos de
fabricación nueva, en Europa, a partir del 2015.

El funcionamiento de estos sistemas se caracteriza no solo por
detectar el accidente, si no también por facilitar la localización de
las víctimas. Por ello, suelen incorporar un sistema de
posicionamiento, como el \ac{GPS}, que permita determinar la posición
y enviarla al centro de emergencias. Además, pueden añadir información
adicional que se considere importante, como la dirección que llevaba
el vehículo en la carretera, el estado de vuelco, etc. Entre los
sistemas de este tipo podemos destacar OnStar, sobre todo extendido en
America, ó eCall, que ha sido el escogido para su uso en Europa.

Todos estos sistemas hacen uso de hardware adicional instalado en el
vehículo con el fin de detectar el accidente y alertar a
emergencias. Sin embargo, los smartphone que encontramos hoy en día,
cuentan con las características apropiadas para realizar estas
funciones. Como ejemplo de un mecanismo de detección y asistencia
basado en smartphone podemos destacar \emph{WreckWatch} que será
estudiado en detalle en la sección~\ref{sec:wreckWatch} de este
documento.

\section{Problemática}

A pesar de los esfuerzos realizados por los gobiernos, compañías de
fabricación de vehículos o instituciones como la
\ac{DGT}\footnote{\url{http://www.dgt.es/}}, los accidentes siguen
apareciendo con frecuencia en las carreteras, constituyendo la octava
causa de muerte a nivel mundial según la
\ac{OMS}\footnote{\url{http://www.who.int/es/}}, y se prevé que será
la quinta para 2030 de no tomar medidas urgentes~\cite{who}.

El problema radica en la localización a tiempo de las víctimas, ya que
en muchas ocasiones las víctimas no están en condiciones de avisar del
suceso, y pasa cierto tiempo hasta que alguien los localiza y es capaz
de avisar a emergencias.

En los vehículos de dos ruedas, este problema de localización se ve
incrementado, ya que los ocupantes del vehículo suelen salir
despedidos y pueden quedar atrapados en algún lugar poco visible, como
unos matorrales abruptos.

A pesar de que existen diferentes sistemas orientados a detectar
accidentes de vehículos y a aportar la ubicación de los ocupantes, los
sistemas orientados a vehículos de dos ruedas son muy escasos, y no
incorporan mecanismos que permitan realizar la localización de los
ocupantes in situ.

Todos estos problemas, unidos al alto coste y a las dificultades de
instalación, que presentan en algunos casos los sistemas de detección
de accidentes disponibles en el mercado, nos llevó a la creación de
este trabajo. El objetivo general que se persigue con \ac{LoRAM} es
la creación de un sistema capaz de detectar accidentes de motocicleta
y facilitar la localización de las víctimas sobre el lugar del
accidente, todo ello haciendo uso exclusivo de las tecnologías
incorporadas en un smartphone convencional.


\section{Estructura del documento}

La realización de este documento se ha llevado a cabo siguiendo la
normativa vigente de la Escuela Superior de Informática de Ciudad Real
para la realización de un \ac{PFC}.

La distribución del contenido de este documento por capítulos es la
siguiente:

\begin{definitionlist}
\item[Capítulo 1: Introducción]

  En este capítulo se ha mostrado una introducción al ámbito en el que
  se engloba este trabajo, comentando brevemente la problemática
  existente.

\item[Capítulo 2: Antecedentes]

  El capítulo de antecedentes realiza un estudio sobre los conceptos y
  tecnologías implicadas en el desarrollo del proyecto.

\item[Capítulo 3: Objetivos]

  En este capítulo se presenta el objetivo general y los distintos
  objetivos específicos que se persiguen con la realización del
  \ac{PFC}.

\item[Capítulo 4: Método de trabajo]

  A lo largo de este capítulo se expone la metodología de desarrollo
  empleada, y se muestran las herramientas utilizadas en la
  realización de este trabajo.

\item[Capítulo 5: Arquitectura y Desarrollo]

  En este capítulo se muestra la división del sistema, detallando cada
  una de las partes que lo forman. También se define el listado de
  requisitos, y se expone el proceso de desarrollo por iteraciones
  seguido durante la realización del proyecto.

\item[Capítulo 6: Resultados]

  A lo largo de este capítulo se analizan los resultados obtenidos con
  la ejecución del sistema, y los costes derivados de su realización.

\item[Capítulo 7: Conclusiones y Trabajo futuro]

  El capítulo de conclusiones y propuestas muestra en qué grado han
  sido alcanzados los objetivos marcados. También se exponen algunas
  propuestas de trabajo futuro y se finaliza con una valoración
  personal sobre lo que ha supuesto la realización de este trabajo.

\end{definitionlist}

% Local Variables:
%   coding: utf-8
%   mode: latex
%   mode: flyspell
%   ispell-local-dictionary: "castellano8"
% End:
