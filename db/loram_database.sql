-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.30-1.1


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema loram
--

CREATE DATABASE IF NOT EXISTS loram;
USE loram;

--
-- Definition of table `loram`.`Incidents`
--

DROP TABLE IF EXISTS `loram`.`Incidents`;
CREATE TABLE  `loram`.`Incidents` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `victim` varchar(80) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `rescue_team` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loram`.`Incidents`
--

/*!40000 ALTER TABLE `Incidents` DISABLE KEYS */;
LOCK TABLES `Incidents` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `Incidents` ENABLE KEYS */;


--
-- Definition of table `loram`.`Rescue_staff`
--

DROP TABLE IF EXISTS `loram`.`Rescue_staff`;
CREATE TABLE  `loram`.`Rescue_staff` (
  `nif` varchar(9) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(25) NOT NULL,
  `surname1` varchar(25) NOT NULL,
  `surname2` varchar(25) NOT NULL,
  `phone` int(11) NOT NULL,
  `team` int(10) NOT NULL,
  PRIMARY KEY (`nif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loram`.`Rescue_staff`
--

/*!40000 ALTER TABLE `Rescue_staff` DISABLE KEYS */;
LOCK TABLES `Rescue_staff` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `Rescue_staff` ENABLE KEYS */;


--
-- Definition of table `loram`.`Rescue_teams`
--

DROP TABLE IF EXISTS `loram`.`Rescue_teams`;
CREATE TABLE  `loram`.`Rescue_teams` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `institution` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loram`.`Rescue_teams`
--

/*!40000 ALTER TABLE `Rescue_teams` DISABLE KEYS */;
LOCK TABLES `Rescue_teams` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `Rescue_teams` ENABLE KEYS */;


--
-- Definition of table `loram`.`Users`
--

DROP TABLE IF EXISTS `loram`.`Users`;
CREATE TABLE  `loram`.`Users` (
  `email` varchar(80) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(25) NOT NULL,
  `surname1` varchar(25) NOT NULL,
  `surname2` varchar(25) NOT NULL,
  `phone` int(11) NOT NULL,
  `numAA` int(11) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loram`.`Users`
--

/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
LOCK TABLES `Users` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
