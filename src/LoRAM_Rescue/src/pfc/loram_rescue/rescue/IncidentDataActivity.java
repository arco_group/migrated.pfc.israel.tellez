package pfc.loram_rescue.rescue;

import pfc.loram_rescue.LoRAM_Rescue;
import pfc.loram_rescue.R;
import LoRAM.IncidentManagerPrx;
import LoRAM.StateType;
import LoRAM.UserData;
import LoRAM.UserManagerPrx;
import LoRAM.UserNotLogged;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class IncidentDataActivity extends Fragment {

	// UI references
	private TextView txvName;
	private TextView txvSurnames;
	private TextView txvPhone;
	private TextView txvNumAA;
	private TextView txvDate;
	private TextView txvType;
	private TextView txvState;
	private TextView txvRescueTeam;
	private TextView txvTitlRescueTeam;
	private Button btnAllocate;
	
	// IncidentData
	private int _id;
	private String _victim;
	private String _date;
	private String _state;
	private String _type;
	private int _team;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		Bundle bundle = getActivity().getIntent().getExtras();

		_id = bundle.getInt("id");
		_victim = bundle.getString("victim");
		_date = bundle.getString("date");
		_state = bundle.getString("state");
		_type = bundle.getString("type");
		_team = bundle.getInt("team");
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		
		if (container != null) {
			view = inflater.inflate(R.layout.activity_incident_data, container, false);
			
			//Activity controllers
			txvName = (TextView)view.findViewById(R.id.txvName);
			txvSurnames = (TextView)view.findViewById(R.id.txvSurnames);
			txvPhone = (TextView)view.findViewById(R.id.txvPhone);
			txvNumAA = (TextView)view.findViewById(R.id.txvNumAA);
			txvDate = (TextView)view.findViewById(R.id.txvDate);
			txvType = (TextView)view.findViewById(R.id.txvType);
			txvState = (TextView)view.findViewById(R.id.txvState);
			txvRescueTeam = (TextView)view.findViewById(R.id.txvRescueTeam);
			txvTitlRescueTeam = (TextView)view.findViewById(R.id.txvTitlRescueTeam);
			btnAllocate = (Button) view.findViewById(R.id.btnAllocate);

			
			//initializeData();
			InitializeDataTask initializeTask = new InitializeDataTask();
			initializeTask.execute((Void)null);
			
			//Controllers setting
			btnAllocate.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AllocateIncidentTask allocateTask = new AllocateIncidentTask();
					allocateTask.execute((Void)null);
					
				}
			});
			if (_state.equals(StateType.Allocated.name())) {
				btnAllocate.setEnabled(false);
				
				txvTitlRescueTeam.setVisibility(View.VISIBLE);
				txvRescueTeam.setVisibility(View.VISIBLE);
			}
			
		}
		
		return view;
	}
	
	class InitializeDataTask extends AsyncTask<Void, Void, UserData> {

		private LoRAM_Rescue _app;
		
		@Override
		protected UserData doInBackground(Void... params) {
			_app = (LoRAM_Rescue)getActivity().getApplication();
			
			UserManagerPrx manager = _app.getUserManagerProxy();
					
			
			try {
				UserData victimData = manager.getLoggedUserData(_victim);
				
				return victimData;
			} catch (UserNotLogged e) {
				e.printStackTrace();
				
				return null;
			} catch (Exception e1) {
				e1.printStackTrace();
				
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(UserData victimData) {
			
			if (victimData != null) {
				txvName.setText(victimData.name);
				String surnames = victimData.surname1 + " " + victimData.surname2;
				txvSurnames.setText(surnames);
				txvPhone.setText(String.valueOf(victimData.phone));
				txvNumAA.setText(String.valueOf(victimData.numAA));
			} else {
				Toast userNotLoggedNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"La víctima no esta logueada", Toast.LENGTH_SHORT);
				userNotLoggedNotification.show();
			}
			
			txvDate.setText(_date);
			txvState.setText(_state);
			txvType.setText(_type);
			txvRescueTeam.setText(String.valueOf(_team));
			
			if (_team == _app.get_user().team) {
				txvRescueTeam.setTextColor(Color.RED);
			}
			
		}		
	}
	
	class AllocateIncidentTask extends AsyncTask<Void, Void, Boolean> {

		private int rescueTeam;
		private LoRAM_Rescue _app;
		
		@Override
		protected Boolean doInBackground(Void... params) {
			_app = (LoRAM_Rescue)getActivity().getApplication();
			
			IncidentManagerPrx manager = _app.getIncidentManagerProxy();
			
			rescueTeam = _app.get_user().team;
			
			try {
				manager.allocateIncident(rescueTeam, _id);
				return true;
			}catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				//Update state
				_state = StateType.Allocated.name();
				txvState.setText(_state);
				_team = rescueTeam;
				_app.set_currentIncident(_id);
				txvRescueTeam.setText(String.valueOf(_team));
				txvRescueTeam.setTextColor(Color.RED);
				
				btnAllocate.setEnabled(false);
				
				txvTitlRescueTeam.setVisibility(View.VISIBLE);
				txvRescueTeam.setVisibility(View.VISIBLE);
			}
		}
	}
}
