package pfc.loram_rescue.rescue;

import pfc.loram_rescue.LoRAM_Rescue;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import LoRAM.AccessDenied;
import LoRAM.ActuatorOff;
import LoRAM.ImposibleConnectWithUser;
import LoRAM.IncidentManagerPrx;
import LoRAM.Location;
import LoRAM.LocationRequest;
import LoRAM.LocationType;
import LoRAM.NotAvailableActuator;
import LoRAM.NotSuitable;
import LoRAM.RemoteControlPrx;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class LocationActivity extends SupportMapFragment {
	
	private GoogleMap _map;
	private RemoteControlPrx _rc;
	
	// IncidentData
	private int _id;
	private String _victim;
	private int _team;
	
	private int _ownTeam;
	private LatLng _pos;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		_map = null;
		_rc = null;
		_pos = null;
		
		Bundle bundle = getActivity().getIntent().getExtras();
		_id = bundle.getInt("id");
		_victim = bundle.getString("victim");
		_team = bundle.getInt("team");

		LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
		_ownTeam = app.get_user().team;
	
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		
		if (container != null) {
			view = super.onCreateView(inflater, container, savedInstanceState);
		}
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
		
		if (_team == _ownTeam || _id == app.get_currentIncident()) {
			setUpMapIfNeeded();
		} else {
			Toast noAllocateNotification = Toast.makeText(getActivity().getApplicationContext(), 
					"No tiene asignado este incidente", Toast.LENGTH_SHORT);
			noAllocateNotification.show();
		}
		
	}
	
	private void setUpMapIfNeeded() {
		if (_map == null || _rc == null) {
			
			if (_map == null) {
				_map = getMap();
			}
			
			if (_rc == null) {
				LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
				
				if (app.get_currentRC() == null) {
					GetRCTask rcTask = new GetRCTask();
					rcTask.execute((Void)null);
				} else {
					_rc = app.get_currentRC();
				}
			}
			
			setUpMap();
			
		} else {
			setUpMap();
		}
	}
	
	private void setUpMap() {
		if (_rc != null) {
			GetPositionTask posTask = new GetPositionTask();
			posTask.execute((Void)null);
			
		}
	}
	
	class GetPositionTask extends AsyncTask<Void, Void, Location> {

		@Override
		protected Location doInBackground(Void... params) {
			LocationRequest request = new LocationRequest(LocationType.GPSType, 15000, 0);
			
			try {
				Location loc= _rc.getLocation(request);
				
				return loc;
			} catch (ActuatorOff e) {
				e.printStackTrace();
				
				return null;
			} catch (NotAvailableActuator e) {
				e.printStackTrace();
				
				return null;
			} catch (NotSuitable e) {
				e.printStackTrace();
				
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(Location loc) {
			if (loc != null) {
				_pos = new LatLng(loc.latitude, loc.longitude);
				
				_map.moveCamera(CameraUpdateFactory.newLatLngZoom(_pos, 15));
				_map.addMarker(new MarkerOptions().position(_pos).title("Víctima"));
				
			} else {
				Toast actuatorOffNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Imposible obtener la ubicación", Toast.LENGTH_SHORT);
				actuatorOffNotification.show();
			}
		}
	}
	
	class GetRCTask extends AsyncTask<Void, Void, RemoteControlPrx> {

		private LoRAM_Rescue _app;
		
		@Override
		protected RemoteControlPrx doInBackground(Void... params) {
			_app = (LoRAM_Rescue)getActivity().getApplication();
			
			IncidentManagerPrx manager = _app.getIncidentManagerProxy();
			
			try {
				RemoteControlPrx rc = manager.getRemoteControl(_victim);
				
				return rc;
			} catch (AccessDenied e) {
				e.printStackTrace();
				
				return null;
			} catch (ImposibleConnectWithUser e) {
				e.printStackTrace();
				
				return null;
			}
			
		}
		
		@Override
		protected void onPostExecute(RemoteControlPrx rc) {
			if (rc != null) {
				_rc = rc;
				_app.set_currentRC(rc);
				setUpMap();
			} else {
				Toast errorNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Imposible obtener la ubicación", Toast.LENGTH_SHORT);
				errorNotification.show();
			}
			
			
		}
	}
}
