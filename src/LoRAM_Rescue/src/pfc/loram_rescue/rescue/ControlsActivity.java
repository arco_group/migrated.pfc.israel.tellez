package pfc.loram_rescue.rescue;


import pfc.loram_rescue.LoRAM_Rescue;
import pfc.loram_rescue.R;
import LoRAM.AccessDenied;
import LoRAM.ActivationRequest;
import LoRAM.ActuatorType;
import LoRAM.DeactivationRequest;
import LoRAM.ImposibleConnectWithUser;
import LoRAM.IncidentManagerPrx;
import LoRAM.LampActivationRequest;
import LoRAM.Level;
import LoRAM.NotAvailableActuator;
import LoRAM.NotSuitable;
import LoRAM.RemoteControlPrx;
import LoRAM.SpeakerActivationRequest;
import LoRAM.VibratorActivationRequest;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ControlsActivity extends Fragment {
	
	private RemoteControlPrx _rc;
	
	// UI references
	private ToggleButton tgbVibrator;
	private ToggleButton tgbLamp;
	private ToggleButton tgbSpeaker;
	private SeekBar sBarVibratorFreq;
	private SeekBar sBarLampBrightness;
	private SeekBar sBarSpeakerVolume;
	private SeekBar sBarSpeakerRate;
	private Button btnSolve;
	
	// IncidentData
	private int _id;
	private String _victim;
	private int _team;
	private int _ownTeam;
	
	// Controls data
	private Level _vibratorFrequency;
	private Level _lampBrightness;
	private float _speakerVolume;
	private float _speakerRate;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		_rc = null;
		_vibratorFrequency = Level.Medium;
		_lampBrightness = Level.High;
		_speakerVolume = 1;
		_speakerRate = 1;
		
		Bundle bundle = getActivity().getIntent().getExtras();
		_id = bundle.getInt("id");
		_victim = bundle.getString("victim");
		_team = bundle.getInt("team");
		
		LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
		_ownTeam = app.get_user().team;
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		
		if (container != null) {
			view = inflater.inflate(R.layout.activity_controls, container, false);
			
			//Activity controllers
			tgbVibrator = (ToggleButton)view.findViewById(R.id.tgbVibrator);
			tgbLamp = (ToggleButton)view.findViewById(R.id.tgbLamp);
			tgbSpeaker = (ToggleButton)view.findViewById(R.id.tgbSpeaker);
			btnSolve = (Button)view.findViewById(R.id.btnSolve);
			sBarVibratorFreq = (SeekBar)view.findViewById(R.id.sBarVibratorFreq);
			sBarLampBrightness = (SeekBar)view.findViewById(R.id.sBarLampBrightness);
			sBarSpeakerVolume = (SeekBar)view.findViewById(R.id.sBarSpeakerVolume);
			sBarSpeakerRate = (SeekBar)view.findViewById(R.id.sBarSpeakerRate);
			
			//Controllers setting
			tgbVibrator.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (tgbVibrator.isChecked()) {
						sBarVibratorFreq.setEnabled(false);
						
						VibratorActivationRequest request = new VibratorActivationRequest(null, null,
								ActuatorType.TypeVibrator, _vibratorFrequency);
						
						ActivateTask activateTask = new ActivateTask();
						activateTask.execute(request);
						
					} else {
						sBarVibratorFreq.setEnabled(true);
						
						DeactivationRequest request = new DeactivationRequest(null, ActuatorType.TypeVibrator);
						
						DeactivateTask deactivateTask = new DeactivateTask();
						deactivateTask.execute(request);
						
					}
				}
			});
			tgbLamp.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (tgbLamp.isChecked()) {
						sBarLampBrightness.setEnabled(false);
						
						LampActivationRequest request = new LampActivationRequest(null, null,
								ActuatorType.TypeLamp, _lampBrightness);
						
						ActivateTask activateTask = new ActivateTask();
						activateTask.execute(request);
						
					} else {
						sBarLampBrightness.setEnabled(true);
						
						DeactivationRequest request = new DeactivationRequest(null, ActuatorType.TypeLamp);

						DeactivateTask deactivateTask = new DeactivateTask();
						deactivateTask.execute(request);
						
					}
				}
			});
			tgbSpeaker.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (tgbSpeaker.isChecked()) {
						sBarSpeakerVolume.setEnabled(false);
						sBarSpeakerRate.setEnabled(false);
						
						SpeakerActivationRequest request = new SpeakerActivationRequest(null, null,
								ActuatorType.TypeSpeaker, _speakerVolume, _speakerRate);
						
						ActivateTask activateTask = new ActivateTask();
						activateTask.execute(request);
						
					} else {
						sBarSpeakerVolume.setEnabled(true);
						sBarSpeakerRate.setEnabled(true);
						
						DeactivationRequest request = new DeactivationRequest(null, ActuatorType.TypeSpeaker);
						
						DeactivateTask deactivateTask = new DeactivateTask();
						deactivateTask.execute(request);
						
					}
				}
			});
			sBarVibratorFreq.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Nothing to do
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Nothing to do
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {

					switch (progress) {
						case 0:
							_vibratorFrequency = Level.Low;
							break;
						case 1:
							_vibratorFrequency = Level.Medium;
							break;
						case 2:
							_vibratorFrequency = Level.High;
							break;
						default:
							_vibratorFrequency = Level.Medium;
							break;
					}
				}
			});
			sBarLampBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					
					switch (progress) {
					case 0:
						_lampBrightness = Level.Low;
						break;
					case 1:
						_lampBrightness = Level.Medium;
						break;
					case 2:
						_lampBrightness = Level.High;
						break;
					default:
						_lampBrightness = Level.Medium;
						break;
				}
					
				}
			});
			sBarSpeakerVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					_speakerVolume = (float)(progress + 1) / 10;
					
				}
			});
			sBarSpeakerRate.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Nothing to do
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					_speakerRate = (float)(progress + 5) / 10;
					
				}
			});
			btnSolve.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					SolveIncidentTask solveTask = new SolveIncidentTask();
					solveTask.execute((Void)null);
					
				}
			});
			
			LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
			
			if (_team == _ownTeam || _id == app.get_currentIncident()) {
				if (_rc == null) {
					if (app.get_currentRC() == null) {
						GetRCTask rcTask = new GetRCTask();
						rcTask.execute((Void)null);
					} else {
						_rc = app.get_currentRC();
					}
				}
			} else {
				Toast noAllocateNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"No tiene asignado este incidente", Toast.LENGTH_SHORT);
				noAllocateNotification.show();
				
				disableControls();
				
			}
			
		}
		
		return view;
	}
	
	private void disableControls() {
		tgbVibrator.setEnabled(false);
		tgbLamp.setEnabled(false);
		tgbSpeaker.setEnabled(false);
		btnSolve.setEnabled(false);
	}
	
	class SolveIncidentTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			LoRAM_Rescue app = (LoRAM_Rescue)getActivity().getApplication();
			
			IncidentManagerPrx manager = app.getIncidentManagerProxy();
			
			try {
				manager.solveIncident(_victim);
				
				return true;
			} catch (AccessDenied e) {
				e.printStackTrace();
				
				return false;
			} catch (Exception e) {
				e.printStackTrace();
				
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				Toast solvedNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"El incidente ha sido marcado como resuelto", Toast.LENGTH_SHORT);
				solvedNotification.show();
				
				getActivity().finish();
			} else {
				Toast errorNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Error al resolver el incidente", Toast.LENGTH_SHORT);
				errorNotification.show();
			}
		}
	}
	
	class ActivateTask extends AsyncTask<ActivationRequest, Void, ActuatorType> {

		@Override
		protected ActuatorType doInBackground(ActivationRequest... request) {
			try {
				_rc.activate(request[0]);
				
				return null;
					
			} catch (NotAvailableActuator e) {
				e.printStackTrace();
					
				return request[0].typeReq;
			} catch (NotSuitable e) {
				e.printStackTrace();
				
				return request[0].typeReq;
			}
		}
		
		@Override
		protected void onPostExecute(ActuatorType type) {
			String sType = "";
			if (type != null) {
				switch (type) {
					case TypeVibrator:
						sType = "vibrador";
						
						//Disable control
						tgbVibrator.setChecked(false);
						break;
					case TypeLamp:
						sType = "lampara";
						
						//Disable control
						tgbLamp.setChecked(false);
						break;
					case TypeSpeaker:
						sType = "altavoz";
						
						//Disable control
						tgbSpeaker.setChecked(false);
						break;

				}
				Toast notAvailableNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Imposible activar " + sType, Toast.LENGTH_SHORT);
				notAvailableNotification.show();
				
			}
		}
			
	}
	
	class DeactivateTask extends AsyncTask<DeactivationRequest, Void, ActuatorType> {

		@Override
		protected ActuatorType doInBackground(DeactivationRequest... request) {
			try {
				_rc.deactivate(request[0]);
				
				return null;
					
			} catch (Exception e) {
				e.printStackTrace();
					
				return request[0].typeReq;
			}
		}
		
		@Override
		protected void onPostExecute(ActuatorType type) {
			String sType = "";
			if (type != null) {
				switch (type) {
					case TypeVibrator:
						sType = "vibrador";
						
						break;
					case TypeLamp:
						sType = "lampara";
						
						break;
					case TypeSpeaker:
						sType = "altavoz";
						
						break;

				}
				Toast errorNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Error al desconectar " + sType, Toast.LENGTH_SHORT);
				errorNotification.show();
				
			}
		}
			
	}
	
	class GetRCTask extends AsyncTask<Void, Void, RemoteControlPrx> {

		private LoRAM_Rescue _app;
		
		@Override
		protected RemoteControlPrx doInBackground(Void... params) {
			_app = (LoRAM_Rescue)getActivity().getApplication();
			
			IncidentManagerPrx manager = _app.getIncidentManagerProxy();
			
			try {
				RemoteControlPrx rc = manager.getRemoteControl(_victim);
				
				return rc;
			} catch (AccessDenied e) {
				e.printStackTrace();
				
				return null;
			} catch (ImposibleConnectWithUser e) {
				e.printStackTrace();
				
				return null;
			}
			
		}
		
		@Override
		protected void onPostExecute(RemoteControlPrx rc) {
			if (rc != null) {
				_rc = rc;
				_app.set_currentRC(rc);
			} else {
				Toast errorNotification = Toast.makeText(getActivity().getApplicationContext(), 
						"Imposible interactuar con la víctima", Toast.LENGTH_SHORT);
				errorNotification.show();
				
				disableControls();
			}
			
			
		}
	}
	
}
