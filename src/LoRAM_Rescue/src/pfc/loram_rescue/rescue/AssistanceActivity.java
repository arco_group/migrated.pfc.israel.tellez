package pfc.loram_rescue.rescue;

import java.util.HashMap;

import pfc.loram_rescue.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;

public class AssistanceActivity extends FragmentActivity implements TabHost.OnTabChangeListener {

	private TabHost _assistance;
	private HashMap<String, TabInfo> _mapTabInfo;
	private TabInfo _lastTab;
	
	private class TabInfo {
		private String _tag;
		private Class<?> _class;
		private Bundle _args;
		private Fragment _fragment;
		public TabInfo(String tag, Class<?> clss, Bundle args) {
			_tag = tag;
			_class = clss;
			_args = args;
		}
	}
	
	class TabFactory implements TabContentFactory {
		
		private final Context _context;
		
		public TabFactory(Context context) {
			_context = context;
		}
		
		public View createTabContent(String tag) {
			View v = new View(_context);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			
			return v;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assistance);
		
		_mapTabInfo = new HashMap<String, AssistanceActivity.TabInfo>();
		_lastTab = null;
		
		initialiseTabHost(savedInstanceState);
		if (savedInstanceState != null) {
			_assistance.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("tab", _assistance.getCurrentTabTag());
		super.onSaveInstanceState(outState);
	}
	
	private void initialiseTabHost(Bundle args) {
		_assistance = (TabHost)findViewById(android.R.id.tabhost);
		_assistance.setup();
		
		TabHost.TabSpec tabSpec;
		TabInfo tabInfo;
		
		tabSpec = _assistance.newTabSpec("IncidentData");
		tabSpec.setIndicator("Datos incidente");
		tabInfo = new TabInfo("IncidentData", IncidentDataActivity.class, args);
		AssistanceActivity.addTab(this, _assistance, tabSpec, tabInfo);
		_mapTabInfo.put(tabInfo._tag, tabInfo);
		
		tabSpec = _assistance.newTabSpec("Location");
		tabSpec.setIndicator("Localización");
		tabInfo = new TabInfo("Location", LocationActivity.class, args);
		AssistanceActivity.addTab(this, _assistance, tabSpec, tabInfo);
		_mapTabInfo.put(tabInfo._tag, tabInfo);
		
		tabSpec = _assistance.newTabSpec("Controls");
		tabSpec.setIndicator("Controles");
		tabInfo = new TabInfo("Controls", ControlsActivity.class, args);
		AssistanceActivity.addTab(this, _assistance, tabSpec, tabInfo);
		_mapTabInfo.put(tabInfo._tag, tabInfo);
		
		this.onTabChanged("IncidentData");
		_assistance.setOnTabChangedListener(this);
	}
	
	private static void addTab(AssistanceActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
		tabSpec.setContent(activity.new TabFactory(activity));
		String tag = tabSpec.getTag();
		
		tabInfo._fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
		if (tabInfo._fragment != null && !tabInfo._fragment.isDetached()) {
			FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			ft.detach(tabInfo._fragment);
			ft.commit();
			activity.getSupportFragmentManager().executePendingTransactions();
		}
		
		tabHost.addTab(tabSpec);
	}
	
	@Override
	public void onTabChanged(String tabId) {
		TabInfo newTab = _mapTabInfo.get(tabId);
		
		if (_lastTab != newTab) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			if (_lastTab != null) {
				if (_lastTab._fragment != null) {
					ft.detach(_lastTab._fragment);
				}
			}
			if (newTab != null) {
				if (newTab._fragment == null) {
					newTab._fragment = Fragment.instantiate(getApplicationContext(), newTab._class.getName(),
							newTab._args);
					ft.add(R.id.tabContent, newTab._fragment, newTab._tag);
				} else {
					ft.attach(newTab._fragment);
				}
			}
			
			_lastTab = newTab;
			ft.commit();
			getSupportFragmentManager().executePendingTransactions();
		}
	}
}
