package pfc.loram_rescue;

import Ice.ConnectFailedException;
import LoRAM.RescuerData;
import LoRAM.UserAlreadyExists;
import LoRAM.UserManagerPrx;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	/**
	 * Keep track of the register task to ensure we can cancel it if requested.
	 */
	private UserRegisterTask mAuthTask = null;

	// Values for data at the time of the register attempt.
	private String mNif;
	private String mPassword;
	private String mName;
	private String mSurname1;
	private String mSurname2;
	private int mPhone;
	private int mTeam;

	// UI references.
	private EditText mNifView;
	private EditText mPasswordView;
	private EditText mRepeatPasswordView;
	private EditText mNameView;
	private EditText mSurname1View;
	private EditText mSurname2View;
	private EditText mPhoneView;
	private EditText mTeamView;
	private View mRegisterFormView;
	private View mRegisterStatusView;
	private TextView mRegisterStatusMessageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		// Set up the register form.
		Bundle bundle = this.getIntent().getExtras();
		mNif = bundle.getString("nif");
		mNifView = (EditText) findViewById(R.id.nif);
		mNifView.setText(mNif);
		mPasswordView = (EditText) findViewById(R.id.password);
		mRepeatPasswordView = (EditText) findViewById(R.id.repeat_password);
		mNameView = (EditText) findViewById(R.id.name);
		mSurname1View = (EditText) findViewById(R.id.surname1);
		mSurname2View = (EditText) findViewById(R.id.surname2);
		mPhoneView = (EditText) findViewById(R.id.phone);
		mTeamView = (EditText) findViewById(R.id.team);
		mTeamView
			.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,
					KeyEvent keyEvent) {
				if (id == R.id.register || id == EditorInfo.IME_NULL) {
					attemptRegister();
					return true;
				}
				return false;
			}
		});
		
		mRegisterFormView = findViewById(R.id.register_form);
		mRegisterStatusView = findViewById(R.id.register_status);
		mRegisterStatusMessageView = (TextView) findViewById(R.id.register_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptRegister();
					}
				});
	}
	
	/**
	 * Attempts to register the account specified by the register form.
	 * If there are form errors (invalid nif, missing fields, etc.), the
	 * errors are presented and no actual register attempt is made.
	 */
	public void attemptRegister() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mNifView.setError(null);
		mPasswordView.setError(null);
		mRepeatPasswordView.setError(null);
		mNameView.setError(null);
		mSurname1View.setError(null);
		mSurname2View.setError(null);
		mPhoneView.setError(null);
		mTeamView.setError(null);

		// Store values at the time of the register attempt.
		mNif = mNifView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		String repeatPassword = mRepeatPasswordView.getText().toString();
		mName = mNameView.getText().toString();
		mSurname1 = mSurname1View.getText().toString();
		mSurname2 = mSurname2View.getText().toString();
		String txtPhone = mPhoneView.getText().toString();
		if (TextUtils.isEmpty(txtPhone)) {
			mPhone = 0;
		} else {
			mPhone = Integer.parseInt(txtPhone);
		}
		String txtTeam = mTeamView.getText().toString();
		if (TextUtils.isEmpty(txtTeam)) {
			mTeam = 0;
		} else {
			mTeam = Integer.parseInt(txtTeam);
		}

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		} else if (!mPassword.equals(repeatPassword)) {
			mRepeatPasswordView.setError(getString(R.string.error_password_not_match));
			focusView = mRepeatPasswordView;
			cancel = true;
		}

		// Check for a valid nif.
		if (TextUtils.isEmpty(mNif)) {
			mNifView.setError(getString(R.string.error_field_required));
			focusView = mNifView;
			cancel = true;
			// Check that last character is a letter
		} else if (!mNif.substring(mNif.length() - 1).matches("[A-Za-z]")) {
			mNifView.setError(getString(R.string.error_invalid_nif));
			focusView = mNifView;
			cancel = true;
		}

		// Check for a valid name.
		if (TextUtils.isEmpty(mName)) {
			mNameView.setError(getString(R.string.error_field_required));
			focusView = mNameView;
			cancel = true;
		}
		
		if (cancel) {
			// There was an error; don't attempt register and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user register attempt.
			mRegisterStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserRegisterTask();
			mAuthTask.execute((Void) null);
		}
	}
	
	/**
	 * Shows the progress UI and hides the register form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mRegisterStatusView.setVisibility(View.VISIBLE);
			mRegisterStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mRegisterFormView.setVisibility(View.VISIBLE);
			mRegisterFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mRegisterStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Represents an asynchronous registration task used to authenticate
	 * the user.
	 */
	public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {
		
		private RescuerData user = new RescuerData(mNif, mName, mSurname1, mSurname2, mPhone, mTeam);
		
		@Override
		protected Boolean doInBackground(Void... params) {
			UserManagerPrx manager = null;
			LoRAM_Rescue app = (LoRAM_Rescue) getApplication();
			
			try {
				// Network access.
				manager = app.getUserManagerProxy();
			} catch (ConnectFailedException e){
				Log.i(getClass().getSimpleName(), e.toString());
				return false;
			}

			try {
				manager.registerRescuer(user, mPassword);
				return true;
			} catch (UserAlreadyExists e) {
				e.printStackTrace();
				
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				LoRAM_Rescue app = (LoRAM_Rescue) getApplication();
				app.set_user(user);
				Toast registeredNotification = Toast.makeText(getApplicationContext(), "Usuario registrado", 
						Toast.LENGTH_SHORT);
				registeredNotification.show();
				
				finish();
			} else {
				mNifView.setError(getString(R.string.error_user_already_exists));
				mNifView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
