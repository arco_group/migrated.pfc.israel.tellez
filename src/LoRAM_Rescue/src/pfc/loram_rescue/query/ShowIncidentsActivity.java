package pfc.loram_rescue.query;


import pfc.loram_rescue.IncidentAdapter;
import pfc.loram_rescue.LoRAM_Rescue;
import pfc.loram_rescue.R;
import pfc.loram_rescue.rescue.AssistanceActivity;
import LoRAM.AccessDenied;
import LoRAM.IncidentData;
import LoRAM.IncidentManagerPrx;
import LoRAM.RescuerData;
import LoRAM.UserManagerPrx;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ShowIncidentsActivity extends Activity {
	
	private ListView _lstIncidentsList;
	private Button _btnRefresh;
	
	private IncidentData[] _incidentsList;
	
	private AlertDialog.Builder _logoutDialog;
	
	private Activity _thisActivity;
	private LoRAM_Rescue _app;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_incidents);
		
		_thisActivity = this;
		_app = (LoRAM_Rescue) getApplication();
		
		//Activity controllers
		_lstIncidentsList = (ListView)findViewById(R.id.lstIncidentsList);
		_btnRefresh = (Button)findViewById(R.id.btnRefresh);
		_logoutDialog = new AlertDialog.Builder(this);
		
		//Controllers setting
		_lstIncidentsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				
				Intent assistance = new Intent(getApplicationContext(), AssistanceActivity.class);
				Bundle bundle = new Bundle();
				bundle.putInt("id", _incidentsList[position].idIncident);
				bundle.putString("victim", _incidentsList[position].victim);
				String timestamp = String.valueOf(_incidentsList[position].dateIncident.day) + "/" + 
						String.valueOf(_incidentsList[position].dateIncident.month) + "/" +
						String.valueOf(_incidentsList[position].dateIncident.year) + " " +
						String.valueOf(_incidentsList[position].timeIncident.hour) + ":" +
						String.valueOf(_incidentsList[position].timeIncident.minute) + ":" +
						String.valueOf(_incidentsList[position].timeIncident.second);
				bundle.putString("date", timestamp);
				bundle.putString("state", _incidentsList[position].state.name());
				bundle.putString("type", _incidentsList[position].typeIncident.name());
				bundle.putInt("team", _incidentsList[position].rescueTeam);
				assistance.putExtras(bundle);
				startActivity(assistance);
			}
			
		});
		
		_btnRefresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ListIncidentTask listTask = new ListIncidentTask();
				listTask.execute(_app);
			}
		});
		
		_logoutDialog.setTitle("Cerrar sesión y salir");
		_logoutDialog.setMessage("¿Desea cerrar la sesión actual y salir?");
		_logoutDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				LogoutTask logout = new LogoutTask();
				logout.execute((Void)null);
				
				//Close activity
				finish();

				dialog.cancel();
			}
		});
		_logoutDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		
		ListIncidentTask listTask = new ListIncidentTask();
		listTask.execute(_app);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {			
			_logoutDialog.show();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public IncidentData[] get_incidentsList() {
		return _incidentsList;
	}

	public class ListIncidentTask extends AsyncTask<LoRAM_Rescue, Void, IncidentData[]> {

		@Override
		protected IncidentData[] doInBackground(LoRAM_Rescue... params) {
			LoRAM_Rescue app = params[0];
			
			IncidentManagerPrx manager = _app.getIncidentManagerProxy();
			RescuerData user = app.get_user();
			
			try {
				IncidentData[] incidentsList = manager.listIncidents(user.nif);
				
				return incidentsList;
			} catch (AccessDenied e) {
				e.printStackTrace();
				Toast accessDeniedNotification = Toast.makeText(getApplicationContext(), 
						"Error: debe iniciar sesión antes de seguir", Toast.LENGTH_SHORT);
				accessDeniedNotification.show();
				
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(IncidentData[] list) {
			IncidentAdapter adapter = new IncidentAdapter(_thisActivity, list);
			_incidentsList = list;
			_lstIncidentsList.setAdapter(adapter);
		}
		
	}
	
	class LogoutTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			LoRAM_Rescue app = (LoRAM_Rescue) getApplication();
			UserManagerPrx manager = app.getUserManagerProxy();
			RescuerData user = app.get_user();
			
			try {
				manager.logout(user.nif);
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
	}
}
