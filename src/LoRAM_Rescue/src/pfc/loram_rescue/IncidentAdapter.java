package pfc.loram_rescue;

import LoRAM.IncidentData;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class IncidentAdapter extends ArrayAdapter<IncidentData> {

	Activity _context;
	IncidentData[] _data;
	
	public IncidentAdapter(Activity context, IncidentData[] data) {
		super(context, R.layout.adapter_incident, data);
		
		_context = context;
		_data = data;
	}
	
	static class ViewHolder {
		TextView txvVictim;
		TextView txvDate;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View item = convertView;
		ViewHolder holder;
		
		if(item == null) {
			LayoutInflater inflater = _context.getLayoutInflater();
			item = inflater.inflate(R.layout.adapter_incident, null);
			
			holder = new ViewHolder();
			holder.txvVictim = (TextView)item.findViewById(R.id.txvVictim);
			holder.txvDate = (TextView)item.findViewById(R.id.txvDate);
			
			item.setTag(holder);
		} else {
			holder = (ViewHolder)item.getTag();
		}
		
		holder.txvVictim.setText(_data[position].victim);
		
		String timestamp = String.valueOf(_data[position].dateIncident.day) + "/" + 
				String.valueOf(_data[position].dateIncident.month) + "/" +
				String.valueOf(_data[position].dateIncident.year) + " " +
				String.valueOf(_data[position].timeIncident.hour) + ":" +
				String.valueOf(_data[position].timeIncident.minute) + ":" +
				String.valueOf(_data[position].timeIncident.second);
		holder.txvDate.setText(timestamp);
		
		return item;
	}

	public IncidentData[] get_data() {
		return _data;
	}

	public void set_data(IncidentData[] data) {
		this._data = data;
	}
}
