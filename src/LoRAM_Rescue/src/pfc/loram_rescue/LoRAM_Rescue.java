package pfc.loram_rescue;

import Ice.ObjectPrx;
import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import LoRAM.RemoteControlPrx;
import LoRAM.RescuerData;
import LoRAM.UserManagerPrx;
import LoRAM.UserManagerPrxHelper;
import android.app.Application;
import android.os.Build.VERSION;

public class LoRAM_Rescue extends Application {
	
	private Ice.Communicator _broker;
	private Ice.ObjectAdapter _adapter;
	
	private UserManagerPrx _userManager;
	private IncidentManagerPrx _incidentManager;
	
	private RescuerData _user;
	private int _currentIncident; //The incident that is solving now
	private RemoteControlPrx _currentRC;
	
	
	private final static String MANAGEMENT_ENDPOINT = 
			"management -t:tcp -h 192.168.1.22 -p 10000:tcp -h 192.168.56.1 -p 10000";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		//Communication setting
		if (VERSION.SDK_INT == 8) {// android.os.Build.VERSION_CODES.FROYO (8)
			//
			// Workaround for a bug in Android 2.2 (Froyo).
			//
			// See http://code.google.com/p/android/issues/detail?id=9431
			//
							
			java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
			java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
		}
		
		initializeCommunicator();
		
		_userManager = null;
		_currentIncident = 0;
		_currentRC = null;
	}
	
	@Override
	public void onTerminate() {
		super.onTerminate();
		
		if(_broker != null) {
			try {
				_broker.destroy();
			}
			catch(Ice.LocalException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initializeCommunicator() {
		try {
			_broker = Ice.Util.initialize();
			_adapter = _broker.createObjectAdapterWithEndpoints("adapter", "tcp -p 10000");
		
			_adapter.activate();
		} catch(Ice.LocalException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public UserManagerPrx getUserManagerProxy() {
		
		if (_userManager == null) {
			ObjectPrx base = _broker.stringToProxy(MANAGEMENT_ENDPOINT);
			_userManager = UserManagerPrxHelper.checkedCast(base);
		}
		
		return _userManager;
	}

	public IncidentManagerPrx getIncidentManagerProxy() {
		
		if (_incidentManager == null) {
			try {
				_incidentManager = _userManager.getIncidentManager(_user.nif);
			} catch (AccessDenied e) {
				e.printStackTrace();
			}
		}
		
		return _incidentManager;
	}

	public Ice.Communicator get_broker() {
		return _broker;
	}

	public Ice.ObjectAdapter get_adapter() {
		return _adapter;
	}

	public RescuerData get_user() {
		return _user;
	}

	public void set_user(RescuerData _user) {
		this._user = _user;
	}

	public int get_currentIncident() {
		return _currentIncident;
	}

	public void set_currentIncident(int _currentIncident) {
		this._currentIncident = _currentIncident;
	}

	public RemoteControlPrx get_currentRC() {
		return _currentRC;
	}

	public void set_currentRC(RemoteControlPrx _currentRC) {
		this._currentRC = _currentRC;
	}

}
