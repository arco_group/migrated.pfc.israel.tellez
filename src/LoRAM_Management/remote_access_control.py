#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import RemoteControl, RemoteControlPrx, \
    ActuatorType, LocationType, NotAvailableActuator, ActuatorOff, NotSuitable


class AccessControl():
    def __init__(self, battery):
        self.battery = battery

        self.lastBatteryLevel = -1

        self.HIGH_LIMIT = 75
        self.MEDIUM_LIMIT = 50
        self.LOW_LIMIT = 15
        self.BATTERY_STATUS_CHANGE_LIMIT = 5

    def isSuitableActivate(self, actuatorsOn):

        level = self.battery.getLevel()

        print "Current level of battery: " + str(level)

        if self.lastBatteryLevel == -1:
            self.lastBatteryLevel = level

        levelDifference = self.lastBatteryLevel - level
        numOn = 0
        for actuator in actuatorsOn:
            if actuator:
                numOn += 1

        suitable = False

        if numOn >= 2:
            if level >= self.HIGH_LIMIT:
                suitable = True
            elif level > self.MEDIUM_LIMIT and levelDifference < self.BATTERY_STATUS_CHANGE_LIMIT:
                suitable = True
        elif numOn == 1:
            if level >= self.MEDIUM_LIMIT:
                suitable = True
            elif level > self.LOW_LIMIT and levelDifference < self.BATTERY_STATUS_CHANGE_LIMIT:
                suitable = True
        else:
            if level >= self.LOW_LIMIT:
                suitable = True

        self.lastBatteryLevel = level

        return suitable

    def isSuitableGetLocation(self, actuatorsOn):

        level = self.battery.getLevel()

        print "Current level of battery: " + str(level)

        if self.lastBatteryLevel == -1:
            self.lastBatteryLevel = level

        levelDifference = self.lastBatteryLevel - level
        numOn = 0
        for actuator in actuatorsOn:
            if actuator:
                numOn += 1

        suitable = False

        if numOn >= 2:
            if level >= self.MEDIUM_LIMIT:
                suitable = True
            elif level > self.LOW_LIMIT and levelDifference < self.BATTERY_STATUS_CHANGE_LIMIT:
                suitable = True
        else:
            if level >= self.LOW_LIMIT:
                suitable = True

        self.lastBatteryLevel = level

        return suitable


class RemoteControlI(RemoteControl):
    def __init__(self, sentry):
        self.proxy = None
        self.sentry = sentry
        self.rcManager = sentry.getRCManager()
        self.accessC = AccessControl(self.rcManager.getBattery())

        self.speaker = {'active': False, 'proxy': None}
        self.lamp = {'active': False, 'proxy': None}
        self.vibrator = {'active': False, 'proxy': None}
        self.gps = None

    def add_to(self, adapter):
        self.proxy = RemoteControlPrx.uncheckedCast(adapter.addWithUUID(self))
        return self.proxy

    def getProxy(self):
        return self.proxy

    def activate(self, request, current=None):
        actuatorsOn = [self.speaker['active'], self.lamp['active'], self.vibrator['active']]

        if self.accessC.isSuitableActivate(actuatorsOn):
            if request.typeReq == ActuatorType.TypeSpeaker:
                print "activating speaker"

                if self.speaker['proxy'] == None:
                    self.speaker['proxy'] = self.rcManager.getSpeaker()

                self.speaker['proxy'].configure(request.volume, request.rate)
                self.speaker['proxy'].turnOn()
                self.speaker['active'] = True

            elif request.typeReq == ActuatorType.TypeLamp:
                print "activating lamp"

                if self.lamp['proxy'] == None:
                    self.lamp['proxy'] = self.rcManager.getLamp()

                self.lamp['proxy'].configure(request.brightness)
                self.lamp['proxy'].turnOn()
                self.lamp['active'] = True

            elif request.typeReq == ActuatorType.TypeVibrator:
                print "activating vibrator"

                if self.vibrator['proxy'] == None:
                    self.vibrator['proxy'] = self.rcManager.getVibrator()

                self.vibrator['proxy'].configure(request.frequency)
                self.vibrator['proxy'].turnOn()
                self.vibrator['active'] = True
        else:
            raise NotSuitable("Not been able to perform the action due to the conditions " \
                                  "of the phone")

    def deactivate(self, request, current=None):

        if request.typeReq == ActuatorType.TypeSpeaker:
            self.speaker['proxy'].turnOff()
            self.speaker['active'] = False

        elif request.typeReq == ActuatorType.TypeLamp:
            self.lamp['proxy'].turnOff()
            self.lamp['active'] = False

        elif request.typeReq == ActuatorType.TypeVibrator:
            self.vibrator['proxy'].turnOff()
            self.vibrator['active'] = False

    def getLocation(self, request, current=None):
        actuatorsOn = [self.speaker['active'], self.lamp['active'], self.vibrator['active']]

        if self.accessC.isSuitableGetLocation(actuatorsOn):
            if request.locType == LocationType.GPSType:
                print "obtaining location"

                if self.gps == None:
                    self.gps = self.rcManager.getGPS()

                self.gps.configure(request.timeBetweenUpdates, request.distanceChangeForUpdates)
                location = self.gps.getLocation()

                return location
            else:
                raise NotAvailableActuator("Only the GPS type is available")
        else:
            raise NotSuitable("Not been able to perform the action due to the conditions " \
                                  "of the phone")
