#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import MySQLdb
import Ice

sys.path.append('./src/utils')
from communication_elements import IncidentDataC
from dbBroker import DBBroker
from session import Session
from datesChanger import DatesChanger

from remote_access_control import RemoteControlI

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import IncidentManager, IncidentManagerPrx, \
    ActuatorType, StateType, IssueType, \
    NotAvailableActuator, AccessDenied, ImposibleConnectWithUser, NotExistRCModule

class IncidentNotFound(Exception):
    def __init__(self, reason=''):
        self.reason = reason

    def __str__(self):
        return "Error: " + reason

class Incident():
    def __init__(self, victim, date, typeIncident, state=StateType.NoAllocate, rescueTeam=0):
        self.victim = victim
        self.date = date
        self.state = state
        self.typeIncident = typeIncident
        self.rescueTeam = rescueTeam

        self.dbBroker = DBBroker()
        self.datesChanger = DatesChanger()

    def __eq__(self, other):
        equal = self.victim == other.victim and self.date == other.date and \
            self.state == other.state and self.typeIncident == other.typeIncident and \
            self.rescueTeam == other.rescueTeam

        return equal

    # DB access

    def insertIncident(self, incident):
        query = "INSERT INTO Incidents(victim, date, state, type, rescue_team) " \
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', %s);" % \
            (incident.victim, incident.date, incident.state, incident.typeIncident, \
                 incident.rescueTeam)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def findIncidentId(self, victim, date):
        query = "SELECT id FROM Incidents WHERE victim=\'%s\' AND date=\'%s\'" % \
            (victim, date)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        result = cursor.fetchone()[0]
        cursor.close()
        self.dbBroker.returnConnection(con)

        return result

    def updateIncident(self, incident, idIncident):
        query = "UPDATE Incidents SET victim=\'%s\', date=\'%s\', state=\'%s\', type=\'%s\', " \
            "rescue_team=%s WHERE id=%s" % \
            (incident.victim, incident.date, incident.state, incident.typeIncident, \
                 incident.rescueTeam, idIncident)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def deleteIncident(self, idIncident):
        query = "DELETE FROM Incidents WHERE id=%s" % (idIncident)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def findIncident(self, idIncident):
        query = "SELECT victim, date, type, state, rescue_team FROM Incidents " \
            "WHERE id=%s" % (idIncident)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        result = cursor.fetchone()
        cursor.close()
        self.dbBroker.returnConnection(con)

        if result == None:
            raise IncidentNotFound()
        else:
            return Incident(result[0], result[1], result[2], result[3], result[4])

    def findUnresolvedIncidents(self):
        query = "SELECT id, victim, date, state, type, rescue_team FROM Incidents " \
            "WHERE state <> \'%s\'" % (StateType.Resolved)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        self.dbBroker.returnConnection(con)

        resultList = []
        for value in result:
            date = self.datesChanger.obtainDateToTimestamp(value[2])
            time = self.datesChanger.obtainTimeToTimestamp(value[2])

            if value[3] == 'NoAllocate':
                state = StateType.NoAllocate
            elif value[3] == 'Allocated':
                state = StateType.Allocated
            else:
                state = StateType.UnknownState

            if value[4] == 'DetectedFall':
                typeIncident = IssueType.DetectedFall
            elif value[4] == 'DetectedCollision':
                typeIncident = IssueType.DetectedCollision
            elif value[4] == 'Motion':
                typeIncident = IssueType.Motion
            elif value[4] == 'AlertButtonPressed':
                typeIncident = IssueType.AlertButtonPressed
            else:
                typeIncident = IssueType.UnknownIssue

            incident = IncidentDataC(value[0], value[1], date, time, state, typeIncident, value[5])
            resultList.append(incident)

        return resultList


class IncidentManagerI(IncidentManager):
    def __init__(self):
        self.proxy = None
        self.adapter = None

        self.session = Session()
        self.datesChanger = DatesChanger()
        self.genIncident = Incident('', '', '')

    def add_to(self, adapter):
        self.proxy = IncidentManagerPrx.uncheckedCast(adapter.addWithUUID(self))
        self.adapter = adapter

        return self.proxy

    def getProxy(self):
        return self.proxy

    def notifyIssue(self, email, sentry, issue, current=None):
        if self.session.checkUser(email):
            print "Issue received by " + email
            self.session.addSentry(email, sentry)

            timestamp = self.datesChanger.createTimestamp(issue.dateIss, issue.timeIss)
            incident = Incident(email, timestamp, issue.typeIss)
            self.genIncident.insertIncident(incident)
            idIncident = self.genIncident.findIncidentId(incident.victim, incident.date)

            self.session.addIncident(email, idIncident)
        else:
            raise AccessDenied("The user isn't logged in to the system")

    def listIncidents(self, nif, current=None):
        if self.session.checkRescuer(nif):
            print "Sending incidents list to " + nif
            result = self.genIncident.findUnresolvedIncidents()
            return result
        else:
            raise AccessDenied("The user isn't logged in to the system")

    def getRemoteControl(self, email, current=None):
        if self.session.checkUser(email):
            print "Sending remote control of " + email

            if self.session.checkRC(email):
                rc = self.session.getRC(email)
                return rc.getProxy()
            else:
                try:
                    sentry = self.session.getSentry(email)
                    rc = RemoteControlI(sentry)
                    proxy = rc.add_to(self.adapter)
                    self.session.addRC(email, rc)

                    return proxy
                except KeyError:
                    raise ImposibleConnectWithUser("The user has not recently notified an incident")
                except NotExistRCModule:
                    raise ImposibleConnectWithUser("The user has not rc module")
        else:
            raise AccessDenied("The user isn't logged in to the system")

    def allocateIncident(self, team, idIncident, current=None):
        # Change state in DB
        incident = self.genIncident.findIncident(idIncident)
        incident.rescueTeam = team
        incident.state = StateType.Allocated
        self.genIncident.updateIncident(incident, idIncident)

    def solveIncident(self, email, current=None):
        if self.session.checkUser(email):
            print "Mark incident as solved"
            idIncident = self.session.getIncident(email)

            # Change state in DB
            incident = self.genIncident.findIncident(idIncident)
            incident.state = StateType.Resolved
            self.genIncident.updateIncident(incident, idIncident)

            # Delete incident to the session
            self.session.deleteIncident(email)

            # Notify to sentry
            sentry = self.session.getSentry(email)
            sentry.solveIncident()
        else:
            raise AccessDenied("The user isn't logged in to the system")
