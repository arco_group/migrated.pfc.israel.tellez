#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys, traceback, Ice

sys.path.append('./src/utils')

from user_management import UserManagerI

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import UserManagerPrx


class LoRAM_Management(Ice.Application):

    def run(self, args):
        if len(args) > 1:
            print self.appName() + ": too many arguments"
            return 1

        self.shutdownOnInterrupt()

        broker = self.communicator()
        adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp -p 10000')
        adapter.activate()

        manager = UserManagerI()
        manager_proxy = adapter.add(manager, broker.stringToIdentity('management'))
        manager.proxy = UserManagerPrx.uncheckedCast(manager_proxy)
        manager.adapter = adapter

        print broker.proxyToString(manager_proxy)
        print "\n"


        broker.waitForShutdown()

        return 0


app = LoRAM_Management()

sys.exit(app.main(sys.argv))
