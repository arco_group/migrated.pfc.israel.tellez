#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice

from datetime import datetime

sys.path.append('./src/utils')
from communication_elements import UserDataC, RescuerDataC
from dbBroker import DBBroker
from session import Session

from incident_management import IncidentManagerI

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import UserManager, UserManagerPrx, \
    UserNotFound, UserNotLogged, AccessDenied, UserAlreadyExists


class UserOrPasswordIncorrect(Exception):
    def __init__(self, reason=''):
        self.reason = reason

    def __str__(self):
        return "Error: " + reason


class User():
    def __init__(self, email, password, name='', surname1='', surname2='', phone=0, numAA=0):
        self.email = email
        self.password = password
        self.name = name
        self.surname1 = surname1
        self.surname2 = surname2
        self.phone = phone
        self.numAA = numAA

        self.dbBroker = DBBroker()

    def getData(self):
        return UserDataC(self.email, self.name, self.surname1, self.surname2, self.phone, self.numAA)

    def __eq__(self, other):
        equal = self.email == other.email and self.password == other.password and \
            self.name == other.name and self.surname1 == other.surname1 and \
            self.surname2 == other.surname2 and self.phone == other.phone and \
            self.numAA == other.numAA

        return equal

    # DB access

    def insertUser(self, user):
        query = "INSERT INTO Users(email, password, name, surname1, surname2, phone, numAA) " \
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %s, %s);" % \
            (user.email, user.password, user.name, user.surname1, user.surname2, user.phone, \
                 user.numAA)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def updateUser(self, user, oldEmail):
        query = "UPDATE Users SET email=\'%s\', password=\'%s\', name=\'%s\', surname1=\'%s\', " \
            "surname2=\'%s\', phone=%s, numAA=%s WHERE email=\'%s\'" % \
            (user.email, user.password, user.name, user.surname1, user.surname2, user.phone, \
                 user.numAA, oldEmail)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def deleteUser(self, user):
        query = "DELETE FROM Users WHERE email=\'%s\' AND password=\'%s\'" % \
            (user.email, user.password)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def existUser(self, email):
        query = "SELECT * FROM Users WHERE email=\'%s\'" % (email)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        num = cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

        if num == 0:
            return False
        else:
            return True

    def findUser(self, email, password):
        query = "SELECT email, password, name, surname1, surname2, phone, numAA FROM Users " \
            "WHERE email=\'%s\' AND password=\'%s\'" % \
            (email, password)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        result = cursor.fetchone()
        cursor.close()
        self.dbBroker.returnConnection(con)

        if result == None:
            raise UserOrPasswordIncorrect()
        else:
            return User(result[0], result[1], result[2], result[3], result[4], result[5], result[6])


class Rescuer():
    def __init__(self, nif, password, name='', surname1='', surname2='', phone=0, team=0):
        self.nif = nif
        self.password = password
        self.name = name
        self.surname1 = surname1
        self.surname2 = surname2
        self.phone = phone
        self.team = team

        self.dbBroker = DBBroker()

    def getData(self):
        return RescuerDataC(self.nif, self.name, self.surname1, self.surname2, self.phone, self.team)

    def __eq__(self, other):
        equal = self.nif == other.nif and self.password == other.password and \
            self.name == other.name and self.surname1 == other.surname1 and \
            self.surname2 == other.surname2 and self.phone == other.phone and \
            self.team == other.team

        return equal

    # DB access

    def insertRescuer(self, rescuer):
        query = "INSERT INTO Rescue_staff(nif, password, name, surname1, surname2, phone, team) "\
            "VALUES(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %s, %s);" % \
            (rescuer.nif, rescuer.password, rescuer.name, rescuer.surname1, \
                 rescuer.surname2, rescuer.phone, rescuer.team)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def updateRescuer(self, rescuer, oldNif):
        query = "UPDATE Rescue_staff SET nif=\'%s\', password=\'%s\', name=\'%s\', " \
            "surname1=\'%s\', surname2=\'%s\', phone=%s, team=%s WHERE nif=\'%s\'" % \
            (rescuer.nif, rescuer.password, rescuer.name, rescuer.surname1, \
                 rescuer.surname2, rescuer.phone, rescuer.team, oldNif)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def deleteRescuer(self, rescuer):
        query = "DELETE FROM Rescue_staff WHERE nif=\'%s\' AND password=\'%s\'" % \
            (rescuer.nif, rescuer.password)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

    def existRescuer(self, nif):
        query = "SELECT * FROM Rescue_staff WHERE nif=\'%s\'" % (nif)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        num = cursor.execute(query)
        cursor.close()
        self.dbBroker.returnConnection(con)

        if num == 0:
            return False
        else:
            return True

    def findRescuer(self, nif, password):
        query = "SELECT nif, password, name, surname1, surname2, phone, team FROM " \
            "Rescue_staff WHERE nif=\'%s\' AND password=\'%s\'" % \
            (nif, password)

        con = self.dbBroker.getConnection()
        cursor = con.cursor()
        cursor.execute(query)
        result = cursor.fetchone()
        cursor.close()
        self.dbBroker.returnConnection(con)

        if result == None:
            raise UserOrPasswordIncorrect()
        else:
            return Rescuer(result[0], result[1], result[2], result[3], result[4], result[5], \
                                   result[6])


class UserManagerI(UserManager):

    def __init__(self):
        self.proxy = None
        self.adapter = None

        self.genUser = User('', '')
        self.genRescuer = Rescuer('', '')
        self.session = Session()
        self.incidentManager = IncidentManagerI()

    def add_to(self, adapter):
        self.proxy = UserManagerPrx.uncheckedCast(adapter.addWithUUID(self))
        self.adapter = adapter

        return self.proxy

    def getProxy(self):
        return self.proxy

    def login(self, email, password, current=None):
        try:
            user = self.genUser.findUser(email, password)
            self.session.putUser(user)
            print email + " user is logged on - " + str(datetime.now())
            return True
        except UserOrPasswordIncorrect:
            if self.genUser.existUser(email):
                print "Failure to login " + email + ": Wrong password"
                return False
            else:
                print email + " user not exist"
                raise UserNotFound("The user don't exist in data base")

    def loginRescuer(self, nif, password, current=None):
        try:
            rescuer = self.genRescuer.findRescuer(nif, password)
            self.session.putRescuer(rescuer)
            print nif + " rescuer is logged on - " + str(datetime.now())
            return True
        except UserOrPasswordIncorrect:
            if self.genRescuer.existRescuer(nif):
                print "Failure to login " + nif + ": Wrong password"
                return False
            else:
                print nif + " rescuer not exist"
                raise UserNotFound("The rescuer don't exist in data base")

    def logout(self, identification, current=None):
        if self.session.checkUser(identification):
            self.session.removeUser(identification)
        elif self.session.checkRescuer(identification):
            self.session.removeRescuer(identification)

        print identification + " user is logged off - " + str(datetime.now())

    def register(self, userData, password, current=None):
        if self.genUser.existUser(userData.email):
            print "Failure to register " + userData.email + ": The user already exists"
            raise UserAlreadyExists("The user already exists")
        else:
            user = User(userData.email, password, userData.name, userData.surname1, \
                            userData.surname2, userData.phone, userData.numAA)

            self.genUser.insertUser(user)
            print "User " + userData.email + " registered - " + str(datetime.now())

    def registerRescuer(self, userData, password, current=None):
        if self.genRescuer.existRescuer(userData.nif):
            print "Failure to register " + userData.nif + ": The user already exists"
            raise UserAlreadyExists("The user already exists")
        else:
            user = Rescuer(userData.nif, password, userData.name, userData.surname1, \
                               userData.surname2, userData.phone, userData.team)

            self.genRescuer.insertRescuer(user)
            print "Rescuer " + userData.nif + " registered - " + str(datetime.now())

    def getLoggedUserData(self, email, current=None):
        if self.session.checkUser(email):
            user = self.session.getUser(email)
            return user.getData()
        else:
            raise UserNotLogged("The user isn't logged")

    def getLoggedRescuerData(self, nif, current=None):
        if self.session.checkRescuer(nif):
            rescuer = self.session.getRescuer(nif)
            return rescuer.getData()
        else:
            raise UserNotLogged("The user isn't logged")

    def getIncidentManager(self, identification, current=None):
        if self.session.checkUser(identification) or self.session.checkRescuer(identification):
            print "Sending incident manager"
            incidentManagerProxy = self.incidentManager.getProxy()

            if incidentManagerProxy == None:
                incidentManagerProxy = self.incidentManager.add_to(self.adapter)

            return incidentManagerProxy
        else:
            raise AccessDenied("The user isn't logged in to the system")
