#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import Ice

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import RCManager, Speaker, GPS


class RCManagerI(RCManager):
    def __init__(self):
        self.proxy = None
        self.speaker = SpeakerI()
        self.vibrator = None
        self.lamp = None
        self.battery = None

    def add_to(self, adapter):
        self.proxy = RCManagerPrx.uncheckedCast(adapter.addWithUUID(self))
        return self.proxy

    def getProxy(self):
        return self.proxy

    def getSpeaker(self, current=None):
        return self.speaker.getProxy()

    def getBattery(self, current=None):
        return self.battery.getProxy()

class SpeakerI(Speaker):
    def __init__(self):
        self.proxy = None
        self.on = False

    def add_to(self, adapter):
        self.proxy = SpeakerPrx.uncheckedCast(adapter.addWithUUID(self))
        return self.proxy

    def getProxy(self):
        return self.proxy

    def turnOn(self, current=None):
        if not self.on:
            self.on = True

    def turnOff(self, current=None):
        if self.on:
            self.on = False

class GPSI(GPS):
    def __init__(self):
        self.proxy = None
        self.isEnable = False
        self.location = None

        def add_to(self, adapter):
            self.proxy = GPSPrx.uncheckedCast(adapter.addWithUUID(self))
            return self.proxy

        def getProxy(self):
            return self.proxy

        def getLocation(self):
            if self.isEnable:
                return self.location
