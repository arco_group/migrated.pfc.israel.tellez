#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys, Ice

sys.path.append('./src/utils')
from timer import Timer

from rc_system import RCManagerI

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import SentryCommunicator

#class MotionSensor():
#    def __init__(self, limit=30, listener):
#        self.limit = limit
#        self.listener = listener

#    def shake(self, event):
#        if event > self._limit:
#            issue = MotionIssueC()
#            self.listener(issue)

class SentryCommunicatorI(SentryCommunicator):
    def __init__(self, manager, sentry):
        self.manager = manager
        self.proxy = None
        self.sentry = sentry

    def add_to(self, adapter):
        self.proxy = SentryCommunicatorPrx.uncheckedCast(adapter.addWithUUID(self))
        return self.proxy

    def getProxy(self):
        return self.proxy

    def notifyIssue(self, issue):
        if self.proxy is not None:
            self.manager.notifyIssue(self.sentry.email, self.proxy, issue)

    def getRCManager(self, current=None):
        self.sentry.getRCProxy()


class Sentry():
    def __init__(self, manager, email):
        self.communicator = SentryCommunicatorI(manager, self)
        #self.motionSensor = MotionSensor()
        self.rc = None
        self.state = "onRoute"
        self.issue = None
        self.email = email

    def confirmIssue(self, issue, delay=10):
        if self.state == "onRoute":
            self.state = "confirming"

            self.issue = issue
            timer = Timer(delay, self.notifyIssue, self.cancelConfirmation)
            timer.start()

            return timer

    def cancelConfirmation(self):
        if self.state == "confirming":
            self.state = "onRoute"

            self.issue = None

    def notifyIssue(self):
        if self.state == "confirming":
            self.state = "rescue"

            self.rc = RCManagerI()

            self.communicator.notifyIssue(self.issue)

    def getRCProxy(self):
        if self.state == "rescue":
            return self.rc
        else:
            raise NotExistRCModule()
