#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys, Ice

sys.path.append('./src/utils')
from datesChanger import DatesChanger
from communication_elements import IssueC

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import Issue, IssueType, UserData, UserNotFound

from detection_system import Sentry
from datetime import datetime


class LoRAM_Agent():
    def __init__(self, manager):
        self.manager = manager
        self.connected = False
        self.sentry = None
        self.onRoute = False
        self.user = None

        self.datesChanger = DatesChanger()

    def login(self, email, password):
        try:
            self.connected = self.manager.login(email, password)
        except UserNotFound:
            self.connected = False

        if self.connected:
            self.user = self.manager.getLoggedUserData(email)

    def register(self, email, password, name, surname1, surname2, phone, numAA):
        if not self.connected:
            user = UserData(email, name, surname1, surname2, phone, numAA)
            self.manager.register(user, password)

    def logout(self):
        if self.connected:
            self.manager.logout(self.user.email)

    def startRoute(self):
        if self.connected and not self.onRoute:
            incidentManager = self.manager.getIncidentManager(self.user.email)
            self.sentry = Sentry(incidentManager, self.user.email)
            self.onRoute = True

    def finishRoute(self):
        if self.onRoute:
            del(self.sentry)
            self.sentry = None
            self.onRoute = False

    def alert(self):
        if self.onRoute:
            timestamp = datetime.today()
            time = self.datesChanger.obtainTimeToTimestamp(timestamp)
            date = self.datesChanger.obtainDateToTimestamp(timestamp)
            issue = IssueC(time, date, IssueType.AlertButtonPressed)

            return self.sentry.confirmIssue(issue)
