#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from rescue_system import RescueSystem

class QuerySystem():
    def __init__(self, userManager, incidentManager, nif, team):
        self.incidentManager = incidentManager
        self.userManager = userManager
        self.nif = nif
        self.team = team

        self.incidentList = None
        self.rescueSystem = None

    def showIncidents(self):
        self.incidentList = self.incidentManager.listIncidents(self.nif)

        for incident in self.incidentList:
            print incident

    def allocate(self, idIncident):
        for incident in self.incidentList:
            if incident.idIncident == idIncident:
                self.incidentManager.allocateIncident(self.team, idIncident)
                victimData = self.userManager.getLoggedUserData(incident.victim)
                self.rescueSystem = RescueSystem(self.incidentManager, victimData)
