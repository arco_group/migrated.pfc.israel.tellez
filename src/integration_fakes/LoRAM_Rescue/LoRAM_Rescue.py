#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import Ice

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import RescuerData, UserNotFound

from query_system import QuerySystem


class LoRAM_Rescue():

    def __init__(self, manager):
        self.manager = manager
        self.connected = False
        self.user = None
        self.querySystem = None

    def loginRescuer(self, nif, password):
        try:
            self.connected = self.manager.loginRescuer(nif, password)
        except UserNotFound:
            self.connected = False

        if self.connected:
            self.user = self.manager.getLoggedRescuerData(nif)
            incidentManager = self.manager.getIncidentManager(self.user.nif)
            self.querySystem = QuerySystem(self.manager, incidentManager, self.user.nif, \
                                               self.user.team)

    def registerRescuer(self, nif, password, name, surname1, surname2, phone, team):
        if not self.connected:
            rescuer = RescuerData(nif, name, surname1, surname2, phone, team)
            self.manager.registerRescuer(rescuer, password)

    def logout(self):
        if self.connected:
            self.manager.logout(self.user.nif)

    def getQuerySystem(self):
        if self.connected:
            return self.querySystem
