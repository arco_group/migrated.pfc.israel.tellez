#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys, Ice

sys.path.append('./src/utils')
from communication_elements import SpeakerActivationRequestC, DeactivationRequestC, LocationRequestC

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import ActuatorType

class RescueSystem():
    def __init__(self, manager, victim):
        self.manager = manager
        self.victim = victim
        self.rc = manager.getRemoteControl(victim.email)

    def solve(self):
        self.manager.solveIncident(self.victim.email)

    def activateSpeaker(self):
        request = SpeakerActivationRequestC()
        self.rc.activate(request)

    def deactivateSpeaker(self):
        request = DeactivationRequestC(None, ActuatorType.TypeSpeaker)
        self.rc.deactivate(request)

    def locate(self):
        request = LocationRequestC()
        victimLocation = self.rc.getLocation(request)

        return victimLocation
