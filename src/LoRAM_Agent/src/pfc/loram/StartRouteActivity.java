package pfc.loram;

import pfc.loram.rc.actuators.GPSI;
import LoRAM.UserData;
import LoRAM.UserManagerPrx;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class StartRouteActivity extends Activity {
	
	private Button _btnStart;
	private Intent _startRoute;
	
	private AlertDialog.Builder _logoutDialog;
	
	private GPSI _gps;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_route);
		
		//Load components
		_gps = new GPSI(getApplicationContext());
		
		
		//Activity controllers
		_btnStart = (Button)findViewById(R.id.btnStart);
		_logoutDialog = new AlertDialog.Builder(this);
		
		//Controllers setting
		_btnStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Check GPS enabled
				boolean enabled = _gps.isEnable();
				
				if (!enabled) {
					startActivity(_gps.getActivator());
				}
				if (enabled) {
					_startRoute = new Intent(getApplicationContext(), OnRouteActivity.class);
					startActivity(_startRoute);
				} else {
					Toast gpsOffNotification = Toast.makeText(getApplicationContext(), "Debe activar la localización GPS", 
							Toast.LENGTH_SHORT);
					gpsOffNotification.show();
				}
				
			}
		});
		
		_logoutDialog.setTitle("Cerrar sesión y salir");
		_logoutDialog.setMessage("¿Desea cerrar la sesión actual y salir?");
		_logoutDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				LoRAM app = (LoRAM) getApplication();
				UserManagerPrx manager = app.getUserManagerProxy();
				UserData user = app.get_user();
				manager.logout(user.email);
				
				//Close activity
				finish();

				dialog.cancel();
			}
		});
		_logoutDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {			
			_logoutDialog.show();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
