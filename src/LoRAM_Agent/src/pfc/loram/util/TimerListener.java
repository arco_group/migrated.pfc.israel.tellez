package pfc.loram.util;

public interface TimerListener {

	void onTimerStart();
	void onProgressUpdate(int progress);
	void onExpiry();
	void onCancelled();
}
