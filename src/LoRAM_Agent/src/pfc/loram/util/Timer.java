package pfc.loram.util;

import java.util.TimerTask;

public class Timer {

	private java.util.Timer _timer;
	private TimerTask _timerTask;
	private long _delay;
	
	private TimerListener _listener;
	
	private boolean _started;
	private boolean _expired;
	private boolean _canceled;
	
	private int _progress;
	
	public Timer (long delay, final TimerListener listener) {
		this._timer = new java.util.Timer();
		this._delay = delay;
		this._started = false;
		this._expired = false;
		this._canceled = false;
		
		this._listener = listener;
		
		this._progress = 0;
		
		this._timerTask = new TimerTask() {
			
			@Override
			public void run() {
				if(_progress < _delay) {
					_progress++;
					_listener.onProgressUpdate(_progress);
				}else {
					expire();
				}
			}
		};
	}
	
	public void configure(long delay, TimerListener listener) {
		_delay = delay;
		_listener = listener;
	}
	
	public void start() {
		_started = true;
		_expired = false;
		_canceled = false;
		_progress = 0;
		_timer.schedule(_timerTask, 0, 1000);
		
		_listener.onTimerStart();
	}
	
	public void expire() {
		if(_started && !_canceled) {
			_timer.cancel();
			_expired = true;
			_started = false;
			
			_listener.onExpiry();
		}
	}
	
	public void cancel() {
		if(_started) {
			_timer.cancel();
			_canceled = true;
			_started = false;
			
			_listener.onCancelled();
		}
	}

	public long get_delay() {
		return _delay;
	}

	public void set_delay(long delay) {
		_delay = delay;
	}
	public TimerListener get_listener() {
		return _listener;
	}

	public void set_listener(TimerListener listener) {
		_listener = listener;
	}
	
	public boolean is_started() {
		return _started;
	}

	public boolean is_expired() {
		return _expired;
	}

	public boolean is_canceled() {
		return _canceled;
	}

	public long get_progress() {
		return _progress;
	}

	public void set_progress(int progress) {
		_progress = progress;
	}

	public void set_timer(java.util.Timer _timer) {
		this._timer = _timer;
	}
	
}
