package pfc.loram.util;

public interface LimitExceededListener {
	
	void onLimitExceeded(float value);
}
