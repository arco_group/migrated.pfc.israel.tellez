package pfc.loram;

import Ice.ConnectFailedException;
import LoRAM.UserAlreadyExists;
import LoRAM.UserData;
import LoRAM.UserManagerPrx;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	/**
	 * Keep track of the register task to ensure we can cancel it if requested.
	 */
	private UserRegisterTask mAuthTask = null;

	// Values for data at the time of the register attempt.
	private String mEmail;
	private String mPassword;
	private String mName;
	private String mSurname1;
	private String mSurname2;
	private int mPhone;
	private int mNumAA;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private EditText mRepeatPasswordView;
	private EditText mNameView;
	private EditText mSurname1View;
	private EditText mSurname2View;
	private EditText mPhoneView;
	private EditText mNumAAView;
	private View mRegisterFormView;
	private View mRegisterStatusView;
	private TextView mRegisterStatusMessageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		// Set up the register form.
		Bundle bundle = this.getIntent().getExtras();
		mEmail = bundle.getString("email");
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);
		mPasswordView = (EditText) findViewById(R.id.password);
		mRepeatPasswordView = (EditText) findViewById(R.id.repeat_password);
		mNameView = (EditText) findViewById(R.id.name);
		mSurname1View = (EditText) findViewById(R.id.surname1);
		mSurname2View = (EditText) findViewById(R.id.surname2);
		mPhoneView = (EditText) findViewById(R.id.phone);
		mNumAAView = (EditText) findViewById(R.id.numAA);
		mNumAAView
			.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,
					KeyEvent keyEvent) {
				if (id == R.id.register || id == EditorInfo.IME_NULL) {
					attemptRegister();
					return true;
				}
				return false;
			}
		});
		
		mRegisterFormView = findViewById(R.id.register_form);
		mRegisterStatusView = findViewById(R.id.register_status);
		mRegisterStatusMessageView = (TextView) findViewById(R.id.register_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptRegister();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}
	
	/**
	 * Attempts to register the account specified by the register form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual register attempt is made.
	 */
	public void attemptRegister() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);
		mRepeatPasswordView.setError(null);
		mNameView.setError(null);
		mSurname1View.setError(null);
		mSurname2View.setError(null);
		mPhoneView.setError(null);
		mNumAAView.setError(null);

		// Store values at the time of the register attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		String repeatPassword = mRepeatPasswordView.getText().toString();
		mName = mNameView.getText().toString();
		mSurname1 = mSurname1View.getText().toString();
		mSurname2 = mSurname2View.getText().toString();
		String txtPhone = mPhoneView.getText().toString();
		if (TextUtils.isEmpty(txtPhone)) {
			mPhone = 0;
		} else {
			mPhone = Integer.parseInt(txtPhone);
		}
		String txtNumAA = mNumAAView.getText().toString();
		if (TextUtils.isEmpty(txtNumAA)) {
			mNumAA = 0;
		} else {
			mNumAA = Integer.parseInt(txtNumAA);
		}

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		} else if (!mPassword.equals(repeatPassword)) {
			mRepeatPasswordView.setError(getString(R.string.error_password_not_match));
			focusView = mRepeatPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		// Check for a valid name.
		if (TextUtils.isEmpty(mName)) {
			mNameView.setError(getString(R.string.error_field_required));
			focusView = mNameView;
			cancel = true;
		}
		
		if (cancel) {
			// There was an error; don't attempt register and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user register attempt.
			mRegisterStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserRegisterTask();
			mAuthTask.execute((Void) null);
		}
	}
	
	/**
	 * Shows the progress UI and hides the register form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mRegisterStatusView.setVisibility(View.VISIBLE);
			mRegisterStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mRegisterFormView.setVisibility(View.VISIBLE);
			mRegisterFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mRegisterStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Represents an asynchronous registration task used to authenticate
	 * the user.
	 */
	public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {
		
		private UserData user = new UserData(mEmail, mName, mSurname1, mSurname2, mPhone, mNumAA);
		
		@Override
		protected Boolean doInBackground(Void... params) {
			UserManagerPrx manager = null;
			LoRAM app = (LoRAM) getApplication();
			
			try {
				// Network access.
				manager = app.getUserManagerProxy();
			} catch (ConnectFailedException e){
				Log.i(getClass().getSimpleName(), e.toString());
				return false;
			}

			try {
				manager.register(user, mPassword);
				return true;
			} catch (UserAlreadyExists e) {
				e.printStackTrace();
				
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				LoRAM app = (LoRAM) getApplication();
				app.set_user(user);
				Toast registeredNotification = Toast.makeText(getApplicationContext(), "Usuario registrado", 
						Toast.LENGTH_SHORT);
				registeredNotification.show();
				
				finish();
			} else {
				mEmailView.setError(getString(R.string.error_user_already_exists));
				mEmailView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
