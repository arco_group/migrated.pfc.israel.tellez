package pfc.loram;

import java.util.Calendar;

import pfc.loram.detection.Sentry;
import pfc.loram.detection.Sentry.SentryBinder;
import pfc.loram.util.Timer;
import pfc.loram.util.TimerListener;
import LoRAM.Date;
import LoRAM.Issue;
import LoRAM.IssueType;
import LoRAM.Time;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

public class IssueConfirmerActivity extends Activity implements TimerListener {

	private static Activity thisActivity;
	
	private static final int DEFAULT_DELAY = 10;
	
	private Intent _intentSentry;
	
	private ServiceConnection _sentryConn;
	private Sentry _sentry;
	
	private ProgressBar _pbConfirm;
	private Button _btnConfirm;
	private Button _btnCancel;
	
	private Timer _timer;
	private Issue _issue;
	
	private boolean _sentryBound;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_issue_confirmer);
		
		thisActivity = this;
		
		//Activity controllers
		_pbConfirm = (ProgressBar)findViewById(R.id.pbConfirm);
		_btnConfirm = (Button)findViewById(R.id.btnConfirm);
		_btnCancel = (Button)findViewById(R.id.btnCancel);
		
		_timer = new Timer(DEFAULT_DELAY, this);
		_sentryBound = false;
		
		_intentSentry = new Intent(getApplicationContext(), Sentry.class);
		
		_sentryConn = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				_sentryBound = false;
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				SentryBinder sentryBinder = (SentryBinder) service;
				_sentry = sentryBinder.getService();
				
				_sentryBound = true;
			}
		};
		
		//Create issue object
		Bundle bundle = this.getIntent().getExtras();
		String sIssueType = bundle.getString("IssueType");
		IssueType issueType = null;
		if (sIssueType.equals("AlertButtonPressed")) {
			issueType = IssueType.AlertButtonPressed;
		} else if (sIssueType.equals("Motion")) {
			issueType = IssueType.Motion;
		}
		Calendar calendar = Calendar.getInstance();
		Time timeIssue = new Time(calendar.get(Calendar.HOUR), 
				calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
		Date dateIssue = new Date(calendar.get(Calendar.DATE),
				calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		_issue = new Issue(timeIssue, dateIssue, issueType);
		
		//Controllers setting
		_pbConfirm.setMax(DEFAULT_DELAY);
		_btnConfirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_timer.expire();
			}
		});
		_btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_timer.cancel();
			}
		});
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		bindService(_intentSentry, _sentryConn, BIND_AUTO_CREATE);
		
		//Start timer
		_timer.start();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		_timer.cancel();
		
		if (_sentryBound) {
			unbindService(_sentryConn);
			_sentryBound = false;
		}
	}

	@Override
	public void onTimerStart() {
		_pbConfirm.setProgress(0);
	}

	@Override
	public void onProgressUpdate(int progress) {
		_pbConfirm.setProgress(progress);
	}

	@Override
	public void onExpiry() {
		
		if (_sentryBound) {
			_sentry.notifyIssue(_issue);
		}
		
		Intent assistance = new Intent(getApplicationContext(), OnAssistanceActivity.class);
		startActivity(assistance);
	}

	@Override
	public void onCancelled() {
		if(_sentryBound) {
			_sentry.cancelConfirmation();
		}
		
		//Close Activity
		finish();

	}

	public static Activity getThisActivity() {
		return thisActivity;
	}
	
}
