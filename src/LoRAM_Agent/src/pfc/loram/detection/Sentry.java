package pfc.loram.detection;

import pfc.loram.IssueConfirmerActivity;
import pfc.loram.LoRAM;
import pfc.loram.OnAssistanceActivity;
import pfc.loram.OnRouteActivity;
import pfc.loram.detection.sensors.AccelerationSensor;
import pfc.loram.rc.RCService;
import pfc.loram.rc.RCService.RCBinder;
import pfc.loram.util.LimitExceededListener;
import Ice.ObjectAdapter;
import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import LoRAM.Issue;
import LoRAM.IssueType;
import LoRAM.NotExistRCModule;
import LoRAM.RCManagerPrx;
import LoRAM.UserData;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class Sentry extends Service {

	private IBinder _binder;
	
	private SentryCommunicatorI _communicator;
	private AccelerationSensor _accelerationSensor;
	
	private String _state;

	private final static float ACCELERATION_LIMIT = 30;
	
	private Intent _intentRCService;
	private ServiceConnection _rcConn;
	private RCBinder _rcBinder;
	
	private boolean _rcBound;
	
	@Override
	public void onCreate() {
		super.onCreate();

		_binder = new SentryBinder();
		_state = "onRoute";
		
		LoRAM app = (LoRAM)getApplication();
		
		//Load Components
		IncidentManagerPrx manager = app.getIncidentManagerProxy();
		ObjectAdapter adapter = app.get_adapter();
		UserData user = app.get_user();
		
		_communicator = new SentryCommunicatorI(manager, this, user);
		_communicator.add_to(adapter);
		
		_accelerationSensor = new AccelerationSensor(getApplicationContext(), ACCELERATION_LIMIT);
		_accelerationSensor.set_listener(new LimitExceededListener() {
			
			@Override
			public void onLimitExceeded(float movement) {
				
				Log.i(getClass().getSimpleName(), "Exceeded limit movement: " +
						Float.toString(movement));
				confirmIssue(IssueType.Motion);
			}
		});
		
		_intentRCService = new Intent(getApplicationContext(), RCService.class);
		_rcConn = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				_rcBound = false;
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				_rcBinder = (RCBinder) service;
				
				_rcBound = true;
			}
		};
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return _binder;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		_accelerationSensor.onDestroy();
		
		if (_rcBound) {
			unbindService(_rcConn);
			_rcBound = false;
		}
		stopService(_intentRCService);
		
		Log.i(getClass().getSimpleName(), "Sentry destroyed");
	}

	public class SentryBinder extends Binder {
		public Sentry getService() {
			return Sentry.this;
		}
	}
	
	public void confirmIssue(IssueType issueType) {
		if (_state.equals("onRoute")) {
			_state = "confirming";
			
			//Start issue confirmer
			Intent confirmIssue = new Intent(getApplicationContext(), IssueConfirmerActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("IssueType", issueType.name());
			confirmIssue.putExtras(bundle);
			confirmIssue.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			startActivity(confirmIssue);
		}
	}
	
	public void cancelConfirmation() {
		if(_state.equals("confirming")){
			_state = "onRoute";
		}
	}
	
	public void notifyIssue(Issue issue) {
		if(_state.equals("confirming")) {
			_state = "rescue";
		
			//Start RC
			startService(_intentRCService);
			bindService(_intentRCService, _rcConn, BIND_AUTO_CREATE);
		
			try {
				_communicator.notifyIssue(issue);
			} catch (AccessDenied e) {
				e.printStackTrace();
				Toast accessDeniedNotification = Toast.makeText(getApplicationContext(), 
						"Error al notifiar un incidente: el usuario no esta logueado", 
						Toast.LENGTH_SHORT);
				accessDeniedNotification.show();
			}
		}
	}
	
	public RCManagerPrx getRCProxy() throws NotExistRCModule {
		if(_state.equals("rescue") && _rcBound) {
			return _rcBinder.getRCProxy();
		} else {
			throw new NotExistRCModule();
		}
	}
	
	public void finishRoute() {
		_accelerationSensor.onDestroy();
		
		if (_rcBound) {
			_rcBinder.deactivateGPS();
			unbindService(_rcConn);
			_rcBound = false;
		}
		stopService(_intentRCService);
		
		//Stop service
		stopSelf();
		
	}
	
	public void solveIncident() {
		Log.i(getClass().getSimpleName(), "Incident resolved");
		
		//Close activities
		IssueConfirmerActivity.getThisActivity().finish();
		OnAssistanceActivity.getThisActivity().finish();
		OnRouteActivity.getThisActivity().finish();
		
		finishRoute();
		
	}

	public String get_state() {
		return _state;
	}

	public void set_state(String _state) {
		this._state = _state;
	}

	public SentryCommunicatorI get_communicator() {
		return _communicator;
	}

	public void set_communicator(SentryCommunicatorI _communicator) {
		this._communicator = _communicator;
	}

	public boolean is_rcBound() {
		return _rcBound;
	}

	public void set_rcBound(boolean _rcBound) {
		this._rcBound = _rcBound;
	}

	public AccelerationSensor get_accelerationSensor() {
		return _accelerationSensor;
	}

	public void set_accelerationSensor(AccelerationSensor accelerationSensor) {
		this._accelerationSensor = accelerationSensor;
	}
}
