package pfc.loram.detection;

import Ice.Current;
import Ice.ObjectAdapter;
import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import LoRAM.Issue;
import LoRAM.NotExistRCModule;
import LoRAM.RCManagerPrx;
import LoRAM.SentryCommunicatorPrx;
import LoRAM.SentryCommunicatorPrxHelper;
import LoRAM.UserData;
import LoRAM._SentryCommunicatorDisp;

public class SentryCommunicatorI extends _SentryCommunicatorDisp{

	private SentryCommunicatorPrx _proxy;
	private IncidentManagerPrx _manager;
	private Sentry _sentry;
	private UserData _user;
	
	public SentryCommunicatorI(IncidentManagerPrx manager, Sentry sentry, UserData user) {
		_proxy = null;
		_manager = manager;
		_sentry = sentry;
		_user = user;
	}
	
	public SentryCommunicatorPrx add_to(ObjectAdapter adapter) {
		_proxy = SentryCommunicatorPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		
		return _proxy;
	}
	
	public void notifyIssue(Issue issue) throws AccessDenied {
		if (_proxy != null) {
			_manager.notifyIssue(_user.email, _proxy, issue);
		}
	}
	
	@Override
	public RCManagerPrx getRCManager(Current __current) throws NotExistRCModule {
		RCManagerPrx rcManager = _sentry.getRCProxy();
		
		return rcManager;
	}

	@Override
	public void solveIncident(Current __current) {
		_sentry.solveIncident();
	}

	public SentryCommunicatorPrx get_proxy() {
		return _proxy;
	}

	public void set_proxy(SentryCommunicatorPrx _proxy) {
		this._proxy = _proxy;
	}

}
