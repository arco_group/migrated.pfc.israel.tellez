package pfc.loram.detection.sensors;


import pfc.loram.util.LimitExceededListener;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class AccelerationSensor implements SensorEventListener{

	private SensorManager _sm;
	private Sensor _accelerometer;
	
	private LimitExceededListener _listener;
	
	private float _accelerationLimit;

	public AccelerationSensor(Context context, float accelerationLimit) {
		
		_sm = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		_accelerometer = _sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		_sm.registerListener(this, _accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		
		_listener = null;
		_accelerationLimit = accelerationLimit;
		
	}
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Nothing to do
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		float x = Math.abs(event.values[0]);
		float y = Math.abs(event.values[1]);
		float z = Math.abs(event.values[2]);
		
		float acceleration = 0;
		
		if (x > y) {
			if (x > z) {
				acceleration = x;
			} else {
				acceleration = z;
			}
		} else {
			if (y > z) {
				acceleration = y;
			} else {
				acceleration = z;
			}
		}
		
		if (acceleration > _accelerationLimit) {
			if(_listener != null) {
				_listener.onLimitExceeded(acceleration);
			}
		}
	}
	
	public void onDestroy() {
		_sm.unregisterListener(this, _accelerometer);
	}
	
	public float get_limitMovement() {
		return _accelerationLimit;
	}

	public void set_limitMovement(float _limitMovement) {
		this._accelerationLimit = _limitMovement;
	}

	public void set_listener(LimitExceededListener _listener) {
		this._listener = _listener;
	}
	
}
