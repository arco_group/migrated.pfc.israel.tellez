package pfc.loram;

import LoRAM.AccessDenied;
import LoRAM.IncidentManagerPrx;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class OnAssistanceActivity extends Activity {

	private static Activity thisActivity;
	
	private Button _btnFinishAssistance;
	
	private AlertDialog.Builder _finishAssistanceDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_on_assistance);
		
		thisActivity = this;
		
		//Activity controllers
		_btnFinishAssistance = (Button)findViewById(R.id.btnFinishAssistance);
		_finishAssistanceDialog = new AlertDialog.Builder(this);
		
		//Controllers setting
		_finishAssistanceDialog.setTitle("Terminar asistencia");
		_finishAssistanceDialog.setMessage("¿Desea dar por terminada la asistencia?");
		_finishAssistanceDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				LoRAM app = (LoRAM) getApplication();
				IncidentManagerPrx manager = app.getIncidentManagerProxy();
				try {
					manager.solveIncident(app.get_user().email);
				} catch (AccessDenied e) {
					e.printStackTrace();
					Toast accessDeniedNotification = Toast.makeText(getApplicationContext(), "El usuario no esta logueado", 
							Toast.LENGTH_SHORT);
					accessDeniedNotification.show();
				}

				dialog.cancel();
			}
		});
		_finishAssistanceDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		_btnFinishAssistance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_finishAssistanceDialog.show();
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {			
			_finishAssistanceDialog.show();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	public static Activity getThisActivity() {
		return thisActivity;
	}

}
