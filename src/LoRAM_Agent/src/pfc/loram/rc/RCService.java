package pfc.loram.rc;

import pfc.loram.LoRAM;
import pfc.loram.R;
import pfc.loram.rc.actuators.BatteryI;
import pfc.loram.rc.actuators.GPSI;
import pfc.loram.rc.actuators.LampI;
import pfc.loram.rc.actuators.SpeakerI;
import pfc.loram.rc.actuators.VibratorI;
import Ice.ObjectAdapter;
import LoRAM.RCManagerPrx;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;

public class RCService extends Service {

	private IBinder _binder;
	
	private RCManagerI _rcManager;
	
	private SpeakerI _speaker;
	private LampI _lamp;
	private VibratorI _vibrator;
	private GPSI _gps;
	private BatteryI _battery;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		_binder = new RCBinder();
		
		LoRAM app = (LoRAM)getApplication();
		
		//Load Components
		ObjectAdapter adapter = app.get_adapter();
		
		_rcManager = new RCManagerI();
		
		_speaker = new SpeakerI(getApplicationContext(), R.raw.alarm);
		_lamp = new LampI();
		_vibrator = new VibratorI(getApplicationContext());
		_gps = new GPSI(getApplicationContext());
		_battery = new BatteryI(getApplicationContext());
		
		_rcManager.add_to(adapter);
		_vibrator.add_to(adapter);
		_speaker.add_to(adapter);
		_lamp.add_to(adapter);
		_gps.add_to(adapter);
		_battery.add_to(adapter);
		
		//Check flash light
		boolean lampAvailable = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
		
		//Add components to _rcManager
		_rcManager.setSpeaker(_speaker);
		_rcManager.setVibrator(_vibrator);
		if (lampAvailable) {
			_rcManager.setLamp(_lamp);
		}
		_rcManager.setGPS(_gps);
		_rcManager.setBattery(_battery);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		return START_STICKY;
	} 
	
	@Override
	public IBinder onBind(Intent intent) {
		return _binder;
	}

	public class RCBinder extends Binder {
		public RCManagerPrx getRCProxy() {
			RCManagerPrx rcProxy = _rcManager.getProxy();
			
			return rcProxy;
		}
		
		public void deactivateGPS() {
			_gps.deactivate();
		}
	}
	
}
