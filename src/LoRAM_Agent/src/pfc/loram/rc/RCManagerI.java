package pfc.loram.rc;

import pfc.loram.rc.actuators.BatteryI;
import pfc.loram.rc.actuators.GPSI;
import pfc.loram.rc.actuators.LampI;
import pfc.loram.rc.actuators.SpeakerI;
import pfc.loram.rc.actuators.VibratorI;
import Ice.Current;
import Ice.ObjectAdapter;
import LoRAM.BatteryPrx;
import LoRAM.GPSPrx;
import LoRAM.LampPrx;
import LoRAM.NotAvailableActuator;
import LoRAM.RCManagerPrx;
import LoRAM.RCManagerPrxHelper;
import LoRAM.SpeakerPrx;
import LoRAM.VibratorPrx;
import LoRAM._RCManagerDisp;

public class RCManagerI extends _RCManagerDisp{

	private RCManagerPrx _proxy;
	private SpeakerI _speaker;
	private LampI _lamp;
	private VibratorI _vibrator;
	private GPSI _gps;
	private BatteryI _battery;
	
	public RCManagerI() {
		_proxy = null;
		
		_speaker = null;
		_lamp = null;
		_vibrator = null;
		_gps = null;
		_battery = null;
	}
	
	public RCManagerPrx add_to(ObjectAdapter adapter) {
		_proxy = RCManagerPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		
		return _proxy;
	}
	
	public RCManagerPrx getProxy() {
		return _proxy;
	}
	
	public void setSpeaker(SpeakerI speaker) {
		_speaker = speaker;
	}
	
	@Override
	public SpeakerPrx getSpeaker(Current __current) throws NotAvailableActuator {
		if(_speaker != null) {
			return _speaker.getProxy();
		} else {
			throw new NotAvailableActuator("The device haven't speaker");
		}
	}

	public void setLamp(LampI lamp) {
		_lamp = lamp;
	}
	
	@Override
	public LampPrx getLamp(Current __current) throws NotAvailableActuator {
		if(_lamp != null) {
			return _lamp.getProxy();
		} else {
			throw new NotAvailableActuator("The device haven't lamp");
		}
	}

	public void setVibrator(VibratorI vibrator) {
		_vibrator = vibrator;
	}
	
	@Override
	public VibratorPrx getVibrator(Current __current)
			throws NotAvailableActuator {
		if (_vibrator != null) {
			return _vibrator.getProxy();
		} else {
			throw new NotAvailableActuator("The device haven't vibrator");
		}
	}

	public void setGPS(GPSI gps) {
		_gps = gps;
	}
	
	@Override
	public GPSPrx getGPS(Current __current) throws NotAvailableActuator {
		if (_gps != null) {
			return _gps.getProxy();
		} else {
			throw new NotAvailableActuator("The device haven't GPS");
		}
	}

	public void setBattery(BatteryI battery) {
		_battery = battery;
	}
	
	@Override
	public BatteryPrx getBattery(Current __current) {
		if (_gps != null) {
			return _battery.getProxy();
		} else {
			return null;
		}
	}

}
