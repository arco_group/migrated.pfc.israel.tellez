package pfc.loram.rc.actuators;

import android.content.Context;
import android.os.Vibrator;
import Ice.Current;
import LoRAM.Level;
import LoRAM.VibratorPrx;
import LoRAM.VibratorPrxHelper;
import LoRAM._VibratorDisp;

public class VibratorI extends _VibratorDisp {

	private VibratorPrx _proxy;
	private boolean _on;
	private long[] _pattern;
	
	private Context _context;
	private Vibrator _vibrator;
	
	private final static long[] HIGH_FREQUENCY_PATTERN = {0, 500, 250};
	private final static long[] MEDIUM_FREQUENCY_PATTERN = {0, 1000, 500};
	private final static long[] LOW_FREQUENCY_PATTERN = {0, 1250, 750};
	
	public VibratorI(Context context) {
		_proxy = null;
		_on = false;
		
		_context = context;
		_vibrator = (Vibrator)_context.getSystemService(Context.VIBRATOR_SERVICE);
		_pattern = MEDIUM_FREQUENCY_PATTERN;
	}
	
	public VibratorPrx add_to(Ice.ObjectAdapter adapter) {
		_proxy = VibratorPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		return _proxy;
	}
	
	public VibratorPrx getProxy() {
		return _proxy;
	}
	
	@Override
	public void turnOn(Current __current) {
		if (!is_on()) {
			_on = true;
			_vibrator.vibrate(_pattern, 0);
		}
	}

	@Override
	public void turnOff(Current __current) {
		if (is_on()) {
			_on = false;
			_vibrator.cancel();
		}
	}

	@Override
	public void configure(Level frequency, Current __current) {
		switch (frequency) {
			case High:
				_pattern = HIGH_FREQUENCY_PATTERN;
				break;
				
			case Medium:
				_pattern = MEDIUM_FREQUENCY_PATTERN;
				break;
				
			case Low:
				_pattern = LOW_FREQUENCY_PATTERN;
				break;
		}
	}

	public boolean is_on() {
		return _on;
	}

}
