package pfc.loram.rc.actuators;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import Ice.Current;
import LoRAM.SpeakerPrx;
import LoRAM.SpeakerPrxHelper;
import LoRAM._SpeakerDisp;

public class SpeakerI extends _SpeakerDisp{

	private SpeakerPrx _proxy;
	private boolean _on;
	private float _volume;
	private float _rate;
	
	private Context _context;
	private SoundPool _soundPool;
	private int _soundID;
	private int _streamID;
	
	public SpeakerI(Context context, int resId) {
		_proxy = null;
		_on = false;
		_volume = 1.0f;
		_rate = 1.0f;
		
		_context = context;
		_soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 100);
		_soundID = _soundPool.load(_context, resId, 1);
		_streamID = 0;
		
	}
	
	public SpeakerI(Context context, int resId, float volume, float rate) {
		_proxy = null;
		_on = false;
		_volume = volume;
		_rate = rate;
		
		_context = context;
		_soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 100);
		_soundID = _soundPool.load(_context, resId, 1);
		_streamID = 0;
	}

	public SpeakerPrx add_to(Ice.ObjectAdapter adapter) {
		_proxy = SpeakerPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		return _proxy;
	}
	
	public SpeakerPrx getProxy() {
		return _proxy;
	}
	
	@Override
	public void turnOn(Current __current) {
		if (!is_on()) {
			_on = true;
			_streamID = _soundPool.play(_soundID, _volume, _volume, 1, -1, _rate);
		}
	}

	@Override
	public void turnOff(Current __current) {
		if (is_on()) {
			_on = false;
			_soundPool.stop(_streamID);
		}
	}

	@Override
	public void configure(float volume, float rate, Current __current) {
		_volume = volume;
		_rate = rate;
	}

	public void changeSound(int resId) {
		_soundPool.release();
		_soundID = _soundPool.load(_context, resId, 1);
	}

	public boolean is_on() {
		return _on;
	}

	public float get_volume() {
		return _volume;
	}

	public float get_rate() {
		return _rate;
	}
	
	public int get_soundID() {
		return _soundID;
	}
	
	public int get_streamID() {
		return _streamID;
	}
}
