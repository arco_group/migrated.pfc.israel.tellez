package pfc.loram.rc.actuators;

import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import Ice.Current;
import LoRAM.ActuatorOff;
import LoRAM.GPSPrx;
import LoRAM.GPSPrxHelper;
import LoRAM.Location;
import LoRAM._GPSDisp;

public class GPSI extends _GPSDisp implements LocationListener{

	private GPSPrx _proxy;
	private boolean _enable;
	private Location _location;
	
	private Context _context;
	private LocationManager _locationManager;
	
	private static final long MIN_TIME_BETWEEN_UPDATES = 30000; // in milliseconds
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // in meters
	
	public GPSI(Context context) {
		_proxy = null;
		_enable = false;
		_location = new Location();
		_context = context;
		
		_locationManager = (LocationManager)_context.getSystemService(Context.LOCATION_SERVICE);
		
		_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BETWEEN_UPDATES, 
				MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
		
		//Put default location
		android.location.Location loc = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (loc != null) {
			onLocationChanged(loc);
		}
	}
	
	public GPSPrx add_to(Ice.ObjectAdapter adapter) {
		_proxy = GPSPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		return _proxy;
	}
	
	public GPSPrx getProxy() {
		return _proxy;
	}

	@Override
	public boolean isEnable(Current __current) {
		_enable = _locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		
		return _enable;
	}
	
	@Override
	public Location getLocation(Current __current) throws ActuatorOff {
		if (isEnable()) {
			return _location;
		} else {
			throw new ActuatorOff("The GPS is off");
		}
	}

	@Override
	public void configure(long timeBetweenUpdates, long distanceChangeForUpdates, Current __current) {
		_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, timeBetweenUpdates, 
				distanceChangeForUpdates, this);
	}

	@Override
	public void onLocationChanged(android.location.Location location) {
		_location.latitude = location.getLatitude();
		_location.longitude = location.getLongitude();
		
		Log.i(getClass().getSimpleName(), "Lat: "+ _location.latitude + " Lon: "+ _location.longitude);
	}

	@Override
	public void onProviderDisabled(String provider) {
		_enable = false;
		
		Log.i(getClass().getSimpleName(), "Provider OFF: "+ provider);
	}

	@Override
	public void onProviderEnabled(String provider) {
		_enable = true;
		
		Log.i(getClass().getSimpleName(), "Provider ON: "+ provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.i(getClass().getSimpleName(), "Provider Status: "+ status);
	}
	
	public Intent getActivator() {
		Intent activator = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		
		return activator;
	}
	
	public void deactivate() {
		_locationManager.removeUpdates(this);
	}
}
