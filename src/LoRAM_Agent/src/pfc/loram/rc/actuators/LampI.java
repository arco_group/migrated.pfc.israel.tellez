package pfc.loram.rc.actuators;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import Ice.Current;
import LoRAM.LampPrx;
import LoRAM.LampPrxHelper;
import LoRAM.Level;
import LoRAM._LampDisp;

public class LampI extends _LampDisp{

	private LampPrx _proxy;
	private boolean _on;
	private String _brightness;
	private boolean _brightnessChangeAvailable;
	private File _brightnessFile;
	
	private static final String HIGH_BRIGHTNESS = "128\n";
	private static final String MEDIUM_BRIGHTNESS = "64\n";
	private static final String LOW_BRIGHTNESS = "32\n";
	
	private Camera _cam;
	
	public LampI() {
		_proxy = null;
		_on = false;
		
		_brightnessFile = new File("/sys/devices/platform/flashlight.0/leds/flashlight/brightness");
		_brightnessChangeAvailable = _brightnessFile.exists() && _brightnessFile.canWrite();
		_brightness = HIGH_BRIGHTNESS;
	}
	
	public LampPrx add_to(Ice.ObjectAdapter adapter) {
		_proxy = LampPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		return _proxy;
	}
	
	public LampPrx getProxy() {
		return _proxy;
	}
	
	@Override
	public void turnOn(Current __current) {
		if (!is_on()) {
			_on = true;
		
			if (_brightnessChangeAvailable) {
				BufferedWriter buffer;
				try {
					buffer = new BufferedWriter(new FileWriter(_brightnessFile), 8192);
					buffer.write(_brightness);
					buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				_cam = Camera.open();
				Parameters parameters = _cam.getParameters();
				parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
				_cam.setParameters(parameters);
				_cam.startPreview();
			}
		}
	}

	@Override
	public void turnOff(Current __current) {
		if (is_on()) {
			_on = false;
		
			if(_brightnessChangeAvailable) {
				BufferedWriter buffer;
				try {
					buffer = new BufferedWriter(new FileWriter(_brightnessFile), 8192);
					buffer.write("0\n");
					buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				_cam.stopPreview();
				_cam.release();
			}
		}
	}

	@Override
	public void configure(Level brightness, Current __current) {
		switch (brightness) {
			case High:
				_brightness = HIGH_BRIGHTNESS;
				break;
				
			case Medium:
				_brightness = MEDIUM_BRIGHTNESS;
				break;
				
			case Low:
				_brightness = LOW_BRIGHTNESS;
				break;
		}
	}

	public boolean is_on() {
		return _on;
	}

}
