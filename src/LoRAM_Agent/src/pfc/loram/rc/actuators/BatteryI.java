package pfc.loram.rc.actuators;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import Ice.Current;
import LoRAM.BatteryPrx;
import LoRAM.BatteryPrxHelper;
import LoRAM._BatteryDisp;

public class BatteryI extends _BatteryDisp{

	private BatteryPrx _proxy;
	private Context _context;
	
	public BatteryI(Context context) {
		_proxy = null;
		_context = context;
	}
	
	public BatteryPrx add_to(Ice.ObjectAdapter adapter) {
		_proxy = BatteryPrxHelper.uncheckedCast(adapter.addWithUUID(this));
		
		return _proxy;
	}
	
	public BatteryPrx getProxy() {
		return _proxy;
	}
	
	@Override
	public float getLevel(Current __current) {
		
		IntentFilter batteryIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = _context.registerReceiver(null, batteryIntentFilter);
		
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
		
		float batteryPct = -1;
		
		if (level != -1 && scale != -1) {
			batteryPct = (level / (float)scale) * 100;
		}
		
		return batteryPct;
	}

}
