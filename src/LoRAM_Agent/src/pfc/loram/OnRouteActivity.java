package pfc.loram;

import pfc.loram.detection.Sentry;
import pfc.loram.detection.Sentry.SentryBinder;
import LoRAM.IssueType;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class OnRouteActivity extends Activity {

	private static Activity thisActivity;
	
	private Intent _intentSentry;
	
	private ServiceConnection _sentryConn;
	private Sentry _sentry;
	
	private Button _btnAlert;
	private Button _btnFinish;
	private AlertDialog.Builder _finishRouteDialog;
	
	private boolean _sentryBound;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_on_route);
		
		thisActivity = this;
		
		_intentSentry = new Intent(getApplicationContext(), Sentry.class);
		
		_sentryConn = new ServiceConnection() {
			
			@Override
			public void onServiceDisconnected(ComponentName name) {
				_sentryBound = false;
			}
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				SentryBinder sentryBinder = (SentryBinder) service;
				_sentry = sentryBinder.getService();
				
				_sentryBound = true;
			}
		};
		
		//Activity controllers
		_btnAlert = (Button)findViewById(R.id.btnAlert);
		_btnFinish = (Button)findViewById(R.id.btnFinish);
		_finishRouteDialog = new AlertDialog.Builder(this);
		
		//Controllers setting
		_btnAlert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				_sentry.confirmIssue(IssueType.AlertButtonPressed);
				
			}
		});
		
		_btnFinish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_sentry.finishRoute();
				
				//Close Activity
				finish();
			}
		});
		_finishRouteDialog.setTitle("Terminar ruta");
		_finishRouteDialog.setMessage("¿Desea dar por terminada la ruta?");
		_finishRouteDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				_sentry.finishRoute();
				
				//Close Activity
				finish();

				dialog.cancel();
			}
		});
		_finishRouteDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		//Start sentry
		startService(_intentSentry);
		
		bindService(_intentSentry, _sentryConn, BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		if (_sentryBound) {
			unbindService(_sentryConn);
			_sentryBound = false;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {			
			_finishRouteDialog.show();
			
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	public static Activity getThisActivity() {
		return thisActivity;
	}
	
	
}
