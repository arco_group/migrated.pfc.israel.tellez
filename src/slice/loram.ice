// -*- mode: c++ -*-

module LoRAM {

  exception GenericError {
    string reason;
  };

  exception ActuatorOff extends GenericError {};
  exception NotAvailableActuator extends GenericError {};
  exception NotExistRCModule extends GenericError {};
  exception UserNotFound extends GenericError {};
  exception UserNotLogged extends GenericError {};
  exception AccessDenied extends GenericError {};
  exception UserAlreadyExists extends GenericError {};
  exception ImposibleConnectWithUser extends GenericError {};
  exception NotSuitable extends GenericError {};

  enum IssueType { DetectedFall, DetectedCollision, Motion, AlertButtonPressed, UnknownIssue };
  enum ActuatorType { TypeSpeaker, TypeLamp, TypeVibrator, TypeLocation };
  enum StateType { NoAllocate, Allocated, Resolved, UnknownState };
  enum LocationType { GPSType, NETWORKType };
  enum Level { High, Medium, Low };

  class Date {
    int day;
    int month;
    int year;
  };

  class Time {
    int hour;
    int minute;
    int second;
  };

  class Location {
    double latitude;
    double longitude;
  };

  class UserData {
    string email;
    string name;
    string surname1;
    string surname2;
    int phone;
    int numAA;
  };

  class RescuerData {
    string nif;
    string name;
    string surname1;
    string surname2;
    int phone;
    int team;
  };

  class IncidentData {
    int idIncident;
    string victim;
    Date dateIncident;
    Time timeIncident;
    StateType state;
    IssueType typeIncident;
    int rescueTeam;
  };

  sequence<IncidentData> IncidentsList;

  class Issue {
    Time timeIss;
    Date dateIss;
    IssueType typeIss;
  };

  class MotionIssue extends Issue {
    float shake;
  };

  class ActivationRequest {
    Time timeReq;
    Time duration;
    ActuatorType typeReq;
  };

  class DeactivationRequest {
    Time timeReq;
    ActuatorType typeReq;
  };

  class SpeakerActivationRequest extends ActivationRequest {
    float volume;
    float rate;
  };

  class LampActivationRequest extends ActivationRequest {
    Level brightness;
  };

  class VibratorActivationRequest extends ActivationRequest {
    Level frequency;
  };

  class LocationRequest {
    LocationType locType;
    long timeBetweenUpdates;
    long distanceChangeForUpdates;
  };

  interface Speaker {
    void turnOn();
    void turnOff();
    void configure(float volume, float rate);
  };

  interface Lamp {
    void turnOn();
    void turnOff();
    void configure(Level brightness);
  };

  interface Vibrator {
    void turnOn();
    void turnOff();
    void configure(Level frequency);
  };

  interface GPS {
    bool isEnable();
    Location getLocation() throws ActuatorOff;
    void configure(long timeBetweenUpdates, long distanceChangeForUpdates);
  };

  interface Battery {
    float getLevel();
  };

  interface RCManager {
    Speaker* getSpeaker() throws NotAvailableActuator;
    Lamp* getLamp() throws NotAvailableActuator;
    Vibrator* getVibrator() throws NotAvailableActuator;
    GPS* getGPS() throws NotAvailableActuator;
    Battery* getBattery();
  };

  interface SentryCommunicator {
    RCManager* getRCManager() throws NotExistRCModule;
    void solveIncident();
  };

  interface RemoteControl {
    void activate(ActivationRequest request) throws NotAvailableActuator, NotSuitable;
    void deactivate(DeactivationRequest request);
    Location getLocation(LocationRequest request) throws NotAvailableActuator,
      ActuatorOff, NotSuitable;
  };

  interface IncidentManager {
    void notifyIssue(string email, SentryCommunicator* sentry, Issue iss) throws AccessDenied;
    IncidentsList listIncidents(string nif) throws AccessDenied;
    RemoteControl* getRemoteControl(string email) throws AccessDenied, ImposibleConnectWithUser;
    void allocateIncident(int team, int idIncident);
    void solveIncident(string email) throws AccessDenied;
  };

  interface UserManager {
    bool login(string email, string password) throws UserNotFound;
    bool loginRescuer(string nif, string password) throws UserNotFound;
    void logout(string identification);
    void register(UserData user, string password) throws UserAlreadyExists;
    void registerRescuer(RescuerData user, string password) throws UserAlreadyExists;
    UserData getLoggedUserData(string email) throws UserNotLogged;
    RescuerData getLoggedRescuerData(string nif) throws UserNotLogged;
    IncidentManager* getIncidentManager(string identification) throws AccessDenied;
  };

};
