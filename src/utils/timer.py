#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import threading

class Timer():
    def __init__(self, delay=0, onExpiry=None, onCancelled=None):
        self.timer = threading.Timer(delay, self.expire)
        self.onExpiry = onExpiry
        self.onCancelled = onCancelled
        self.started = False
        self.expired = False
        self.canceled = False

    def configure(self, delay, onExpiry, onCancelled):
        self.timer = threading.Timer(delay, self.expire)
        self.onExpiry = onExpiry
        self.onCancelled = onCancelled

    def start(self):
        self.started = True
        self.expired = False
        self.canceled = False
        self.timer.start()

    def expire(self):
        if (self.started and not self.canceled):
            self.timer.cancel()
            self.expired = True
            self.started = False

            self.onExpiry()

    def cancel(self):
        if (self.started):
            self.timer.cancel()
            self.canceled = True
            self.started = False

            self.onCancelled()
