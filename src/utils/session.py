#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-


class Session():
    users = {}
    rescueStaff = {}

    @staticmethod
    def putUser(user):
        sessionData = {'user': user}
        Session.users[user.email] = sessionData

    @staticmethod
    def putRescuer(rescuer):
        sessionData = {'rescuer': rescuer}
        Session.rescueStaff[rescuer.nif] = sessionData

    @staticmethod
    def removeUser(email):
        del Session.users[email]

    @staticmethod
    def removeRescuer(rescuer):
        del Session.rescueStaff[rescuer]

    @staticmethod
    def checkUser(email):
        return email in Session.users

    @staticmethod
    def checkRescuer(nif):
        return nif in Session.rescueStaff

    @staticmethod
    def checkRC(email):
        sessionData = Session.users[email]
        return 'rc' in sessionData

    @staticmethod
    def addSentry(email, sentry):
        sessionData = Session.users[email]
        sessionData['sentry'] = sentry

    @staticmethod
    def addIncident(email, idIncident):
        sessionData = Session.users[email]
        sessionData['incident'] = idIncident

    @staticmethod
    def addRC(email, rc):
        sessionData = Session.users[email]
        sessionData['rc'] = rc

    @staticmethod
    def getUser(email):
        sessionData = Session.users[email]
        return sessionData['user']

    @staticmethod
    def getRescuer(nif):
        sessionData = Session.rescueStaff[nif]
        return sessionData['rescuer']

    @staticmethod
    def getSentry(email):
        sessionData = Session.users[email]
        return sessionData['sentry']

    @staticmethod
    def getIncident(email):
        sessionData = Session.users[email]
        return sessionData['incident']

    @staticmethod
    def getRC(email):
        sessionData = Session.users[email]
        return sessionData['rc']

    @staticmethod
    def deleteIncident(email):
        sessionData = Session.users[email]
        del sessionData['incident']
