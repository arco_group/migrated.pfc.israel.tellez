#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import Ice

Ice.loadSlice('./src/slice/loram.ice')
from LoRAM import IssueType, ActuatorType, StateType, \
    Date, Time, Location, UserData, RescuerData, IncidentData, Issue, MotionIssue, \
    DeactivationRequest, SpeakerActivationRequest, LampActivationRequest, \
    VibratorActivationRequest, LocationRequest, LocationType

class ObjectFactory(Ice.ObjectFactory):
    def create(self, type):

        if type == Time.ice_staticId():
            return TimeC()
        elif type == UserData.ice_staticId():
            return UserDataC()
        elif type == FallIssue.ice_staticId():
            return FallIssueC()
        elif type == SpeakerActivationRequest.ice_staticId():
            return SpeakerActivationRequestC()
        elif type == LocationRequest.ice_staticId():
            return LocationRequestC()

        assert(False)
        return None

    def destroy(self):
        # Nothing to do
        pass

class TimeC(Time):
    def __init__(self, hour=0, minute=0, second=0):
        Time.__init__(self, hour, minute, second)

    def __eq__(self, other):
        equal = self.hour == other.hour and self.minute == other.minute and \
            self.second == other.second
        return equal

class DateC(Date):
    def __init__(self, day=0, month=0, year=0):
        Date.__init__(self, day, month, year)

    def __eq__(self, other):
        equal = self.day == other.day and self.month == other.month and \
            self.year == other.year
        return equal

class LocationC(Location):
    def __init__(self, latitude=0, longitude=0):
        Location.__init__(self, latitude, longitude)

    def __eq__(self, other):
        equal = round(self.latitude) == round(other.latitude) and \
            round(self.longitude) == round(other.longitude)
        return equal

class UserDataC(UserData):
    def __init__(self, email='', name='', surname1='', surname2='', phone=0, numAA=0):
        UserData.__init__(self, email, name, surname1, surname2, phone, numAA)

    def __eq__(self, other):
        equal = self.email == other.email and self.name == other.name and \
            self.surname1 == other.surname1 and self.surname2 == other.surname2 and \
            self.phone == other.phone and self.numAA == other.numAA

        return equal

    def configure(self, email, name, surname1, surname2, phone, numAA):
        self.email = email
        self.name = name
        self.surname1 = surname1
        self.surname2 = surname2
        self.phone = phone
        self.numAA = numAA

class RescuerDataC(RescuerData):
    def __init__(self, nif='', name='', surname1='', surname2='', phone=0, team=0):
        RescuerData.__init__(self, nif, name, surname1, surname2, phone, team)

    def __eq__(self, other):
        equal = self.nif == other.nif and self.name == other.name and \
            self.surname1 == other.surname1 and self.surname2 == other.surname2 and \
            self.phone == other.phone and self.team == other.team

        return equal

class IncidentDataC(IncidentData):
    def __init__(self, idIncident=0, victim='', dateIncident=DateC(), timeIncident=TimeC(), \
                     state=StateType.NoAllocate, typeIncident=IssueType.DetectedFall, \
                     rescueTeam=0):
        IncidentData.__init__(self, idIncident, victim, dateIncident, timeIncident, state, \
                                  typeIncident, rescueTeam)

    def __eq__(self, other):
        equal = self.idIncident == other.idIncident and self.victim == other.victim and \
            self.dateIncident == other.dateIncident and self.timeIncident == other.timeIncident and \
            self.state == other.state and self.typeIncident == other.typeIncident and \
            self.rescueTeam == other.rescueTeam

        return equal

class MotionIssueC(MotionIssue):
    def __init__(self, timeIss=TimeC(), dateIss=DateC(), \
                     typeIss=IssueType.Motion, shake=''):
        MotionIssue.__init__(self, timeIss, dateIss, typeIss, shake)

    def __eq__(self, other):
        equal = self.timeIss == other.timeIss and self.dateIss == other.dateIss and \
            self.typeIss == other.typeIss and self.shake == other.shake
        return equal

class IssueC(Issue):
    def __init__(self, timeIss=TimeC(), dateIss=DateC(), \
                     typeIss=IssueType.AlertButtonPressed):
        Issue.__init__(self, timeIss, dateIss, typeIss)

    def __eq__(self, other):
        equal = self.timeIss == other.timeIss and self.dateIss == other.dateIss  and \
            self.typeIss == other.typeIss

        return equal

class DeactivationRequestC(DeactivationRequest):
    def __init__(self, timeReq=TimeC(), typeReq=None):
        DeactivationRequest.__init__(self, timeReq, typeReq)

    def __eq__(self, other):
        equal = self.timeReq == other.timeReq and self.typeReq == other.typeReq
        return equal

class SpeakerActivationRequestC(SpeakerActivationRequest):
    def __init__(self, timeReq=TimeC(), duration=TimeC(), typeReq=ActuatorType.TypeSpeaker, \
                     volume=1, rate=1):
        SpeakerActivationRequest.__init__(self, timeReq, duration, typeReq, volume, rate)

    def __eq__(self, other):
        equal = self.timeReq == other.timeReq and self.duration == other.duration and \
            self.typeReq == other.typeReq and self.volume == other.volume and \
            self.rate == other.rate
        return equal

class LampActivationRequestC(LampActivationRequest):
    def __init__(self, timeReq=TimeC(), duration=TimeC(), typeReq=ActuatorType.TypeLamp, \
                     frequency=0, brightness=0):
        LampActivationRequest.__init__(self, timeReq, duration, typeReq, frequency, brightness)

    def __eq__(self, other):
        equal = self.timeReq == other.timeReq and self.duration == other.duration and \
            self.typeReq == other.typeReq and self.frequency == other.frequency and \
            self.brightness == other.brightness
        return equal

class VibratorActivationRequestC(VibratorActivationRequest):
    def __init__(self, timeReq=TimeC(), duration=TimeC(), typeReq=ActuatorType.TypeVibrator, \
                     power=0):
        VibratorActivationRequest.__init__(self, timeReq, duration, typeReq, power)

    def __eq__(self, other):
        equal = self.timeReq == other.timeReq and self.duration == other.duration and \
            self.typeReq == other.typeReq and self.power == other.power
        return equal

class LocationRequestC(LocationRequest):
    def __init__(self, locType=LocationType.GPSType, timeBetweenUpdates=15000, \
                     distanceChangeForUpdates=0):
        LocationRequest.__init__(self, locType, timeBetweenUpdates, distanceChangeForUpdates)

    def __eq__(self, other):
        equal = self.locType == other.locType and \
            self.timeBetweenUpdates == other.timeBetweenUpdates and \
            self.distanceChangeForUpdates == other.distanceChangeForUpdates
        return equal
