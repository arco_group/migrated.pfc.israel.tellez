#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from communication_elements import DateC, TimeC

class DatesChanger():

    @staticmethod
    def createTimestamp(date, time):
        timestamp = str(date.year) + '-' + str(date.month) + '-' + str(date.day) + ' ' \
            + str(time.hour) + ':' + str(time.minute) + ':' + str(time.second)

        return timestamp

    @staticmethod
    def obtainDateToTimestamp(timestamp):
        date = str(timestamp).split()[0]
        splitDate = date.split('-')
        result = DateC(int(splitDate[2]), int(splitDate[1]), int(splitDate[0]))

        return result

    @staticmethod
    def obtainTimeToTimestamp(timestamp):
        time = str(timestamp).split()[1]
        splitTime = time.split(':')
        result = TimeC(int(splitTime[0]), int(splitTime[1]), int(float(splitTime[2])))

        return result
