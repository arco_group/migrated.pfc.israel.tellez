#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import MySQLdb

class DBBroker():
    host = 'localhost'
    user = 'loram_user'
    passwd = 'loram'
    db = 'loram'

    available = []
    busy = []

    @staticmethod
    def getConnection():
        if len(DBBroker.available) == 0:
            con = MySQLdb.connect(DBBroker.host, DBBroker.user, DBBroker.passwd, DBBroker.db)
            DBBroker.available.append(con)

        con = DBBroker.available.pop() # Get the last available connection
        DBBroker.busy.append(con) # Put in busy list

        return con

    @staticmethod
    def returnConnection(con):
        if con in DBBroker.busy:
            con.commit()
            DBBroker.busy.remove(con)
            DBBroker.available.append(con)

    @staticmethod
    def clean():
        for con in DBBroker.available[:]:
            con.close()
            DBBroker.available.remove(con)
